/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */
/*global  Raphael */


define(['jquery',
        'zmienneGlobalne',
        'paper',
        'obslugaJSON',
        'wspolne/odswiezajObiekt'
       ], function (
    $,
    varGlobal,
    paper,
    json,
    odswiezajObiekt
) {
    "use strict";

    var imgWOW,
        tekst,
        init = false,
        trwaRysowanieGrafiki = false,
        daneDoOdswiezania = [],

        imgNG,
        NGTekst,
        NGzalacz,
        NGpotwierdz,


        imgNWib,
        NWibTekst,
        NWibZalacz,
        NWibPotwierdz,

        imgSmar,
        NSmarTekst,
        NSmarZalacz,
        NSmarPotwierdz,

        temp,


        odswiezDiody = function () {
            var i,
                ustawStan = function (_nazwaPath, _kolorObwodki, _kolrTla) {
                    var stanBitu = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);

                    if (stanBitu === true) { // stan wysoki
                        _nazwaPath.fillColor = _kolrTla;
                        _nazwaPath.strokeColor = _kolorObwodki;
                    } else {
                        _nazwaPath.fillColor = '#363636';
                        _nazwaPath.strokeColor = 'silver';
                    }
                };


            for (i = 0; i < daneDoOdswiezania.length; i += 1) {
                switch (daneDoOdswiezania[i].id) {
                case 'idZalaczM1':
                    ustawStan(NGzalacz, 'yellow', 'darkOrange');
                    break;
                case 'idZalaczWilnikiWibracyjne':
                    ustawStan(NWibZalacz, 'yellow', 'darkOrange');
                    break;
                case 'idZalaczPompeSmarowania':
                    ustawStan(NSmarZalacz, 'yellow', 'darkOrange');
                    break;
                case 'idPotwZalNapedGlowny':
                    ustawStan(NGpotwierdz, 'lime', 'green');
                    break;
                case 'idPotwZalSilnikiWibracyjne':
                    ustawStan(NWibPotwierdz, 'lime', 'green');
                    break;
                case 'idPotwZalSilnikPompySmar':
                    ustawStan(NSmarPotwierdz, 'lime', 'green');
                    break;
                }
            }
        },

        inicjacja = function () {
            var canvas,
                d = new $.Deferred(), // wywolanie asynchroniczne
                skala = 0.45, // zmniejszenie obrazka z korpusem gula
                punkt,
                styl = {
                    fontFamily: 'Arial',
                    fontWeight: 'normal', // bold , normal
                    fontSize: '1.2em',
                    fillColor: 'white',
                    justification: 'left' // middle
                },
                stylKrancowka = {
                    fillColor: '#363636',
                    strokeColor: 'silver',
                    strokeWidth: 2
                },
                i;

            // raz na jakiś czas grafika źle się rysuje - jest podwójnie wywoływana - nie wiem dlaczego tak się dzieje 
            //(prawdopodobnie jakiś hazard w parametrach), stąd to zabezpieczenie
            if (!trwaRysowanieGrafiki) {
                trwaRysowanieGrafiki = true;
            } else {
                console.log('PRÓBA PONOWNEGO PRZERYSOWANIA GRAFIKI!!!!!!!!!');
                return;
            }

            daneDoOdswiezania = daneDoOdswiezania.concat(json.szukajWartosci("grafikaTab1", varGlobal.sygnaly));
            $('#grafika').empty();
            canvas = document.createElement('canvas');
            $(canvas)
                .attr('id', 'canvas')
                .width('100%')
                .height('100%')
                .appendTo('#grafika');
            paper.setup(canvas); // Get a reference to the canvas object

            imgWOW = new paper.Raster('obrazki/wow02.png');
            imgWOW.position = paper.view.bounds.leftCenter;
            imgWOW.scale(skala);
            imgWOW.onLoad = function () {
                imgWOW.position.x += imgWOW.bounds.width / 2;
                imgNG = new paper.Raster('obrazki/wowNG01.png');
                imgNG.position = paper.view.center;
                imgNG.scale(skala - 0.2);
                imgNG.onLoad = function () {
                    imgNG.position.y -= imgNG.bounds.height / 2;

                    punkt = new paper.Point(imgNG.bounds.bottomCenter);
                    NGTekst = new paper.PointText(punkt);
                    NGTekst.content = 'M1: Napęd główny';
                    NGTekst.style = styl;
                    NGTekst.position.x -= imgNG.bounds.width / 1.5;
                    NGTekst.position.y += imgNG.bounds.height / 6;

                    punkt = new paper.Point(NGTekst.bounds.leftCenter); // dioda ZAŁĄCZ
                    NGzalacz = new paper.Path.Circle(punkt, 15);
                    NGzalacz.style = stylKrancowka;
                    NGzalacz.position.x += NGzalacz.bounds.width / 2;
                    NGzalacz.position.y += NGzalacz.bounds.height;
                    punkt = new paper.Point(NGzalacz.bounds.rightCenter); // opis diody
                    tekst = new paper.PointText(punkt);
                    tekst.content = 'Załącz';
                    tekst.style = styl;
                    tekst.position.x += NGzalacz.bounds.width / 2;
                    tekst.position.y += tekst.bounds.height / 2;




                    punkt = new paper.Point(NGzalacz.bounds.bottomCenter); // dioda PRACA
                    NGpotwierdz = new paper.Path.Circle(punkt, 15);
                    NGpotwierdz.style = stylKrancowka;
                    NGpotwierdz.position.y += NGpotwierdz.bounds.height / 1.5;
                    punkt = new paper.Point(NGpotwierdz.bounds.rightCenter); // opis diosy
                    tekst = new paper.PointText(punkt);
                    tekst.content = 'Praca';
                    tekst.style = styl;
                    tekst.position.x += NGpotwierdz.bounds.width / 2;
                    tekst.position.y += tekst.bounds.height / 2;

                    imgNWib = new paper.Raster('obrazki/wowNWib01.png');
                    imgNWib.position = imgNG.bounds.rightCenter;
                    imgNWib.scale(skala - 0.1);
                    imgNWib.onLoad = function () {
                        imgNWib.position.x += imgNWib.bounds.width;

                        //punkt = new paper.Point(imgNWib.bounds.bottomCenter);
                        punkt = new paper.Point(NGTekst.bounds.bottomCenter);
                        NWibTekst = new paper.PointText(punkt);
                        NWibTekst.content = 'M2, M3: Elektrowibr.';
                        NWibTekst.style = styl;
                        NWibTekst.position.x = imgNWib.position.x;

                        punkt = new paper.Point(NWibTekst.bounds.leftCenter); // dioda ZAŁĄCZ
                        NWibZalacz = new paper.Path.Circle(punkt, 15);
                        NWibZalacz.style = stylKrancowka;
                        NWibZalacz.position.x += NWibZalacz.bounds.width / 2;
                        NWibZalacz.position.y += NWibZalacz.bounds.height;
                        punkt = new paper.Point(NWibZalacz.bounds.rightCenter); // opis diody
                        tekst = new paper.PointText(punkt);
                        tekst.content = 'Załącz';
                        tekst.style = styl;
                        tekst.position.x += NWibZalacz.bounds.width / 2;
                        tekst.position.y += tekst.bounds.height / 2;

                        punkt = new paper.Point(NWibZalacz.bounds.bottomCenter); // dioda PRACA
                        NWibPotwierdz = new paper.Path.Circle(punkt, 15);
                        NWibPotwierdz.style = stylKrancowka;
                        NWibPotwierdz.position.y += NWibPotwierdz.bounds.height / 1.5;
                        punkt = new paper.Point(NWibPotwierdz.bounds.rightCenter); // opis diosy
                        tekst = new paper.PointText(punkt);
                        tekst.content = 'Praca';
                        tekst.style = styl;
                        tekst.position.x += NWibPotwierdz.bounds.width / 2;
                        tekst.position.y += tekst.bounds.height / 2;

                        imgSmar = new paper.Raster('obrazki/wowNSmar01.png');
                        imgSmar.position = imgNWib.bounds.rightCenter;
                        imgSmar.scale(skala - 0.1);
                        imgSmar.onLoad = function () {
                            imgSmar.position.x += imgSmar.bounds.width;

                            punkt = new paper.Point(NGTekst.bounds.bottomCenter);
                            NSmarTekst = new paper.PointText(punkt);
                            NSmarTekst.content = 'M4: Smarowanie';
                            NSmarTekst.style = styl;
                            NSmarTekst.position.x = imgSmar.position.x;

                            punkt = new paper.Point(NSmarTekst.bounds.leftCenter); // dioda ZAŁĄCZ
                            NSmarZalacz = new paper.Path.Circle(punkt, 15);
                            NSmarZalacz.style = stylKrancowka;
                            NSmarZalacz.position.x += NSmarZalacz.bounds.width / 2;
                            NSmarZalacz.position.y += NSmarZalacz.bounds.height;
                            punkt = new paper.Point(NSmarZalacz.bounds.rightCenter); // opis diosy
                            tekst = new paper.PointText(punkt);
                            tekst.content = 'Załącz';
                            tekst.style = styl;
                            tekst.position.x += NSmarZalacz.bounds.width / 2;
                            tekst.position.y += tekst.bounds.height / 2;

                            punkt = new paper.Point(NSmarZalacz.bounds.bottomCenter); // dioda PRACA
                            NSmarPotwierdz = new paper.Path.Circle(punkt, 15);
                            NSmarPotwierdz.style = stylKrancowka;
                            NSmarPotwierdz.position.y += NSmarPotwierdz.bounds.height / 1.5;
                            punkt = new paper.Point(NSmarPotwierdz.bounds.rightCenter); // opis diosy
                            tekst = new paper.PointText(punkt);
                            tekst.content = 'Praca';
                            tekst.style = styl;
                            tekst.position.x += NSmarPotwierdz.bounds.width / 2;
                            tekst.position.y += tekst.bounds.height / 2;

                            init = true;
                            trwaRysowanieGrafiki = false;
                        };

                    };

                };





                //paper.view.draw(); // wymuszenie rysowania (domuślnie paper.js rysuje tylko wtedy gdy jest focus - najechanie myszką)
                paper.view.onFrame = function (event) {
                    if (event.count % 12 === 0) {
                        if (init) {
                            odswiezDiody();
                        }
                    }
                };

            };
        };


    return {
        inicjacja: inicjacja
    };
});

//console.log(event.count); // the number of times the frame event was fired:
//console.log(event.time);  // The total amount of time passed since the first frame event in seconds
//console.log(event.delta);  // The time passed in seconds since the last frame event: