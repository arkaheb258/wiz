/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    'use strict';


    var cc,

        wykonaj = function (kod, selected) {
            var i,
                MENU_GULTRYBSERW,
                rozkaz,
                idButtona;

            selected.blur();

            switch (kod) {
            case varGlobal.kodyKlawiszy.gora:
                if (selected.prev().length === 0) {
                    selected.parent().find(".przyciskMenuGULTrybSerw").last().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.prev().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            case varGlobal.kodyKlawiszy.dol:
                if (selected.next().length === 0) {
                    selected.parent().find(".przyciskMenuGULTrybSerw").first().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.next().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;
            case varGlobal.kodyKlawiszy.enter:
                varGlobal.trybSerwisowyAktywny = true;
                idButtona = $(selected).attr("id");
                MENU_GULTRYBSERW = varGlobal.danePlikuKonfiguracyjnego.MENU_GULTRYBSERW[0].zawartosc;
                for (i = 0; i < MENU_GULTRYBSERW.length; i += 1) {
                    if (MENU_GULTRYBSERW[i].id === idButtona) {
                        rozkaz = MENU_GULTRYBSERW[i].rozkaz;
                    }
                }

                require(['obslugaJSON'], function (json) {
                    json.wyslij(rozkaz);
                    //console.log(rozkaz);

                    //require(['gulTrybSerwisowy/main'], function (main) {
                    //main.podtrzymujTrybSerwisowy(rozkaz);
                    //});

                    require(['progresBar'], function (progresBar) {
                        selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                        progresBar.inicjacja({
                            show: true,
                            status: 'sending'
                        }).done(function (odpowiedzAsynch) {
                            var div,
                                sygnalyPLC = [];
                            require(['diagnostykaKolumny/main'], function (main) {
                                main.start(idButtona, "NL-TRYB-SRVC_NP-TRYB-SRVC_NR-TRYB-SRVC_STYCZ-TRYB-SRVC_SK-TRYB-SRVC_IPS-TRYB-SRVC_PRED-TRYB-SRVC");

                                div = document.createElement("div");
                                $(div)
                                    .attr('id', "idDivSter")
                                    .css({
                                        'border': '0.1em solid',
                                        'border-color': 'grey',
                                        'border-radius': '0.5em',
                                        'padding': '1',
                                        'width': '50%',
                                        'display': 'inline-block',
                                        'fill': 'grey',
                                        'margin': '2px 2px 2px 2px'
                                    });
                                $("#dialogDiagnostykaKolumny").append(div);

                                sygnalyPLC = json.szukajWartosci('STER-TRYB-SRVC');
                                require(['dodajPojedynczaTabele'], function (dodajPojedynczaTabele) {
                                    dodajPojedynczaTabele.dodaj({
                                        objects: sygnalyPLC,
                                        cssDescription: 'tdOpisKolumny',
                                        cssValue: 'tdWartoscKolumny',
                                        id: "#idDivSter"
                                    });

                                    require(['gulTrybSerwisowy/main'], function (main) {
                                        main.odswiezaj(sygnalyPLC);
                                    });
                                });

                                require(['gulTrybSerwisowy/tooltip'], function (tooltip) {
                                    tooltip.inicjacja("#dialogDiagnostykaKolumny");
                                });

                                $("#dialogDiagnostykaKolumny").dialog("option", "position", {
                                    my: "bottom",
                                    at: "bottom-50px",
                                    //at: "top+30",
                                    of: window
                                });

                            });
                        });
                    });




                });



                break;
            case varGlobal.kodyKlawiszy.escape:
                require(['gulTrybSerwisowy/main'], function (main) {
                    main.zamknij();
                });
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            default:

            }


        };

    return {
        wykonaj: wykonaj
    };
});



//                                    $("#dialogDiagnostykaKolumny").dialog("option", "position", {
//                                        my: "bottom",
//                                        at: "bottom-20",
//                                        of: window
//                                    });