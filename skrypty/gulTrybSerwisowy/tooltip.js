/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

// dodanie pomocy nawigacyjnych - pokazanie wszystkich możliwych kierunkow
define(['jquery', 'wspolne/stworzTooltip'], function ($, stworzTooltip) {
    'use strict';


    var intervalId,
        bar,
        infoNawi,

        inicjacja = function (_idTooltipParent) {
            var div,
                styleCSS = {
                    'padding': '0.4em',
                    'margin': '0.2em',
                    'display': 'inline-table',
                    'border': '0.1em solid',
                    'width': '20%',
                    'border-color': '#2f2c2c',
                    'border-radius': '0.5em',
                    'text-align': 'center'
                },
                fragmentHtml = document.createDocumentFragment();

            div = document.createElement('div');
            $(div).append(stworzTooltip.inicjacja('g', 'Zwiększ prędkość', styleCSS));
            $(div).append(stworzTooltip.inicjacja('d', 'Zmniejsz prędkość', styleCSS));
            $(div).append(stworzTooltip.inicjacja('l', 'Jazda w lewo', styleCSS));
            $(div).append(stworzTooltip.inicjacja('p', 'Jazda w prawo', styleCSS));
            $(fragmentHtml).append(div);
            div = document.createElement('div');
            $(div).append(stworzTooltip.inicjacja('unlock', 'IPS F - zezwolenie', styleCSS));
            $(div).append(stworzTooltip.inicjacja('lock', 'IPS I - zazbrój', styleCSS));
            $(div).append(stworzTooltip.inicjacja('g_d', 'Załącz organ', styleCSS));
            $(div).append(stworzTooltip.inicjacja('ent', 'Komunikaty', styleCSS));
            $(div).append(stworzTooltip.inicjacja('esc', 'Wyjście', styleCSS));
            $(fragmentHtml).append(div);

            require(['alert2'], function (AlertKM) {
                setTimeout(function () {
                    infoNawi = new AlertKM({
                        id: 'idinfoNawi',
                        width: '95%',
                        position: 'top',
                        fontSize: '1.2em',
                        padding: '1em',
                        html: fragmentHtml,
                        type: 'info'
                    });
                    infoNawi.render();
                }, 500);
            });

            intervalId = setInterval(function () {
                if ($(_idTooltipParent).length === 0) {
                    clearInterval(intervalId);
                    infoNawi.remove();
                    infoNawi = null;
                }
            }, 500);
        };


    return {
        inicjacja: inicjacja
    };
});