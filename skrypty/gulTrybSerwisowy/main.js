/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne', 'obslugaJSON', 'wspolne/odswiezajObiekt'], function ($, varGlobal, json, odswiezajObiekt) {
    "use strict";

    var init = false,
        idDialog = "#DialogGULtrybSerwisowy", // tak będzie nazwane okienko popup
        idButtonPowrot, // po zamknięciu caego okienka dialog powrot na button w zakladce tab ustawienia
        tytul,
        idButtonPowrotMenu,
        intervalId,
        intervalId2,


        zamknij = function () {
            $(idButtonPowrot).addClass("kopex-selected").addClass(varGlobal.ui_state); // Powrot nawigacji na button
            $(idDialog).dialog('close');
            clearInterval(intervalId);
            clearInterval(intervalId2);
            varGlobal.trybSerwisowyAktywny = false;
        },


        odswiezaj = function (dane) {
            var i;

            //console.log(dane);
            clearInterval(intervalId2);
            intervalId2 = setInterval(function () { // przechwycenie Id funkcji setInterval, po zamknieciu okna bedzie mozliwe zakonczenie odswiezania
                for (i = 0; i < dane.length; i += 1) {
                    if (dane[i].typ_danych === "Lista") {
                        odswiezajObiekt.typLista(dane[i]);
                    }
                }
            }, varGlobal.czasOdswiezania);

        },


        wylaczTrybSerwisowy = function () {
            varGlobal.trybSerwisowyAktywny = false;
            varGlobal.doWyslania.zalTrybSerwisowy.wID = 0;
            varGlobal.doWyslania.zalTrybSerwisowy.wWartosc = 0;
            json.wyslij(varGlobal.doWyslania.zalTrybSerwisowy);
            clearInterval(intervalId);
            clearInterval(intervalId2);
            varGlobal.trybSerwisowyAktywny = false;
            //console.log(varGlobal.doWyslania.zalTrybSerwisowy);
            //zamknij();
        },


        //        podtrzymujTrybSerwisowy = function (_rozkaz) { // wywoływane z gulTrybSerwisowy/nawiMenu.js
        //            clearInterval(intervalId);
        //            intervalId = setInterval(function () {
        //                //console.log(_rozkaz);
        //                //json.wyslij(_rozkaz);
        //            }, 3000);
        //        },



        otworzMenu = function () {
            var div,
                fragMenu2 = document.createDocumentFragment();


            if ($(idDialog).length === 0) { // sprawdzenie czy div już nie istnieje
                div = document.createElement("div");
                $(div)
                    .addClass('OknaDialog')
                    .addClass('ui-corner-all')
                    .css({
                        'padding': '2em',
                        'margin': '1em'
                    })
                    .attr('id', idDialog.replace("#", ""));
                $('body').append(div);

                $(idDialog).dialog({
                    modal: true,
                    closeOnEscape: false,
                    autoOpen: false,
                    height: "auto",
                    width: '70%',
                    title: $(idButtonPowrot).text()
                });

                require(['wspolne/dodajMenu2'], function (dodajMenu2) {
                    fragMenu2 = dodajMenu2.dodajElementyHtml(varGlobal.danePlikuKonfiguracyjnego.MENU_GULTRYBSERW[0].zawartosc, 'przyciskMenuGULTrybSerw');
                    $(idDialog).append(fragMenu2);
                    $(idDialog).dialog("open");
                    $("button").button(); // Nadanie stylu jquery
                    $(idDialog).children().first().addClass("kopex-selected").addClass(varGlobal.ui_state); // Skierowanie nawigacji z klawiatury na nowo stworzone elementy submenu
                    dodajMenu2.allignVertical(idDialog); // wyrównanie buttonów w osi Y   

                    $(idDialog).dialog("option", "position", {
                        my: "center",
                        at: "center",
                        of: window
                    });
                });
            }

            //            require(['gulTrybSerwisowy/tooltip'], function (tooltip) {
            //                tooltip.inicjacja(idDialog);
            //            });

            $(idDialog).one("dialogclose", function (event, ui) { // oczekiwanie na zdarzenie zamknięcia okienka
                $(idDialog).remove();
            });
        },


        inicjacja = function (_idButtona) {
            idButtonPowrot = '#' + _idButtona;
            $(idButtonPowrot).on("click", function (event, ui) {
                otworzMenu();
            });
        };


    return {
        inicjacja: inicjacja,
        zamknij: zamknij,
        odswiezaj: odswiezaj,
        //podtrzymujTrybSerwisowy: podtrzymujTrybSerwisowy,
        wylaczTrybSerwisowy: wylaczTrybSerwisowy
    };

});