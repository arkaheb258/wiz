/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define*/


define(['wspolne/odswiezajObiekt',  'zmienneGlobalne'], function (odswiezajObiekt, varGlobal) {
    'use strict';


    var aktualizuj = function (_daneWej) {
        var i,
            namurDiagnostyka,
            daneTCP,
            length;

        //console.log(_daneWej);
        //console.log('diagnostyka plc - odswiezaj');
        daneTCP = varGlobal.daneTCP;
        length = _daneWej.length;
        for (i = 0; i < length; i += 1) {
            // ------------------------------
            // dane typu analog
            // ------------------------------
            if (_daneWej[i].typ_danych === "Analog" && daneTCP.Analog[_daneWej[i].poz_ramka] !== undefined) {
                odswiezajObiekt.typAnalog(_daneWej[i]);
//                if (_daneWej[i].id === 'idPozycjaNapinacza') {
//                    console.log('napinacz');
//                }

            }

            // ------------------------------
            // dane typu lista
            // ------------------------------
            if (_daneWej[i].typ_danych === "Lista") {
                odswiezajObiekt.typLista(_daneWej[i]);
            }

            // ------------------------------
            // dane typu bit
            // ------------------------------
            if (_daneWej[i].typ_danych === "Bit") { // Wyswietlenie danych bitowych
                if ((_daneWej[i].jednostka === "DO") || (_daneWej[i].jednostka === "Lampka") || (_daneWej[i].jednostka === "LED")) {
                    odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'darkOrange');
                } else if (_daneWej[i].jednostka === "error") {
                    odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'darkred');
                } else if (_daneWej[i].jednostka === "DI") {
                    odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'green');
                } else if (_daneWej[i].jednostka === "Namur") {
                    if (_daneWej[i + 1] === undefined) {
                        odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'green');
                        return;
                    }

                    if (_daneWej[i + 1].jednostka === "NamurDiagnostyka") { // jeśli następny obiekt jest diagnostyką dla wejścia namur
                        namurDiagnostyka = odswiezajObiekt.typBitStan(_daneWej[i + 1]); // sygnał diagnostyki zawsze jest od razu za sygnałem namur!!!
                        if (!namurDiagnostyka) { // diagnostyka - OK
                            odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'green');
                        } else { // diagnostyka - błąd
                            odswiezajObiekt.typBit(_daneWej[i], 'darkred', 'darkred');
                        }
                    } else { // brak obiektu z diagnostyką
                        odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'green');
                    }


                } else {
                    odswiezajObiekt.typBit(_daneWej[i], 'transparent', 'green');
                }
            }
        }
    };


    return {
        aktualizuj: aktualizuj
    };

});

