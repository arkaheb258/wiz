/*global $, jQuery*/
/*jslint devel: true */
/*jslint nomen: true*/
/*global require, define */
/*global plikParametry:true, plikSygnaly:true, plikKonfiguracja:true, plikDiagnostykaBlokow:true, plikKomunikaty:true */

define(['jquery',
        'kommTCP',
        'zmienneGlobalne',
        'obslugaJSON',
        'kontrolkiUI',
        'klawiatura',
        'ladowanieHtml',
        'd3'],
    function (jquery,
        dane,
        varGlobal,
        json,
        jqui,
        klawiatura,
        ladowanieHtml,
        d3) {
        'use strict';

        var ccc,


            domyslne = function () {
                var minuty = 5;

                console.log('przywracam domyslne');
                varGlobal.parametry = json.pobierz("jsonDefault/parametry.json");
                varGlobal.sygnaly = json.pobierz('jsonDefault/sygnaly.json');
                varGlobal.danePlikuKonfiguracyjnego = json.pobierz('jsonDefault/konfiguracja.json');
                varGlobal.tekstyKomunikatow = json.pobierz("jsonDefault/komunikaty.json");
                varGlobal.diagnostykaBlokow = json.pobierz('jsonDefault/diagnostykaBlokow.json');

                require(['kommTCP'], function (kommTCP) {
                    kommTCP.socket.emit('getDefPar');
                    kommTCP.socket.emit('getDefSyg');
                });

                varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                varGlobal.typKombajnu = varGlobal.parametry.TYPM;

                jqui.inicjacja(); // Zaladowanie kontrolek jquery ui                                            
                dane.inicjacja(); // Inicjacja komunikacji tcp
                ladowanieHtml.inicjacja(); // Dynamiczne dodawanie elementow html wg pliku jsona 

                require(['alert2'], function (AlertKM) {
                    var infoUstawieniaDomyslne = new AlertKM({
                        id: 'idUstawieniaDomyslne',
                        position: 'bottom',
                        type: 'warning',
                        padding: '0.3em',
                        width: '99%',
                        texts: ['Uwaga: Uszkodzone pliki konfiguracyjne - przyjęto ustawienia domyślne!']
                    });
                    infoUstawieniaDomyslne.render();
                });
            },



            inicjacja = function () {

                require(['plikSoft'], function (plikSoft) {
                    console.log(plikSoft.dane);

                    varGlobal.hardware.ip = plikSoft.dane.ip;
                    varGlobal.typKombajnu = plikSoft.dane.kombajn;
                    varGlobal.wersjaJezykowa = plikSoft.dane.jezyk;
                    varGlobal.wersjaWyposazenia = 0;

                    switch (plikSoft.dane.jezyk) {
                    case 0: // polski
                        varGlobal.wersjaJezykowa = plikSoft.dane.jezyk;
                        break;
                    case 1: // rosyjski
                        varGlobal.wersjaJezykowa = plikSoft.dane.jezyk;
                        break;
                    default:
                        varGlobal.wersjaJezykowa = 0;
                        console.log('Brak poprawnej wersji językowej! Ustawiam język polski');
                        break;
                    }


                    //                    var pobierz = function (_nazwaPliku) {
                    //                        var pobraneDane;
                    //
                    //                        $.ajax({
                    //                            dataType: "json",
                    //                            mimeType: "application/json",
                    //                            'async': false,
                    //                            url: _nazwaPliku,
                    //                            error: function (xhr, status) {
                    //                                console.log(status);
                    //                            },
                    //                            complete: function (xhr, status) {
                    //                                console.log(status);
                    //                            },
                    //                            success: function (json) {
                    //                                //console.log(json);
                    //
                    //                            }
                    //                        });
                    //                        return pobraneDane;
                    //                    };
                    //pobierz('skrypty/qqq.json');
                    //pobierz('json/wow/parametry_0.json');





                    //                                                     __  __       _ ____   ___  _   _   _ _ 
                    //  _ __   ___  _ __  _ __ __ ___      ___ __   ___  _/_/ /__/     | / ___| / _ \| \ | | | | |
                    // | '_ \ / _ \| '_ \| '__/ _` \ \ /\ / / '_ \ / _ \/ __|/ __|  _  | \___ \| | | |  \| | | | |
                    // | |_) | (_) | |_) | | | (_| |\ V  V /| | | | (_) \__ \ (__  | |_| |___) | |_| | |\  | |_|_|
                    // | .__/ \___/| .__/|_|  \__,_| \_/\_/ |_| |_|\___/|___/\___|  \___/|____/ \___/|_| \_| (_|_)
                    // |_|         |_|   


                    //                    require(['../json/' + varGlobal.typKombajnu + '/komunikaty_' + varGlobal.wersjaJezykowa], function (plik) {
                    //                        varGlobal.parametry = plik.dane;
                    //                    });
                    //                    require(['../json/' + varGlobal.typKombajnu + '/parametry_' + varGlobal.wersjaJezykowa], function (plik) {
                    //                        varGlobal.parametry = plik.dane;
                    //                    });
                    //                    require(['../json/' + varGlobal.typKombajnu + '/sygnaly_' + varGlobal.wersjaJezykowa], function (plik) {
                    //                        varGlobal.sygnaly = plik.dane;
                    //                    });
                    //                    require(['../json/' + varGlobal.typKombajnu + '/diagnostykaBlokow'], function (plik) {
                    //                        varGlobal.diagnostykaBlokow = plik.dane;
                    //                    });
                    //                    require(['../json/' + varGlobal.typKombajnu + '/konfiguracja_' + varGlobal.wersjaJezykowa], function (plik) {
                    //                        varGlobal.danePlikuKonfiguracyjnego = plik.dane;
                    //                        jqui.inicjacja();
                    //                        ladowanieHtml.inicjacja();
                    //                        //dane.inicjacja();
                    //                    });






                    //d3.json("skrypty/qqq.json", function (json) {
                    //alert(json);
                    //});


                    varGlobal.parametry = json.pobierz("jsonDefault/parametry.json");
                    varGlobal.sygnaly = json.pobierz('jsonDefault/sygnaly.json');
                    varGlobal.danePlikuKonfiguracyjnego = json.pobierz('jsonDefault/konfiguracja.json');
                    varGlobal.tekstyKomunikatow = json.pobierz("jsonDefault/komunikaty.json");
                    varGlobal.diagnostykaBlokow = json.pobierz('jsonDefault/diagnostykaBlokow.json');
                    jqui.inicjacja();
                    ladowanieHtml.inicjacja();



//                    json.pobierzAsynchronicznie('soft.json').done(function (_plikOK) {
//                        console.log(_plikOK);
//                    });
//                    json.pobierzAsynchronicznie('json/wow/parametry.json').done(function (_plikOK) {
//                        console.log(_plikOK);
//                    });
//                    json.pobierzAsynchronicznie('json/wow/sygnaly.json').done(function (_plikOK) {
//                        console.log(_plikOK);
//                    });
//                    json.pobierzAsynchronicznie('json/wow/komunikaty.json').done(function (_plikOK) {
//                        console.log(_plikOK);
//                    });
//                    json.pobierzAsynchronicznie('json/wow/diagnostykaBlokow.json').done(function (_plikOK) {
//                        console.log(_plikOK);
//                    });
//                    json.pobierzAsynchronicznie('json/wow/konfiguracja.json').done(function (_plikOK) {
//                        jqui.inicjacja();
//                        ladowanieHtml.inicjacja();
//                    });




                    //                    require(['obslugaJSON'], function (obslugaJSON) {
                    //                        var ccc;
                    //                        //ccc = obslugaJSON.pobierzOffline('json/wow/komunikaty_0.json');
                    //                        //ccc = obslugaJSON.pobierzOffline('skrypty/qqq2.json');
                    //                        //alert(ccc);
                    //
                    //                        varGlobal.tekstyKomunikatow = obslugaJSON.pobierzOffline('json/wow/komunikaty_0.json');
                    //                        varGlobal.parametry = obslugaJSON.pobierzOffline('json/wow/parametry_0.json');
                    //                        varGlobal.sygnaly = obslugaJSON.pobierzOffline('json/wow/sygnaly_0.json');
                    //                        varGlobal.diagnostykaBlokow = obslugaJSON.pobierzOffline('json/wow/diagnostykaBlokow.json');
                    //                        varGlobal.danePlikuKonfiguracyjnego = obslugaJSON.pobierzOffline('json/wow/konfiguracja_0.json');
                    //                        jqui.inicjacja();
                    //                        ladowanieHtml.inicjacja();
                    //
                    //
                    //                    });




                    // file:///C:/path/...../js/forcetree.json
                    //require(['json!../qqq_0.json'], function (data) {
                    //D:\node2\www\source2\json
                    //plikSoft: '../json/soft',

                    //require(['json!./qqq.json'], function (data) {
                    //require(['json!./../json/qqq.json'], function (data) {
                    //                    require(['json!./../json/wow/parametry_0.json'], function (data) {
                    //                        //require(['json!./../json/' + varGlobal.typKombajnu + '/parametry_' + varGlobal.wersjaJezykowa + '.json'], function (data) {
                    //                        console.log(data);
                    //                    });


                    //                    require(['json!./../json/' + varGlobal.typKombajnu + '/komunikaty_' + varGlobal.wersjaJezykowa + '.json'], function (plik) {
                    //                        varGlobal.tekstyKomunikatow = plik;
                    //                    });
                    //                    require(['json!./../json/' + varGlobal.typKombajnu + '/parametry_' + varGlobal.wersjaJezykowa + '.json'], function (plik) {
                    //                        varGlobal.parametry = plik;
                    //                    });
                    //                    require(['json!./../json/' + varGlobal.typKombajnu + '/sygnaly_' + varGlobal.wersjaJezykowa + '.json'], function (plik) {
                    //                        varGlobal.sygnaly = plik;
                    //                    });
                    //                    require(['json!./../json/' + varGlobal.typKombajnu + '/diagnostykaBlokow.json'], function (plik) {
                    //                        varGlobal.diagnostykaBlokow = plik;
                    //                    });
                    //                    require(['json!./../json/' + varGlobal.typKombajnu + '/konfiguracja_' + varGlobal.wersjaJezykowa + '.json'], function (plik) {
                    //                        varGlobal.danePlikuKonfiguracyjnego = plik;
                    //                        jqui.inicjacja();
                    //                        ladowanieHtml.inicjacja();
                    //                    });









                });


                //                varGlobal.hardware.ip = '192.168.3.66';
                //                varGlobal.typKombajnu = 'GUL';
                //                varGlobal.wersjaWyposazenia = 0;
                //                varGlobal.wersjaJezykowa = 0;
                //
                //                varGlobal.parametry = plikParametry;
                //                varGlobal.sygnaly = plikSygnaly;
                //                varGlobal.danePlikuKonfiguracyjnego = plikKonfiguracja;
                //                varGlobal.diagnostykaBlokow = plikDiagnostykaBlokow;
                //                varGlobal.tekstyKomunikatow = plikKomunikaty;
                //                plikParametry = null;
                //                plikSygnaly = null;
                //                plikKonfiguracja = null;
                //                plikDiagnostykaBlokow = null;
                //
                //                jqui.inicjacja(); // konfiguracja kontrolek jquery ui                                            
                //                ladowanieHtml.inicjacja(); // rozpoczęcie dynamicznego ładowania wizualizacji w zależności od typu kombajnu
                //                //dane.inicjacja(); // inicjacja komunikacji tcp

            },


            inicjacja2 = function () {






                //pobieranie asynchroniczne plików jeden po drugim
                json.pobierzAsynchronicznie('hardware.json').done(function (_plikOK) {
                    if (_plikOK) {
                        switch (varGlobal.hardware.ip) {
                        case '192.168.3.31':
                            varGlobal.hardware.czyMinimumViz = false;
                            break;
                        case '192.168.3.51':
                            if (window.screen.width < 500) { // beagle ma rozdzielczość 480x277
                                varGlobal.hardware.czyMinimumViz = true;
                            } else {
                                varGlobal.hardware.czyMinimumViz = false;
                            }
                            break;
                        default:
                            // !!!!!!!!!!!!!!!!! DO TESTÓW NA LAPTOPIE !!!!!!!!!!!!!!!!!!!!!!!!1
                            if (window.screen.width < 500) { // beagle ma rozdzielczość 480x277
                                varGlobal.hardware.czyMinimumViz = true;
                            } else {
                                varGlobal.hardware.czyMinimumViz = false;
                            }
                        }

                        json.pobierzAsynchronicznie('parametry.json').done(function (_plikOK) {
                            if (_plikOK) { // odpowiedź asynchroniczna 
                                json.pobierzAsynchronicznie('sygnaly.json').done(function (_plikOK) {
                                    if (_plikOK) { // odpowiedź asynchroniczna 
                                        json.pobierzAsynchronicznie('komunikaty.json').done(function (_plikOK) {
                                            if (_plikOK) {
                                                json.pobierzAsynchronicznie('konfiguracja.json').done(function (_plikOK) { // pobranie konfiguracja.json
                                                    if (_plikOK) { // odpowiedź asynchroniczna 
                                                        json.pobierzAsynchronicznie('diagnostykaBlokow.json').done(function (_plikOK) {
                                                            if (_plikOK) {
                                                                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART !== undefined) {
                                                                    varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                                                                } else {
                                                                    varGlobal.wersjaWyposazenia = 0;
                                                                }

                                                                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART !== undefined) {
                                                                    varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                                                                } else {
                                                                    varGlobal.wersjaJezykowa = 0;
                                                                }

                                                                //varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                                                                //varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                                                                varGlobal.typKombajnu = varGlobal.parametry.TYPM;

                                                                jqui.inicjacja(); // konfiguracja kontrolek jquery ui                                            
                                                                dane.inicjacja(); // inicjacja komunikacji tcp
                                                                ladowanieHtml.inicjacja(); // rozpoczęcie dynamicznego ładowania wizualizacji w zależności od typu kombajnu

                                                                if ((varGlobal.hardware.ip === '192.168.3.66')) { // do pracy na laptopie w biurze
                                                                    varGlobal.poziomDostepu = 'Srvc';
                                                                }

                                                                require(['parametry/odswiez'], function (odswiez) {
                                                                    odswiez.inicjacja(); // odświeżenie listy parametrów
                                                                });

                                                                console.log('Typ:' + varGlobal.typKombajnu + ', Wyposazenie:' + varGlobal.wersjaWyposazenia + ', Jezyk:' + varGlobal.wersjaJezykowa + ', Res:' + screen.width + 'x' + screen.height + ', IP:' + varGlobal.hardware.ip);

                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    }
                });


            };

        return {
            inicjacja: inicjacja,
            domyslne: domyslne
        };

    });