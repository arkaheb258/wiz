/*global $, jQuery*/
/*jslint devel: true */
/*jslint nomen: true*/
/*global require, define */
/*global plikParametry:true, plikSygnaly:true, plikKonfiguracja:true, plikDiagnostykaBlokow:true, plikKomunikaty:true */

define(['jquery',
        'kommTCP',
        'zmienneGlobalne',
        'obslugaJSON',
        'kontrolkiUI',
        'klawiatura',
        'ladowanieHtml',
       'alert2'],
    function (jquery,
        dane,
        varGlobal,
        json,
        jqui,
        klawiatura,
        ladowanieHtml,
        AlertKM) {
        'use strict';

        var jsonError = false,
            initSocketIo = false,
            jsonBadfiles = [],
            intervalId,
            infoUstawieniaDomyslne,
            infoSerwerOffline = null,
            infoZlyPlik = null,
            initSerwerOffline = false,


            domyslne = function () {

                //                if (!czyUstawieniaDomyslne) {
                //                    czyUstawieniaDomyslne = true;
                //                    setTimeout(function () {
                //                        if ($("#DialogBrakKomunikacjiTCP").length > 0) { // zamkniecie  okienka o braku komunikajci - tez z mala zwloka czasowa
                //                            $("#DialogBrakKomunikacjiTCP").dialog("close");
                //                        }
                //                        require(['alert'], function (alert) {
                //                            alert.inicjacja({
                //                                texts: ['Przyjmuję ustawienia domyslne'],
                //                                timer: czasDoOdswiezenia - 1000
                //                            });
                //                            setTimeout(function () {
                //                                require(['app'], function (app) { // wywołanie pobrania plików domyslnych jsona
                //                                    app.domyslne();
                //                                });
                //                            }, czasDoOdswiezenia);
                //                        });
                //                    }, czasDoOdswiezenia);
                //                }



                //                var minuty = 5;
                //
                //                console.log('przywracam domyslne');
                //                varGlobal.parametry = json.pobierz("jsonDefault/parametry.json");
                //                varGlobal.sygnaly = json.pobierz('jsonDefault/sygnaly.json');
                //                varGlobal.danePlikuKonfiguracyjnego = json.pobierz('jsonDefault/konfiguracja.json');
                //                varGlobal.tekstyKomunikatow = json.pobierz("jsonDefault/komunikaty.json");
                //                varGlobal.diagnostykaBlokow = json.pobierz('jsonDefault/diagnostykaBlokow.json');
                //
                //                require(['kommTCP'], function (kommTCP) {
                //                    kommTCP.socket.emit('getDefPar');
                //                    kommTCP.socket.emit('getDefSyg');
                //                });
                //
                //                varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                //                varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                //                varGlobal.typKombajnu = varGlobal.parametry.TYPM;
                //
                //                jqui.inicjacja(); // Zaladowanie kontrolek jquery ui                                            
                //                dane.inicjacja(); // Inicjacja komunikacji tcp
                //                ladowanieHtml.inicjacja(); // Dynamiczne dodawanie elementow html wg pliku jsona 
                //
                //                require(['alert2'], function (AlertKM) {
                //                    var infoUstawieniaDomyslne = new AlertKM({
                //                        id: 'idUstawieniaDomyslne',
                //                        position: 'bottom',
                //                        type: 'warning',
                //                        padding: '0.3em',
                //                        width: '99%',
                //                        texts: ['Uwaga: Uszkodzone pliki konfiguracyjne - przyjęto ustawienia domyślne!']
                //                    });
                //                    infoUstawieniaDomyslne.render();
                //                });
            },


            uruchomWizualizacje = function () {

                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART !== undefined) {
                    varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                } else {
                    varGlobal.wersjaWyposazenia = 0;
                }

                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART !== undefined) {
                    varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                } else {
                    varGlobal.wersjaJezykowa = 0;
                }
                varGlobal.typKombajnu = varGlobal.parametry.TYPM;

                jqui.inicjacja();
                ladowanieHtml.inicjacja();

                require(['parametry/odswiez'], function (odswiez) {
                    odswiez.inicjacja(); // odświeżenie listy parametrów
                });

                if ((varGlobal.hardware.ip === '192.168.3.66')) { // do pracy na laptopie w biurze
                    varGlobal.poziomDostepu = 'Srvc';
                }

                console.log('Typ:' + varGlobal.typKombajnu +
                    ', Wyposazenie:' + varGlobal.wersjaWyposazenia +
                    ', Jezyk:' + varGlobal.wersjaJezykowa +
                    ', Res:' + screen.width + 'x' + screen.height +
                    ', IP:' + varGlobal.hardware.ip);



                //setTimeout(function () { // nie wiem dlaczego ale bez zwłoki czasowej stronka przestaje się ładować
                //}, 2000);
                //dane.inicjacja();

                //require(['kommTCP'], function (dane) {
                dane.inicjacja();
                //});

            },


            wyswietlAlarmPobraniaPlikow = function () {
                if (infoZlyPlik === null) {
                    infoZlyPlik = new AlertKM({
                        id: 'idInfoZlyPlik',
                        title: 'Błąd pobrania pliku json',
                        position: 'center',
                        texts: jsonBadfiles
                    });
                    infoZlyPlik.render();
                } else {
                    infoZlyPlik.updateTexts(jsonBadfiles);
                }
            },


            pobierz = function (_nazwaPliku) {
                var pelnaSciezka;

                pelnaSciezka = 'json/' + varGlobal.typKombajnu + '/' + _nazwaPliku + '_' + varGlobal.wersjaJezykowa + '.json';
                //pobierz('json/' + varGlobal.typKombajnu + '/sygnaly_' + varGlobal.wersjaJezykowa + '.json');
                //pobierz('json/' + varGlobal.typKombajnu + '/komunikaty_' + varGlobal.wersjaJezykowa + '.json');
                //pobierz('json/' + varGlobal.typKombajnu + '/diagnostykaBlokow.json');
                //pobierz('json/' + varGlobal.typKombajnu + '/konfiguracja_' + varGlobal.wersjaJezykowa + '.json'); // to zawsze ma być pobrane na końcu

                $.ajax({
                    dataType: "jsonp",
                    mimeType: "application/json",
                    timeout: 15000,
                    'async': true,
                    //url: 'http://127.0.0.1/json/wow/konfiguracja_0.json',
                    url: 'http://127.0.0.1/' + pelnaSciezka,
                    error: function (xhr, status) {
                        //alert('error');

                        jsonError = true;
                        jsonBadfiles.push(_nazwaPliku + ' - ' + status);
                        wyswietlAlarmPobraniaPlikow();
                    },
                    complete: function (xhr, status) {},
                    success: function (json) {
                        //alert('succes');

                        if ((typeof json) === "string") { // nie przyszla struktura jsona -> blad pobrania pliku po ftp ze sterownika przez serwer www
                            jsonError = true;
                            jsonBadfiles.push(json);
                            //console.log(jsonBadfiles);
                            wyswietlAlarmPobraniaPlikow();
                        } else { // plik ze zterownika ok
                            switch (_nazwaPliku) {
                            case 'parametry':
                                varGlobal.parametry = json;
                                break;
                            case 'komunikaty':
                                varGlobal.tekstyKomunikatow = json;
                                break;
                            case 'diagnostykaBlokow':
                                varGlobal.diagnostykaBlokow = json;
                                break;
                            case 'sygnaly':
                                varGlobal.sygnaly = json;
                                break;
                            case 'konfiguracja':
                                varGlobal.danePlikuKonfiguracyjnego = json;
                                if (!jsonError) { // wszystkie pliki json ok
                                    uruchomWizualizacje();
                                } else {
                                    console.log('Wizualizacja nie uruchomiona - uszkodzone pliki json');
                                }
                                break;

                            default:
                                console.log('default');
                                break;
                            }
                        }
                    }
                });
            },


            pobierzPlikiJson = function () {
                //console.log('uruchamiam wizualizacje');
                //                pobierz('json/hardware.json');
                //                pobierz('json/parametry.json');
                //                pobierz('json/sygnaly.json');
                //                pobierz('json/komunikaty.json');
                //                pobierz('json/diagnostykaBlokow.json');
                //                pobierz('json/konfiguracja.json'); // to zawsze ma być pobrane na końcu

            },



            inicjacja = function () {

                //console.log('inicjacja');
                // sprawdzenie czy serwer w ogóle żyje
                $.ajax({
                    url: 'http://localhost/json/soft.json',
                    //url: 'http://127.0.0.1/gul/konfiguracja_0.json',
                    //url: 'http://127.0.0.1/soft.json?callback=?',

                    dataType: "jsonp",
                    mimeType: "application/json",
                    timeout: 2500,
                    error: function (xhr, status) {
                        //console.log(status);
                        jsonBadfiles.push('soft.json - ' + status);
                        wyswietlAlarmPobraniaPlikow();
                    },
                    complete: function (xhr, status) {},
                    success: function (json) {
                        //console.log('success');

                        varGlobal.hardware.ip = json.ip;
                        varGlobal.typKombajnu = json.kombajn;
                        //varGlobal.wersjaJezykowa = json.jezyk;
                        varGlobal.wersjaWyposazenia = 0;

                        switch (varGlobal.hardware.ip) {
                        case '192.168.3.31':
                            varGlobal.hardware.czyMinimumViz = false;
                            break;
                        case '192.168.3.51':
                            if (window.screen.width < 500) { // beagle ma rozdzielczość 480x277
                                varGlobal.hardware.czyMinimumViz = true;
                            } else {
                                varGlobal.hardware.czyMinimumViz = false;
                            }
                            break;
                        default:
                            if (window.screen.width < 500) { // !!!!!!!!!!!!!!!!! DO TESTÓW NA LAPTOPIE !!!!!!!!!!!!!!!!!!!!!!!!1
                                varGlobal.hardware.czyMinimumViz = true;
                            } else {
                                varGlobal.hardware.czyMinimumViz = false;
                            }
                        }

                        switch (json.jezyk) {
                        case 0: // polski
                            varGlobal.wersjaJezykowa = json.jezyk;
                            break;
                        case 1: // rosyjski
                            varGlobal.wersjaJezykowa = json.jezyk;
                            break;
                        default:
                            varGlobal.wersjaJezykowa = 0;
                            console.log('Brak poprawnej wersji językowej! Ustawiam język polski');
                            break;
                        }

                        pobierz('parametry');
                        pobierz('sygnaly');
                        pobierz('komunikaty');
                        //pobierz('diagnostykaBlokow');
                        pobierz('konfiguracja'); // to zawsze ma być pobrane na końcu

                        //                        pobierz('json/' + varGlobal.typKombajnu + '/parametry_' + varGlobal.wersjaJezykowa + '.json');
                        //                        pobierz('json/' + varGlobal.typKombajnu + '/sygnaly_' + varGlobal.wersjaJezykowa + '.json');
                        //                        pobierz('json/' + varGlobal.typKombajnu + '/komunikaty_' + varGlobal.wersjaJezykowa + '.json');
                        //                        pobierz('json/' + varGlobal.typKombajnu + '/diagnostykaBlokow.json');
                        //                        pobierz('json/' + varGlobal.typKombajnu + '/konfiguracja_' + varGlobal.wersjaJezykowa + '.json'); // to zawsze ma być pobrane na końcu


                    }

                });







            };



        return {
            inicjacja: inicjacja
        };

    });