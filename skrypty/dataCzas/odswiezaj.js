/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require */


define(['jquery', 'obslugaJSON', 'zmienneGlobalne'], function ($, json, varGlobal) {
    "use strict";

    var init = false,
        daneDoOdswiezania = [],
        data = new Date(), //"October 13, 1975 11:13:00"


        odswiezajZegar = function () {
            var tekstDaty,
                zeroWiodace = function (i) {
                    return (i < 10) ? '0' + i : i;
                };

            data.setTime(varGlobal.daneTCP.TimeStamp_js);
            //console.log(varGlobal.tempTCP.TimeStamp_js);

            tekstDaty = data.getUTCFullYear() + '/' + zeroWiodace(data.getUTCMonth() + 1) + '/' + zeroWiodace(data.getUTCDate()) + ' ' +
                zeroWiodace(data.getUTCHours()) + ":" + zeroWiodace(data.getUTCMinutes()) + ":" + zeroWiodace(data.getUTCSeconds());

            //            tekstDaty = data.getFullYear() + '/' + zeroWiodace(data.getMonth() + 1) + '/' + zeroWiodace(data.getDate()) + ' ' +
            //                zeroWiodace(data.getHours()) + ":" + zeroWiodace(data.getMinutes()) + ":" + zeroWiodace(data.getSeconds());

            varGlobal.data = tekstDaty;

            //$('#p_dataCzas').text(tekstDaty);

            return tekstDaty;
        },


        inicjacja = function () {
            setInterval(function () {
                $('#p_dataCzas').text(odswiezajZegar());
            }, varGlobal.czasOdswiezania);
        };


    return {
        inicjacja: inicjacja,
        odswiezajZegar: odswiezajZegar
    };
});