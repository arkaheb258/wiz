/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

// dodanie pomocy nawigacyjnych - pokazanie wszystkich możliwych kierunkow
define(['jquery', 'wspolne/stworzTooltip'], function ($, stworzTooltip) {
    'use strict';


    var intervalId,
        bar,
        infoNawi,

        inicjacja = function () {
            var div,
                fragmentHtml = document.createDocumentFragment(),
                styleCSS = {
                    'padding': '0.4em',
                    'margin': '0.2em',
                    'display': 'inline-table',
                    'border': '0.1em solid',
                    'width': '45%',
                    'border-color': '#2f2c2c',
                    'border-radius': '0.5em',
                    'text-align': 'center'
                };

            div = document.createElement('div');
            $(div).append(stworzTooltip.inicjacja('g_d', 'Zmień wartość', styleCSS));
            $(div).append(stworzTooltip.inicjacja('l_p', 'Nawiguj', styleCSS));
            $(div).append(stworzTooltip.inicjacja('ent', 'Zatwierdź', styleCSS));
            $(div).append(stworzTooltip.inicjacja('esc', 'Anuluj', styleCSS));
            $(fragmentHtml).append(div);

            require(['alert2'], function (AlertKM) {
                setTimeout(function () {
                    infoNawi = new AlertKM({
                        id: 'idinfoDataCzas',
                        width: '55%',
                        position: 'top',
                        fontSize: '1.2em',
                        padding: '1em',
                        html: fragmentHtml,
                        type: 'info'
                    });
                    infoNawi.render();
                }, 500);
            });

            intervalId = setInterval(function () {
                if ($("#DialogDataCzas").length === 0) {
                    clearInterval(intervalId);
                    infoNawi.remove();
                    infoNawi = null;
                }
            }, 500);
        };


    return {
        inicjacja: inicjacja
    };
});