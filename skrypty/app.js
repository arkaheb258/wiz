/*global $, jQuery*/
/*jslint devel: true */
/*jslint nomen: true*/
/*global require, define */
/*global plikParametry:true, plikSygnaly:true, plikKonfiguracja:true, plikDiagnostykaBlokow:true, plikKomunikaty:true */

define(['jquery',
        'kommTCP',
        'zmienneGlobalne',
        'obslugaJSON',
        'kontrolkiUI',
        'klawiatura',
        'ladowanieHtml',
       'alert2'],
    function (jquery,
        dane,
        varGlobal,
        json,
        jqui,
        klawiatura,
        ladowanieHtml,
        AlertKM) {
        'use strict';

        var jsonError = false,
            initSocketIo = false,
            jsonBadfiles = [],
            intervalId,
            infoUstawieniaDomyslne,
            infoSerwerOffline = null,
            infoZlyPlik = null,
            initSerwerOffline = false,

            uruchomWizualizacje = function () {
                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART !== undefined) {
                    varGlobal.wersjaWyposazenia = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaWyposazeniaElektr.WART;
                } else {
                    varGlobal.wersjaWyposazenia = 0;
                }

                if (varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART !== undefined) {
                    varGlobal.wersjaJezykowa = varGlobal.parametry.DANE.grupa1.podgrupa2.rKonfWersjaJezykowa.WART;
                } else {
                    varGlobal.wersjaJezykowa = 0;
                }
                varGlobal.typKombajnu = varGlobal.parametry.TYPM;

                jqui.inicjacja();
                ladowanieHtml.inicjacja();
                if ((varGlobal.hardware.ip === '192.168.3.66')) { // do pracy na laptopie w biurze
                    varGlobal.poziomDostepu = 'Srvc';
                }

                console.log('Typ:' + varGlobal.typKombajnu +
                    ', Wyposazenie:' + varGlobal.wersjaWyposazenia +
                    ', Jezyk:' + varGlobal.wersjaJezykowa +
                    ', Res:' + screen.width + 'x' + screen.height +
                    ', IP:' + varGlobal.hardware.ip);

                dane.inicjacja();
                require(['parametry/odswiez'], function (odswiez) {
                    odswiez.inicjacja(); // odświeżenie listy parametrów
                });
            },


            wyswietlAlarmPobraniaPlikow = function (_nazwaPliku) {
                if (infoZlyPlik === null) {
                    infoZlyPlik = new AlertKM({
                        id: 'idInfoZlyPlik',
                        title: 'Błąd pobrania pliku json',
                        position: 'center',
                        texts: jsonBadfiles
                    });
                    infoZlyPlik.render();
                } else {
                    infoZlyPlik.updateTexts(jsonBadfiles);
                }
            },


            pobierz = function (_nazwaPliku, _katalog) {
                $.ajax({
                    dataType: "jsonp",
                    mimeType: "application/json",
                    timeout: 15000,
                    'async': true,
                    //url: 'http://localhost:8888/' + _nazwaPliku, //  + '?callback=?'
                    url: 'http://localhost:8888/' + _katalog + '/' + _nazwaPliku + '?callback=?', //  
                    error: function (xhr, status) {
                        jsonError = true;
                        jsonBadfiles.push(_katalog + '/' + _nazwaPliku + ' - ' + status);
                        console.log('error');
                        console.log(jsonBadfiles);
                        wyswietlAlarmPobraniaPlikow();
                    },
                    complete: function (xhr, status) {},
                    success: function (json) {
                        if ((typeof json) === "string") { // nie przyszla struktura jsona -> blad pobrania pliku po ftp ze sterownika przez serwer www
                            jsonError = true;
                            jsonBadfiles.push(json);
                            console.log('string');
                            //console.log(jsonBadfiles);
                            wyswietlAlarmPobraniaPlikow();
                        } else if (json.error) {
                            jsonError = true;
                            jsonBadfiles.push(_katalog + '/' + _nazwaPliku + ' - ' + json.error + ', ' + json.desc);
                            console.log('json.error');
                            //console.log(jsonBadfiles);
                            wyswietlAlarmPobraniaPlikow();
                        } else { // plik ze zterownika ok
                            switch (_nazwaPliku) {
                            case 'hardware.json':
                                varGlobal.hardware = json;
                                switch (varGlobal.hardware.ip) {
                                case '192.168.3.31':
                                    varGlobal.hardware.czyMinimumViz = false;
                                    break;
                                case '192.168.3.51':
                                    if (window.screen.width < 500) { // beagle ma rozdzielczość 480x277
                                        varGlobal.hardware.czyMinimumViz = true;
                                    } else {
                                        varGlobal.hardware.czyMinimumViz = false;
                                    }
                                    break;
                                default:
                                    if (window.screen.width < 500) { // DO TESTÓW NA LAPTOPIE
                                        varGlobal.hardware.czyMinimumViz = true;
                                    } else {
                                        varGlobal.hardware.czyMinimumViz = false;
                                    }
                                }

                                if (!jsonError) { // wszystkie pliki json ok
                                    uruchomWizualizacje();
                                } else { // błąd pobrania plików json
                                    console.log('Wizualizacja nie uruchomiona - uszkodzone pliki json');
                                    setTimeout(function () {
                                        jsonError = false; // nastąpi ponowne pobranie plików - tym razem z katalogu jsonDefault
                                        jsonBadfiles = [];
                                        infoZlyPlik.remove();
                                        infoZlyPlik = null;
                                        var infoUstawieniaDomyslne = new AlertKM({
                                            id: 'idUstawieniaDomyslne',
                                            type: 'warning',
                                            texts: ['Przyjmuję ustawienia domyślne']
                                        });
                                        infoUstawieniaDomyslne.render();
                                        setTimeout(function () {
                                            pobierz('parametry.json', 'jsonDefault');
                                            pobierz('sygnaly.json', 'jsonDefault');
                                            pobierz('komunikaty.json', 'jsonDefault');
                                            pobierz('diagnostykaBlokow.json', 'jsonDefault');
                                            pobierz('konfiguracja.json', 'jsonDefault');
                                            pobierz('hardware.json', 'json');
                                            infoUstawieniaDomyslne.updateTexts(['Przyjęto ustawienia domyślne  - ograniczona funkcjonalność!']);
                                        }, 3000);
                                    }, 3000);
                                }
                                break;
                            case 'parametry.json':
                                varGlobal.parametry = json;
                                break;
                            case 'komunikaty.json':
                                varGlobal.tekstyKomunikatow = json;
                                break;
                            case 'diagnostykaBlokow.json':
                                varGlobal.diagnostykaBlokow = json;
                                break;
                            case 'sygnaly.json':
                                varGlobal.sygnaly = json;
                                break;
                            case 'konfiguracja.json':
                                varGlobal.danePlikuKonfiguracyjnego = json;
                                break;
                            default:
                                console.log('default');
                                break;
                            }
                        }
                    }
                });
            },


            pobierzPlikiJson = function () {
                pobierz('parametry.json', 'json'); // w sumie tego można nie pobierać bo paczka przychodzi po gpar ale warto sprawdzić czy pliki są prawidłowe
                pobierz('sygnaly.json', 'json');
                pobierz('komunikaty.json', 'json');
                pobierz('diagnostykaBlokow.json', 'json');
                pobierz('konfiguracja.json', 'json');
                pobierz('hardware.json', 'json');
            },


            inicjacja = function () {

                //alert('inicjacja');
                require(['socketio'], function (_io) { // sprawdzenie czy serwer już wstał
                    var socket = _io('http://localhost:8888');
                    //alert('require - socketIO');

                    socket.on('gpar', function (_obj) { // ten sygnał przychodzi od razu gdy serwer połączy się z plc
                        //console.log('socket on gpar 1');
                        //alert('socket on gpar 1');
                        if (!initSocketIo) {
                            //console.log('socket on gpar 2');
                            //alert('socket on gpar 2');
                            initSocketIo = true;
                            varGlobal.parametry = _obj;
                            console.log('socket.io - app.js - gpar');
                            pobierzPlikiJson();

                            if (infoSerwerOffline !== null) {
                                //console.log('socket on gpar 3');
                                //alert('socket on gpar 3');
                                infoSerwerOffline.remove();
                                infoSerwerOffline = null;
                            }
                            clearInterval(intervalId);
                            //console.log('socket on gpar 4');
                            //alert('socket on gpar 4');
                            socket = null;
                        }
                    });

                    // potrzebne po odświeżeniu przeglądarki - gpar przychodzi tylko przy starcie lub po zmianie parametru
                    //                    socket.on('actPar', function (_obj) {
                    //                        console.log('socket on actPar 1');
                    //                        if (!initSocketIo) {
                    //                            console.log('socket on actPar 2');
                    //                            initSocketIo = true;
                    //                            varGlobal.parametry = _obj;
                    //                            console.log('socket.io - app.js - actPar');
                    //                            initSocketIo = true;
                    //                            pobierzPlikiJson();
                    //
                    //                            if (infoSerwerOffline !== null) {
                    //                                console.log('socket on actPar 3');
                    //                                infoSerwerOffline.remove();
                    //                                infoSerwerOffline = null;
                    //                            }
                    //                            clearInterval(intervalId);
                    //                            console.log('socket on actPar 4');
                    //                            socket = null;
                    //                        }
                    //                    });

                    // cykliczne sprawdzanie czy serwer już wstał
                    //intervalId = setInterval(function () {
                    //socket.emit('getPar'); // potrzebne po F5                         
                    if (!initSocketIo && infoSerwerOffline === null) {
                        infoSerwerOffline = new AlertKM({
                            id: 'idCzekamNaserwer',
                            type: 'warning',
                            texts: ['Czekam na serwer ...']
                        });
                        infoSerwerOffline.render();
                    }
                    //}, 500);
                });
            };



        return {
            inicjacja: inicjacja
        };


    });