/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

// Stworzenie listy z wszystkimi parametrami --> kontrolka jquery menu
define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    "use strict";

    var ccc,
        idDialog = "#idDialogPrametrySzybkie", // tak będzie nazwane okienko popup,
        idButtonPowrotMenuGlowne, // po zamknięciu caego okienka dialog powrot na button w zakladce tab ustawienia
        idButtonPowrot,

        zamknij = function () {
            $(idButtonPowrotMenuGlowne).addClass("kopex-selected").addClass(varGlobal.ui_state); // Powrot nawigacji na button
            $(idDialog).dialog('close');
        },


        startEdycjiParametru = function (selected) {
            selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
            idButtonPowrot = '#' + $(selected).attr("id");
        },


        koniecEdycjiParametru = function () {
            $(idButtonPowrot).addClass("kopex-selected").addClass(varGlobal.ui_state);
        },


        inicjacja = function (idButton) {
            var div,
                fragMenu2 = document.createDocumentFragment();

            idButtonPowrotMenuGlowne = '#' + idButton;
            if ($(idDialog).length === 0) { // sprawdzenie czy div już nie istnieje
                div = document.createElement("div");
                $(div)
                    .addClass('OknaDialog')
                    .addClass('ui-corner-all')
                    .css({
                        'padding': '2em',
                        'margin': '1em'
                    })
                    .attr('id', idDialog.replace("#", ""));
                $('body').append(div);

                $(idDialog).dialog({
                    modal: true,
                    closeOnEscape: false,
                    autoOpen: false,
                    height: "auto",
                    width: '90%',
                    title: $(idButtonPowrotMenuGlowne).text()
                });

                require(['wspolne/dodajMenu2'], function (dodajMenu2) {
                    fragMenu2 = dodajMenu2.dodajElementyHtml(varGlobal.danePlikuKonfiguracyjnego.MENU_PARSZYBKIE[0].zawartosc, 'przyciskMenuParametrySzybkie');
                    $(idDialog).append(fragMenu2);
                    $(idDialog).dialog("open");
                    $("button").button(); // Nadanie stylu jquery
                    $(idDialog).children().first().addClass("kopex-selected").addClass(varGlobal.ui_state); // Skierowanie nawigacji z klawiatury na nowo stworzone elementy submenu
                    dodajMenu2.allignVertical(idDialog); // wyrównanie buttonów w osi Y   

                    $(idDialog).dialog("option", "position", {
                        my: "center",
                        at: "center",
                        of: window
                    });
                });
            }

            $(idDialog).one("dialogclose", function (event, ui) { // oczekiwanie na zdarzenie zamknięcia okienka
                $(idDialog).remove();
            });

        };


    return {
        inicjacja: inicjacja,
        zamknij: zamknij,
        startEdycjiParametru: startEdycjiParametru,
        koniecEdycjiParametru: koniecEdycjiParametru
    };

});