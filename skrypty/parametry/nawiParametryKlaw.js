/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    'use strict';


    var aktualnaWartoscKlaw = '',

        wykonaj = function (kod, selected) {
            //console.log('klaw wykonaj');
            
            $('#keyboard').keyup(function (event) {
                event.preventDefault();
                event.stopPropagation();
            });

            $('#keyboard').focus();
            var keyBrd = $('#keyboard').getkeyboard();

            switch (kod) {
            case varGlobal.kodyKlawiszy.lewo:
                $('#keyboard').trigger('navigate', "left");
                break;
            case varGlobal.kodyKlawiszy.prawo:
                $('#keyboard').trigger('navigate', "right");
                break;
            case varGlobal.kodyKlawiszy.gora:
                $('#keyboard').trigger('navigate', "up");
                break;
            case varGlobal.kodyKlawiszy.dol:
                $('#keyboard').trigger('navigate', "down");
                break;
            case varGlobal.kodyKlawiszy.enter:
                $('#keyboard').trigger('navigate', "enter"); // Nawigacja po wirtualnej klawiaturze
                break;

                // wartości dodawane na klawiature z ramki tcp
            case 48: // 0
                keyBrd.insertText('0');
                break;
            case 49: // 1
                keyBrd.insertText('1');
                break;
            case 50: // 2
                keyBrd.insertText('2');
                break;
            case 51: // 3
                keyBrd.insertText('3');
                break;
            case 52: // 4
                keyBrd.insertText('4');
                break;
            case 53: // 5
                keyBrd.insertText('5');
                break;
            case 54: // 6
                keyBrd.insertText('6');
                break;
            case 55: // 7
                keyBrd.insertText('7');
                break;
            case 56: // 8
                keyBrd.insertText('8');
                break;
            case 57: // 9
                keyBrd.insertText('9');
                break;

            case varGlobal.kodyKlawiszy.escape:
                if ($('#DialogEdycjaParametru').dialog("isOpen")) {
                    require(['parametry/edycjaParametru'], function (edycja) {
                        edycja.zamknijKlawiature();
                    });
                }

                if ($('#DialogEdycjaRozkazu').length > 0) {
                    require(['rozkazy/edytuj'], function (edytuj) {
                        edytuj.zamknij();
                    });
                }

//                if ($('#idDialogPrametrySzybkie').length > 0) {
//                    require(['parametry/szybkieParametry'], function (szybkieParametry) {
//                        szybkieParametry.koniecEdycjiParametru();
//                    });
//                }
                break;

            default:

            }
            keyBrd = null;
        };

    return {
        wykonaj: wykonaj
    };
});