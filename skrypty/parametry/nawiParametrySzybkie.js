/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne', 'klawiatura/nawiWspolne'], function ($, varGlobal, nawiWspolne) {
    'use strict';


    var zezwolenie,

        wykonaj = function (kod, selected) {
            var id = $(selected).attr("id");
            //console.log(selected);

            selected.blur();

            switch (kod) {
            case varGlobal.kodyKlawiszy.gora:
                if (selected.prev().length === 0) {
                    selected.parent().find(".przyciskMenuParametrySzybkie").last().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.prev().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            case varGlobal.kodyKlawiszy.dol:
                if (selected.next().length === 0) {
                    selected.parent().find(".przyciskMenuParametrySzybkie").first().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.next().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            case varGlobal.kodyKlawiszy.enter:
                //console.log('varGlobal.kodyKlawiszy.enter');

                zezwolenie = true;
                switch (varGlobal.poziomDostepu) { // Sprawdzenie jaki aktualnie jest ustawiony poziom dostepu
                case 'Brak':
                    if ($(selected).hasClass('Srvc')) {
                        nawiWspolne.brakDostepuDoButtona(selected);
                        zezwolenie = false;
                    }
                    break;
                case 'User':
                case 'User2':
                    if ($(selected).hasClass('Srvc')) {
                        nawiWspolne.brakDostepuDoButtona(selected);
                        zezwolenie = false;
                    }
                    break;
                }

                if (!zezwolenie) {
                    setTimeout(function () {
                        require(['poziomDostepu/main'], function (main) {
                            main.otworzMenu();
                        });
                    }, 1000);
                    return;
                }

                require(['parametry/szybkieParametry'], function (szybkieParametry) {
                    szybkieParametry.startEdycjiParametru(selected);
                });
                require(['parametry/edycjaParametru'], function (edycjaParametru) {
                    edycjaParametru.inicjacja2(id);
                });
                break;

            case varGlobal.kodyKlawiszy.escape:
                require(['parametry/szybkieParametry'], function (szybkieParametry) {
                    szybkieParametry.zamknij();
                });
                break;

            default:

            }
        };

    return {
        wykonaj: wykonaj
    };
});