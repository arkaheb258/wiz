/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require*/

define(['zmienneGlobalne', 'socketio', 'alert2'], function (varGlobal, _io, AlertKM) {
    'use strict';

    var i,
        timeoutIdNodexVisu = 0,
        timeoutIdNodexPLC = 0,
        timeoutIdNoNewData = 0,
        poprzedniaRamka,
        infoBrakPolTcp = null,
        init = false,
        daneDiag = {
            sID: '',
            DigitData: [],
            AnalogData: []
        },
        socket = _io('http://localhost:8888'),


        policzKomunikaty = function () {
            var ccc,
                sprawdzWordBlokad = function (val) {
                    var i,
                        licznik = 0,
                        maska = 1;

                    for (i = 0; i < 16; i += 1) {
                        if (val & maska) {
                            licznik += 1; // Licznik zalozonych blokad
                        }
                        maska = maska << 1;
                    }
                    return licznik;
                },
                sprawdzWordKomunikatow = function (val, inx) {
                    var i,
                        maska = 1;

                    for (i = 0; i < 16; i += 1) {
                        if (val & maska) {
                            if (varGlobal.daneTCP.MesgType[inx] & maska) { //Sprawdzenia czy mamy do czynienia z alarmem czy ostrzezeniem MESG_TYPE: 0-alarm, 1-ostrzezenie
                                varGlobal.komunikaty.ostrz += 1;
                            } else {
                                varGlobal.komunikaty.alarmy += 1;
                            }
                        }
                        maska = maska << 1;
                    }
                };



            varGlobal.blokady.zalUser = 0; // Liczniki ilosci aktywnych blokad
            varGlobal.blokady.zalSrvc = 0;
            varGlobal.blokady.zalAdv = 0;

            $.each(varGlobal.daneTCP.BlockUsr, function (index, value) {
                varGlobal.blokady.zalUser += sprawdzWordBlokad(value);
            });
            $.each(varGlobal.daneTCP.BlockSrvc, function (index, value) {
                varGlobal.blokady.zalSrvc += sprawdzWordBlokad(value);
            });
            $.each(varGlobal.daneTCP.BlockAdv, function (index, value) {
                varGlobal.blokady.zalAdv += sprawdzWordBlokad(value);
            });

            varGlobal.komunikaty.alarmy = 0;
            varGlobal.komunikaty.ostrz = 0;
            $.each(varGlobal.daneTCP.Mesg, function (index, value) {
                sprawdzWordKomunikatow(value, index);
            });
            //console.log(varGlobal.komunikaty.alarmy);
        },


        wyzerujRamkeTCP = function () { // wyzerowanie ramki po stracie polaczenia
            var i;

            //console.log('zeruje ramke');
            for (i = 0; i < varGlobal.daneTCP.Analog.length; i += 1) {
                if (varGlobal.daneTCP.Analog[i] !== undefined) {
                    varGlobal.daneTCP.Analog[i] = 0;
                }
            }
            for (i = 0; i < varGlobal.daneTCP.Bit.length; i += 1) {
                if (varGlobal.daneTCP.Bit[i] !== undefined) {
                    varGlobal.daneTCP.Bit[i] = 0;
                }
            }
            for (i = 0; i < varGlobal.daneTCP.Mesg.length; i += 1) {
                if (varGlobal.daneTCP.Mesg[i] !== undefined) {
                    varGlobal.daneTCP.Mesg[i] = 0;
                }
            }
            varGlobal.komunikaty.alarmy = 0;
            varGlobal.komunikaty.ostrz = 0;
        },


        pokazPlanszeBrakPolaczenia = function (teksty) {
            if (infoBrakPolTcp === null) {
                infoBrakPolTcp = new AlertKM({
                    id: 'idBrakPolTcp',
                    position: 'center',
                    height: '92%',
                    width: '95%',
                    opacity: '0.95',
                    title: 'BRAK POŁĄCZENIA',
                    type: 'info',
                    texts: teksty
                });
                infoBrakPolTcp.render();
            } else {
                infoBrakPolTcp.updateTexts(teksty);
            }
        },



        pobierzDane = function () { // rejestracja trzech zdarzeń do obsługi połączenia z socketem.io
            var length,
                timeout = 2000,
                startKomunikacji = false,
                timeoutInit;

            // przypadek gdy serwer nie zainicjuje komunikacji (nie odpali żadnego ze zdarzeń)
            setTimeout(function () {
                if (!startKomunikacji) {
                    pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP1, 'Brak inicjacji komunikacji z serwerem node']); // ""Monitor  ---x---  Node  -------  PLC1"
                }
            }, 5000);






            //  _ __ ___  ___ ___  _ __  _ __   ___  ___| |_ 
            // | '__/ _ \/ __/ _ \| '_ \| '_ \ / _ \/ __| __|
            // | | |  __/ (_| (_) | | | | | | |  __/ (__| |_ 
            // |_|  \___|\___\___/|_| |_|_| |_|\___|\___|\__|
            socket.on('reconnecting', function () {
                console.log('socket.io - reconnecting');
                pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP1, 'socket.io - reconnecting']); // ""Monitor  ---x---  Node  -------  PLC1"
                setTimeout(function () { // uzyskanie migania reconnecting/disconnected
                    pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP1, 'socket.io - disconnected']); // ""Monitor  ---x---  Node  -------  PLC1"
                }, 1000);
            });
            socket.on('connect', function () {
                console.log('socket.io - connected');
            });
            socket.on('disconnect', function () {
                console.log('socket.io - disconnected');
                setTimeout(function () {
                    wyzerujRamkeTCP();
                    pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP1, 'socket.io - disconnected']); // ""Monitor  ---x---  Node  -------  PLC1"
                }, timeout);
            });


            //            __               _     
            //  _ __ ___ / _|_ __ ___  ___| |__  
            // | '__/ _ \ |_| '__/ _ \/ __| '_ \ 
            // | | |  __/  _| | |  __/\__ \ | | |
            // |_|  \___|_| |_|  \___||___/_| |_|                               
            socket.on('refresh', function (_obj) {
                location.reload();
            });


            //   __ _ _ __   __ _ _ __ 
            //  / _` | '_ \ / _` | '__|
            // | (_| | |_) | (_| | |   
            //  \__, | .__/ \__,_|_|   
            //  |___/|_|
            socket.on('gpar', function (_obj) {
                console.log('socketIO - gpar - zmiana parametrow');
                varGlobal.parametry = _obj;
                //socket.emit('getDefPar'); // prośba o przysłanie DEFAULTOWEJ struktury plików parametry.json oraz sygnaly.json
                //socket.emit('getDefSyg');
                socket.emit('getPar'); // prośba o przysłanie aktualnej struktury plików parametry.json oraz sygnaly.json
                socket.emit('getSyg');
                require(['parametry/odswiez'], function (odswiez) {
                    odswiez.przeladuj(); // odświeżenie listy parametrów
                });
            });
            socket.on('actPar', function (_obj) { // actual
                console.log('socketIO - actPar');
                varGlobal.parametry = _obj;
            });
            socket.on('actSyg', function (_obj) { // actual
                console.log('socketIO - actSyg');
                varGlobal.sygnaly = _obj;
            });
            socket.on('defPar', function (_obj) { // default
                console.log('socketIO - defPar - pobrano parametry domyślne');
                varGlobal.parametry = _obj;
            });
            socket.on('defSyg', function (_obj) { // default
                console.log('socketIO - defSyg - pobrano sygnały domyślne');
                varGlobal.sygnaly = _obj;
            });


            //      _ _                             _         _         
            //   __| (_) __ _  __ _ _ __   ___  ___| |_ _   _| | ____ _ 
            //  / _` | |/ _` |/ _` | '_ \ / _ \/ __| __| | | | |/ / _` |
            // | (_| | | (_| | (_| | | | | (_) \__ \ |_| |_| |   < (_| |
            //  \__,_|_|\__,_|\__, |_| |_|\___/|___/\__|\__, |_|\_\__,_|
            //                |___/                     |___/         
            socket.on('daneDiag', function (obj) {
                //console.log(obj);
                daneDiag.DigitData = obj.DigitData;
                daneDiag.AnalogData = obj.AnalogData;
            });


            //      _                  
            //   __| | __ _ _ __   ___ 
            //  / _` |/ _` | '_ \ / _ \
            // | (_| | (_| | | | |  __/
            //  \__,_|\__,_|_| |_|\___|
            socket.on('dane', function (_obj) {
                startKomunikacji = true;

                //console.log(_obj);

                // Brak danych z PLC do serwera node
                if ((_obj.error) || _obj.ERROR) {
                    if (!timeoutInit) {
                        timeoutInit = true;
                        timeoutIdNodexPLC = setTimeout(function () { // inicjacja okienka informującego o błędzie z opóźnieniem
                            wyzerujRamkeTCP();
                            pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP2, _obj.error]); // ""Monitor  -------  Node  ---x---  PLC1"
                        }, timeout);
                    } else { // aktualizacja wyświetlanych komunikatów z obiektu _obj.error)
                        if (infoBrakPolTcp !== null) {
                            pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP2, _obj.error]); // ""Monitor  -------  Node  ---x---  PLC1"
                        }
                    }
                } else { // powróciła poprawna komunikacja
                    timeoutInit = false;
                    clearTimeout(timeoutIdNodexPLC);
                    setTimeout(function () {
                        if (infoBrakPolTcp !== null) {
                            infoBrakPolTcp.remove();
                            infoBrakPolTcp = null;
                        }
                    }, 2000);
                }

                // Nie przychodzi _obj.error tylko po prostu dane w ogóle przestają dochodzić z node a nie było zdarzenia disconnect
                clearTimeout(timeoutIdNoNewData);
                timeoutIdNoNewData = setTimeout(function () { // brak danych przychodzących z PLC, połączenie z serwerem www ok
                    if (socket.connected) { // jeżeli nastąpiło wcześniej zdarzenie disconnect socketa to nie nadpisuj tej informacji
                        wyzerujRamkeTCP();
                        pokazPlanszeBrakPolaczenia([varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolTCP1, 'Brak danych z serwera Node']); // ""Monitor  ---x---  Node  -------  PLC1"

                    }
                }, timeout + 3000);

                //Obsługa poprawnych danych standardowych
                if (_obj.error === undefined && _obj.Analog !== undefined) {
                    //console.log(_obj);

                    //                    varGlobal.daneTCP.TimeStamp_s = _obj.TimeStamp_s;
                    //                    varGlobal.daneTCP.TimeStamp_ms = _obj.TimeStamp_ms;
                    //                    varGlobal.daneTCP.TimeStamp_js = _obj.TimeStamp_js;
                    //                    varGlobal.daneTCP.Analog = _obj.Analog;
                    //                    varGlobal.daneTCP.Bit = _obj.Bit;
                    //                    varGlobal.daneTCP.Mesg = _obj.Mesg;
                    //                    varGlobal.daneTCP.MesgType = _obj.MesgType;
                    //                    varGlobal.daneTCP.MesgStatus = _obj.MesgStatus;
                    //                    varGlobal.daneTCP.BlockUsr = _obj.BlockUsr;
                    //                    varGlobal.daneTCP.BlockSrvc = _obj.BlockSrvc;
                    //                    varGlobal.daneTCP.BlockAdv = _obj.BlockAdv;

                    varGlobal.daneTCP = _obj;

                    policzKomunikaty();
                }


            });

        },


        inicjacja = function () {
            pobierzDane();

            setInterval(function () {
                //console.log(varGlobal.daneTCP.Bit);
            }, 1000);

        };




    return {
        inicjacja: inicjacja,
        daneDiag: daneDiag,
        socket: socket
    };
});



//            socket.on 'connect', - > console.log 'connected'
//            socket.on 'reconnect', - > console.log 'reconnect'
//            socket.on 'connecting', - > console.log 'connecting'
//            socket.on 'reconnecting', - > console.log 'reconnecting'
//            socket.on 'connect_failed', - > console.log 'connect failed'
//            socket.on 'reconnect_failed', - > console.log 'reconnect failed'
//            socket.on 'close', - > console.log 'close'
//            socket.on 'disconnect', - > console.log 'disconnect'