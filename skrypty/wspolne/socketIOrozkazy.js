/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require */


define(['jquery', 'zmienneGlobalne', 'socketio', 'progresBar'], function ($, varGlobal, _io, progresBar) {
    'use strict';



    var init = false,
        socket = _io('http://localhost:8888'),

        zarzadzaniePlikami = function (_ioOn) { //{"ioEmit": "zarzadzaniePlikami", "ioMess": "backupTinyCore", "ioOn": "zarzadzaniePlikamiOdp"}

            socket.on(_ioOn, function (_str) {
                console.log(_str);
                switch (_str.trim()) { // trim() ponieważ odpowiedź przychodzi ze skryptu basha (komenda echo) i na końcu dodawany jest znak entera
                case 'OK': // "ioMess": "jsonZPLC"
                    progresBar.inicjacja({
                        status: 'OK',
                        info: 'Zakończono pobieranie plików'
                    });
                    break;

                case 'backupTinyCoreOK': //"ioMess": "backupTinyCore"
                case 'browserRefreshOK': // "ioMess": "browserRefresh"
                case 'systemRestartOK': //"ioMess": "systemRestart"
                    progresBar.inicjacja({
                        status: 'OK',
                        info: _str
                    });
                    break;

                case 'error':
                    progresBar.inicjacja({
                        status: 'error'
                    });
                    break;

                default:
                    progresBar.inicjacja({
                        info: _str // wszystko inne np. wyjście ze skrypt filetool.sh -b
                    });
                }
            });
        },


        inicjacja = function (_obiekt) {
            console.log(_obiekt);
            socket.emit(_obiekt.ioEmit, _obiekt.ioMess);

            progresBar.inicjacja({
                show: true,
                status: 'sending'
            });

            if (!init) {
                init = true;
                zarzadzaniePlikami("zarzadzaniePlikamiOdp"); // rejestracja możliwych odpowiedzi serwera
            }
        };


    return {
        inicjacja: inicjacja
    };


});