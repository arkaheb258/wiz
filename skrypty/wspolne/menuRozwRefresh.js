/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    "use strict";

    var ccc,


        zmienOpis = function (opis) {
            opis = opis.replace("(", "");
            opis = opis.replace(varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakDostepu, "");
            opis = opis.replace(":", "");
            opis = opis.replace(varGlobal.poziomDostepu, "");
            opis = opis.replace(")", "");
            return opis;
        },


        inicjacja = function (menuId) {
            var menusDisabled = $(menuId).find('.ui-state-disabled'),
                tekst;

            $.each(menusDisabled, function (key, val) {
                if ($(val).hasClass('Srvc') && (varGlobal.poziomDostepu === 'Srvc' || varGlobal.poziomDostepu === 'Adv')) {
                    $('#' + $(val).attr('id')).removeClass('ui-state-disabled');

                    tekst = zmienOpis($('#' + $(val).attr('id')).text());
                    $('#' + $(val).attr('id')).text(tekst);
                }

                if ($(val).hasClass('Adv') && (varGlobal.poziomDostepu === 'Adv')) {
                    $('#' + $(val).attr('id')).removeClass('ui-state-disabled');

                    tekst = zmienOpis($('#' + $(val).attr('id')).text());
                    $('#' + $(val).attr('id')).text(tekst);
                }
            });
        };


    return {
        inicjacja: inicjacja
    };

});