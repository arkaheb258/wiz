/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require*/


define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    'use strict';

    var danePlikuKonfiguracyjnego,
        init = false,
        czyBladPobraniaPliku = false,
        czyUstawieniaDomyslne = false,


        pobierz = function (_nazwaPliku) {
            var pobraneDane;

            $.ajax({
                mimeType: "application/json;charset=UTF-8",
                'async': false,
                url: _nazwaPliku,
                error: function (xhr, status) {
                    czyBladPobraniaPliku = true;
                    console.log("Blad pobrania danych z pliku JSON, xhr:" + xhr.responseText);
                },
                complete: function (xhr, status) {},
                success: function (json) {
                    if ((typeof json) === "string") { // nie przyszla struktura jsona -> blad pobrania pliku po ftp ze sterownika przez serwer www
                        czyBladPobraniaPliku = true;
                        //bladPobraniaPliku(json);
                    } else { // plik ze zterownika ok
                        pobraneDane = json;
                    }
                }
            });
            return pobraneDane;
        },


        wyslij = function (dane) {
            var nowyParametr,
                czyBladWyslaniaJSON = false,
                tekst;

            console.log(dane);

            $.ajax({
                dataType: "jsonp",
                timeout: 15000, // informacja od Tomka Gorskiego ze ma byc taki dlugi timeout
                data: dane,
                url: varGlobal.adresSerweraRozkazy,
                error: function (xhr, status) {
                    require(['progresBar'], function (progresBar) {
                        progresBar.inicjacja({
                            status: 'error', // OK
                            error: "xhr_status:" + xhr.status + ", Ajax status:" + status
                        });
                    });
                },
                complete: function (xhr, status) {
                    //console.log('status: ' + status);
                    if ((status !== 'success') && (status !== 'error')) {
                        require(['progresBar'], function (progresBar) {
                            progresBar.inicjacja({
                                status: 'error',
                                error: "Ajax status: " + status
                            });
                        });
                    }
                },
                success: function (odpowiedz) {
                    //console.log(odpowiedz);
                    if (odpowiedz === 'OK') { // przyszla poprawna odpowiedz dla wiekszosci rozkazow
                        console.log(odpowiedz);
                    } else if (Object.prototype.toString.call(odpowiedz) === "[object Array]") { // przyszla tablica z historia
                        require(['komunikaty/historia'], function (historia) {
                            historia.inicjacja(odpowiedz); // przekaz pobrane dane historii do wyswietlenia
                        });
                    } else if (odpowiedz === 'PAR_OK') { // parametry zostaly zaktualizowane -> mozna przeladowac pliki
                        console.log('PAR_OK');
                        //                        if (!init) {
                        //                            init = true;
                        //                            require(['parametry/odswiez'], function (odswiez) {
                        //                                odswiez.przeladuj(); // odświeżenie listy parametrów
                        //                            });
                        //                        }
                    } else if (odpowiedz === 'BLOK_OK') { // blokada zalozona
                        setTimeout(function () {
                            if ($("#PelnaListaKomm").length > 0) { // w przypadku operacji na pełnej liście komunikatów -> odświeżenie aktualnie wyświetlanego widoku
                                require(['komunikatyPelnaLista/main'], function (pelnaLista) {
                                    pelnaLista.odswiez();
                                });
                            }
                        }, 2000);
                    } else if (odpowiedz === 'EKS_OK') { // blokada zalozona
                        require(['ksiazkaSerwisowa/potwierdzenie'], function (potwierdzenieEKS) {
                            potwierdzenieEKS.odswiezWygladButtona(); // przekaz pobrane dane historii do wyswietlenia
                        });
                    } else { // blad
                        console.log('odpowiedz - blad: ' + odpowiedz);
                        czyBladWyslaniaJSON = true;
                    }

                    // zamknięcie okienek progress bar z informacją o sukcesie lub błędzie
                    require(['progresBar'], function (progresBar) {
                        if (czyBladWyslaniaJSON) {
                            progresBar.inicjacja({
                                //show: true,
                                status: 'error',
                                error: 'Ajax: ' + odpowiedz
                            });
                        } else {
                            progresBar.inicjacja({
                                status: 'OK'
                            });
                        }
                    });

                }
            });
        },


        szukajWartosci = function (_wartoscSzukana, _dane) { //struktura pliku JSON to nazwa:wartosc
            var pasujace = [],
                tempDane = [];

            if (_dane === undefined) {
                tempDane = JSON.parse(JSON.stringify(varGlobal.sygnaly));
            } else {
                tempDane = _dane;
            }

            $.each(tempDane, function (key, val) {
                var aktualnyObiekt = this;
                $.each(aktualnyObiekt, function (k, v) {
                    if ((k === 'grupa') || (k === 'grupa_2') || (k === 'plc_nr') || (k === 'id')) {
                        if (v === _wartoscSzukana) {
                            //aktualnyObiekt.oldVal = undefined;
                            pasujace.push(aktualnyObiekt);
                        }
                    }
                });
            });
            return pasujace;
        },


        szukajWykresow = function (wartoscSzukana, dane) { //struktura pliku JSON to nazwa:wartosc
            var pasujace = []; // Tymczasowa tabela pasujacych obiektow

            $.each(dane, function (key, val) {
                var aktualnyObiekt = this;
                $.each(aktualnyObiekt, function (k, v) {
                    if (k === 'wykresy') {
                        if (v === wartoscSzukana) {
                            if (aktualnyObiekt.id !== undefined) { // W oknie danych diagnostycznych nie chcemy pustych danych    if (aktualnyObiekt.id !== "") {
                                pasujace.push(aktualnyObiekt);
                            }
                        }
                    }
                });
            });
            return pasujace;
        };


//        pobierzAsynchronicznie = function (_nazwaPliku) {
//            var defer = new $.Deferred();
//
//            switch (_nazwaPliku) {
//            case "parametry.json":
//                varGlobal.parametry = pobierz('json/' + _nazwaPliku);
//                break;
//            case 'sygnaly.json':
//                varGlobal.sygnaly = pobierz('json/' + _nazwaPliku);
//                break;
//            case 'konfiguracja.json':
//                varGlobal.danePlikuKonfiguracyjnego = pobierz('json/' + _nazwaPliku);
//                break;
//            case 'konfiguracjaMinViz.json':
//                varGlobal.danePlikuKonfiguracyjnego = pobierz('json/' + 'ktw/' + _nazwaPliku);
//                break;
//            case 'hardware.json':
//                varGlobal.hardware = pobierz('json/' + _nazwaPliku);
//                break;
//            case 'komunikaty.json':
//                varGlobal.tekstyKomunikatow = pobierz('json/' + _nazwaPliku);
//                //console.log(varGlobal.tekstyKomunikatow);
//                break;
//            case 'diagnostykaBlokow.json':
//                varGlobal.diagnostykaBlokow = pobierz('json/' + _nazwaPliku);
//                break;
//            default:
//            }
//
//            if (!czyBladPobraniaPliku) {
//                defer.resolve(true);
//            } else {
//                defer.resolve(false);
//            }
//            return defer;
//        };



    return {
        pobierz: pobierz,
        //pobierzOffline: pobierzOffline,
        wyslij: wyslij,
        szukajWartosci: szukajWartosci,
        szukajWykresow: szukajWykresow
        //pobierzAsynchronicznie: pobierzAsynchronicznie
    };

});