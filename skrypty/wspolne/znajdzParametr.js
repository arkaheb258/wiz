/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define */


define(['jquery', 'zmienneGlobalne'], function ($, varGlobal) {
    'use strict';

    var ccc,


        inicjacja = function (idParametru) {
            var zmienianyParametr = {
                grupa: '',
                podgrupa: '',
                id: '',
                obiekt: null
            };

            zmienianyParametr.id = idParametru;
            $.each(varGlobal.parametry.DANE, function (key, val) {
                zmienianyParametr.grupa = key;
                $.each(val, function (ke, va) {
                    zmienianyParametr.podgrupa = ke;
                    if ((typeof va === 'object')) {
                        $.each(va, function (k, v) {
                            if (k === idParametru) {
                                zmienianyParametr.obiekt = v;
                                //console.log(zmienianyParametr);
                            }
                        });
                    }
                });
            });
            
            
            if (zmienianyParametr.obiekt === null) {
                zmienianyParametr = null;
            }

            return zmienianyParametr;
        };


    return {
        inicjacja: inicjacja
    };


});