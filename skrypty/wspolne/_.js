/*jslint devel: true */
/*jslint nomen: true*/
/*global define, require, module, jQuery */


(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Global
        factory(jQuery);
    }
})(function ($) {
    'use strict';

    $.fn.yourJQueryPlugin = function () {
        // Plugin code here
    };



    // AMD requirement
    //return scrollTo;
});