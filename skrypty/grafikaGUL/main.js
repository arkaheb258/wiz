/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */
/*global  Raphael */


define(['jquery',
        'zmienneGlobalne',
        'paper'
       ], function (
    $,
    varGlobal,
    paper
) {
    "use strict";

    var zezwoleniePracyOrganow = false,
        trwaRysowanieGrafiki = false,
        grupaKombajn,
        imgKorpusGUL,
        imgOrganLewy,
        imgOrganPrawy,
        imgNapedLewy,
        imgNapedPrawy,
        imgRolka,
        imgSekcje,

        przesuniecieOrganLewy,
        przesuniecieOrganPrawy,

        imgPrzekladniaLewa,
        imgPrzekladniaPrawa,
        imgPrzekladniaRolka,

        tekstNapedLewyOpis,
        tekstNapedLewyMoment,
        momentZadNL = 0,
        momentAktNL = 0,

        tekstNapedPrawyOpis,
        tekstNapedPrawyMoment,
        momentZadNP = 0,
        momentAktNP = 0,

        tekstRolkaOpis,
        tekstRolkaMoment,
        momentZadNR = 0,
        momentAktNR = 0,

        tekstOrganPrad,
        tekstOrganTh,
        organThValOld,
        tekstPozycjaRolki,

        pathJazda,
        tekstPredkosc,
        pathLewo,
        pathPrawo,
        pathOrganPrad,
        circleOrganTh,
        wartoscPraduOrganu,

        krancowkaNLzwolnij,
        krancowkaNLstop,
        krancowkaNPzwolnij,
        krancowkaNPstop,

        stycznikM1,
        stycznikT1,
        stycznikNL,
        stycznikNP,
        stycznikNR,
        stycznikSNP,

        xNAM_NLzwolnij = false,
        xNAM_NLstop = false,
        xNAM_NPzwolnij = false,
        xNAM_NPstop = false,
        xEH_NLzwolnij = false,
        xEH_NLstop = false,
        xEH_NPzwolnij = false,
        xEH_NPstop = false,

        tekstWirtualneKrancowki,
        nrSekcjiGULtekst,
        pradGUL,
        nrSekcjiGUL,
        stylKrancowka = {
            fillColor: '#363636',
            strokeColor: 'silver',
            strokeWidth: 2
        },
        potwZalaczenieStycznikow = [],


        odswiezKrancowke = function (_nazwa, _kolor) {
            var ccc,
                ustawStan = function (_nazwaPath) {
                    switch (_kolor) {
                    case 'szary':
                        _nazwaPath.fillColor = '#363636';
                        _nazwaPath.strokeColor = 'silver';
                        break;
                    case 'zielony':
                        _nazwaPath.fillColor = 'green';
                        _nazwaPath.strokeColor = 'lime';
                        break;
                    case 'pomaranczowy':
                        _nazwaPath.fillColor = 'orange';
                        _nazwaPath.strokeColor = 'yellow';
                        break;
                    case 'niebieski':
                        _nazwaPath.fillColor = 'blue';
                        _nazwaPath.strokeColor = 'DodgerBlue';
                        break;
                    case 'czerwony':
                        _nazwaPath.fillColor = 'darkred';
                        _nazwaPath.strokeColor = 'red';
                        break;
                    }
                };

            switch (_nazwa) {
            case 'krancowkaNLzwolnij':
                ustawStan(krancowkaNLzwolnij);
                break;
            case 'krancowkaNLstop':
                ustawStan(krancowkaNLstop);
                break;
            case 'krancowkaNPzwolnij':
                ustawStan(krancowkaNPzwolnij);
                break;
            case 'krancowkaNPstop':
                ustawStan(krancowkaNPstop);
                break;

            case 'PotZalOrgan':
                ustawStan(stycznikM1);
                break;
            case 'PotZalNL':
                ustawStan(stycznikNL);
                break;
            case 'PotZalNP':
                ustawStan(stycznikNP);
                break;
            case 'PotZalNR':
                ustawStan(stycznikNR);
                break;
            case 'PotZalSNP':
                ustawStan(stycznikSNP);
                break;
            case 'PotZalT1':
                ustawStan(stycznikT1);
                break;
            case 'wirtualneKrancowki':
                if (_kolor === 'pokaz') {
                    if (!tekstWirtualneKrancowki.visible) {
                        tekstWirtualneKrancowki.visible = true;
                    }
                }
                if (_kolor === 'ukryj') {
                    if (tekstWirtualneKrancowki.visible) {
                        tekstWirtualneKrancowki.visible = false;
                    }
                }
                break;
            }



        },


        odswiezPozycjeGUL = function (_nowaPozycja) {
            var liczbaSekcjiMin = 6, // rKonfSciany_NrSekcji_NL   rKonfSciany_NrSekcji_NP
                liczbaSekcjiMax = 66, // max liczba sekcji na trasie - to ma być pobrane z parametrów!!!!!!!!!!!!!!!!!!!!
                czyNumeracjaSekcjiOdNL,

                pozXkrancowkaStopNL,
                pozXkrancowkaStopNP,
                dlugoscTrasy, // w pixelach
                dlugoscSekcji, // w pixelach
                pozycjaKombajnu; // w pixelach


            if (varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NL.WART < varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NP.WART) {
                czyNumeracjaSekcjiOdNL = true; // numeracja sekcji zaczyna się od napędu lewego -> NL=1 / NP=np 120
            } else {
                czyNumeracjaSekcjiOdNL = false; // numeracja sekcji zaczyna się od napędu prawego -> NP=1 / NL=np 120
            }

            // znalezienie z parametrów wartości min i max sekcji w ścianie
            liczbaSekcjiMin = Math.min(varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NL.WART,
                varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NP.WART);
            liczbaSekcjiMax = Math.max(varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NL.WART,
                varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_NrSekcji_NP.WART);

            if (_nowaPozycja !== nrSekcjiGUL) { // przerysowanie tylko wtedy gdy nastąpiła zmiana pozycji
                nrSekcjiGUL = _nowaPozycja;

                pozXkrancowkaStopNL = krancowkaNLstop.bounds.x + imgKorpusGUL.bounds.width / 3;
                pozXkrancowkaStopNP = krancowkaNPstop.bounds.x - imgKorpusGUL.bounds.width / 3;
                dlugoscTrasy = pozXkrancowkaStopNP - pozXkrancowkaStopNL;
                dlugoscSekcji = dlugoscTrasy / liczbaSekcjiMax;

                if (czyNumeracjaSekcjiOdNL) {
                    pozycjaKombajnu = pozXkrancowkaStopNL + (_nowaPozycja * dlugoscSekcji); // przesunięcie kombajnu na trasie
                    if (_nowaPozycja < liczbaSekcjiMin) { // wartość poniżej zakresu
                        pozycjaKombajnu = pozXkrancowkaStopNL + (liczbaSekcjiMin * dlugoscSekcji);
                    }
                    if (_nowaPozycja > liczbaSekcjiMax) { // wartość powyżej zakresu
                        pozycjaKombajnu = pozXkrancowkaStopNL + (liczbaSekcjiMax * dlugoscSekcji);
                    }

                } else {
                    pozycjaKombajnu = pozXkrancowkaStopNP - (_nowaPozycja * dlugoscSekcji);
                    if (_nowaPozycja < liczbaSekcjiMin) { // wartość poniżej zakresu
                        pozycjaKombajnu = pozXkrancowkaStopNP - (liczbaSekcjiMin * dlugoscSekcji);
                    }
                    if (_nowaPozycja > liczbaSekcjiMax) { // wartość powyżej zakresu
                        pozycjaKombajnu = pozXkrancowkaStopNP - (liczbaSekcjiMax * dlugoscSekcji);
                    }
                }

                // przesunięcie
                grupaKombajn.position.x = pozycjaKombajnu;
            }
        },


        odswiezCalkeCieplnaM1 = function (_procent) {

            var kolor = 'orange',
                center,
                through,
                to,
                cFrom;


            //            if (parseInt(_procent, 10) === organThValOld) {
            //                return;
            //            } else {
            //                organThValOld = parseInt(_procent, 10);
            //            }
            organThValOld = parseInt(_procent, 10);

            if (_procent <= 0) {
                kolor = 'grey';
            } else if (_procent > 0 && _procent <= 50) {
                kolor = 'green';
            } else {
                kolor = 'orange';
            }

            tekstOrganTh.fillColor = kolor;
            tekstOrganTh.content = 'M1 Th: ' + _procent.toFixed(0) + '%';
            if (wartoscPraduOrganu > 0) {
                circleOrganTh.remove();
                return;
            }

            _procent = _procent / 100; // przeliczenie procentów na potrzeby paper.js
            center = new paper.Point(pathOrganPrad.bounds.center);
            if (circleOrganTh) {
                circleOrganTh.remove();
            }
            if (_procent >= 1) {
                _procent = 1;
            }

            //cFrom = new paper.Point(pathOrganPrad.bounds.top),
            cFrom = new paper.Point(pathOrganPrad.bounds.x + pathOrganPrad.bounds.height / 5, pathOrganPrad.bounds.y + pathOrganPrad.bounds.width / 2);
            through = cFrom.clone().rotate((360 * _procent / 2), center);
            to = cFrom.clone().rotate((360.01 * _procent), center);
            circleOrganTh = new paper.Path.Arc(cFrom, through, to);
            circleOrganTh.strokeColor = kolor;
            circleOrganTh.strokeWidth = 5;
        },


        odswiezAnalog = function (_obiekt, _wartosc) {
            var ccc,
                animacja = function (_nazwaObrazka, _nazwaTekstu) {
                    if (_wartosc > 2) {
                        _nazwaObrazka.rotate(9);
                        _nazwaTekstu.fillColor = 'orange'; // orange   white
                    } else {
                        _nazwaTekstu.fillColor = 'grey';
                    }
                };

            if (_wartosc === undefined) {
                return;
            }

            switch (_obiekt.id) {
            case 'uiISilnikOrganu':
                tekstOrganPrad.content = 'M1: ' + _wartosc.toFixed(1) + ' A';
                wartoscPraduOrganu = _wartosc; // potrzebna zmienna globalna do rysowania całki cieplnej
                if (_wartosc > 0) {
                    tekstOrganPrad.fillColor = 'orange';
                    //imgOrganLewy.rotate(-9);
                    //imgOrganPrawy.rotate(9);

                    pathOrganPrad.rotate(9); // -19
                    if (_wartosc < _obiekt.ana_warn_h) {
                        pathOrganPrad.fillColor = 'green';
                        pathOrganPrad.strokeColor = 'lime';
                    }
                    if ((_wartosc > _obiekt.ana_warn_h) && (_wartosc < _obiekt.ana_alarm_h)) {
                        pathOrganPrad.fillColor = 'darkOrange';
                        pathOrganPrad.strokeColor = 'yellow';
                    }
                    if (_wartosc > _obiekt.ana_alarm_h) {
                        pathOrganPrad.fillColor = 'darkred';
                        pathOrganPrad.strokeColor = 'red';
                    }
                } else {
                    tekstOrganPrad.fillColor = 'grey';
                    pathOrganPrad.style = stylKrancowka;
                }
                break;
            case 'idPradNP':
                tekstNapedPrawyOpis.content = 'NP: ' + _wartosc.toFixed(1) + 'A';
                animacja(imgPrzekladniaPrawa, tekstNapedPrawyOpis);
                break;
            case 'idPradNL':
                tekstNapedLewyOpis.content = 'NL: ' + _wartosc.toFixed(1) + 'A';
                animacja(imgPrzekladniaLewa, tekstNapedLewyOpis);
                break;
            case 'idPradRolka':
                tekstRolkaOpis.content = 'NR: ' + _wartosc.toFixed(1) + 'A';
                animacja(imgPrzekladniaRolka, tekstRolkaOpis);
                break;
            case 'idPozycjaGUL':
                nrSekcjiGULtekst.content = _wartosc + '/' + varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_LiczbaSekcji.WART; // wyświetlenie aktualnej pozycji     rKonfSciany_LiczbaSekcji
                odswiezPozycjeGUL(_wartosc); // przesunięcie obrazka z GUL
                break;
            case 'idPozycjaNapinacza':
                tekstPozycjaRolki.content = _wartosc + '/' + varGlobal.parametry.DANE.grupa11.podgrupa1.rKonfSciany_LiczbaSekcji.WART; // wyświetlenie aktualnej pozycji     rKonfSciany_LiczbaSekcji
                break;
            case 'idPrędkoscGUL':
                tekstPredkosc.content = _wartosc.toFixed(1);
                break;
            case 'iNL_FalSilnikZadanyMoment':
                momentZadNL = _wartosc.toFixed(0);
                break;
            case 'iNL_MomentSilnikaFal':
                momentAktNL = _wartosc.toFixed(0);
                tekstNapedLewyMoment.content = 'M: ' + momentAktNL + ' / ' + momentZadNL + ' %';
                break;
            case 'iNP_FalSilnikZadanyMoment':
                momentZadNP = _wartosc.toFixed(0);
                break;
            case 'iNP_MomentSilnikaFal':
                momentAktNP = _wartosc.toFixed(0);
                tekstNapedPrawyMoment.content = 'M: ' + momentAktNP + ' / ' + momentZadNP + ' %';
                break;
            case 'iNR_FalSilnikZadanyMoment':
                momentZadNR = _wartosc.toFixed(0);
                break;
            case 'iNR_MomentSilnikaFal':
                momentAktNR = _wartosc.toFixed(0);
                tekstRolkaMoment.content = 'M: ' + momentAktNR + ' / ' + momentZadNR + ' %';
                break;
            case 'uiThSilnikOrganu':
                odswiezCalkeCieplnaM1(_wartosc);
                break;

            default:
            }
        },


        odswiezJazde = function (_wartosc) { // 0:stop, 1:lewo, 16:prawo
            switch (_wartosc) {
            case 0:
                tekstPredkosc.fillColor = 'grey';
                pathLewo.fillColor = 'grey';
                pathPrawo.fillColor = 'grey';
                break;
            case 1:
                pathPrawo.fillColor = 'grey';
                tekstPredkosc.fillColor = 'orange';
                pathLewo.fillColor = 'orange';
                break;
            case 16:
                pathLewo.fillColor = 'grey';
                tekstPredkosc.fillColor = 'orange';
                pathPrawo.fillColor = 'orange';
                break;
            }
        },


        inicjacja = function (_kierunekRolki) { // _kierunekRolki  0:prawo, 1:lewo
            var canvas,
                defer = new $.Deferred(), // wywolanie asynchroniczne
                skala = 0.25, // zmniejszenie obrazka z korpusem gula
                punkt,
                tekst,
                pozX,
                pozY,
                styl = {
                    fontFamily: 'Arial',
                    fontWeight: 'normal', // bold , normal
                    fontSize: '1.4em',
                    fillColor: 'grey',
                    justification: 'middle'
                },
                styl2 = {
                    fontFamily: 'Arial',
                    fontWeight: 'normal', // bold , normal
                    fontSize: '1.3em',
                    fillColor: 'grey',
                    justification: 'middle'
                },
                i;


            //_kierunekRolki = 0;
            // raz na jakiś czas grafika źle się rysuje - jest podwójnie wywoływana - nie wiem dlaczego tak się dzieje 
            //(prawdopodobnie jakiś hazard w parametrach), stąd to zabezpieczenie
            if (!trwaRysowanieGrafiki) {
                trwaRysowanieGrafiki = true;
            } else {
                console.log('PRÓBA PONOWNEGO PRZERYSOWANIA GRAFIKI!!!!!!!!!');
                return;
            }

            $('#grafika').empty();
            canvas = document.createElement('canvas');
            $(canvas)
                .attr('id', 'canvas')
                .attr('keepalive', 'true') // to ma być nie zakomentowane tylko podczas testów w biurze!
                .attr('data-paper-keepalive', 'true')
                .width('100%')
                .height('100%')
                .appendTo('#grafika');
            paper.setup(canvas); // Get a reference to the canvas object

            //imgKorpusGUL = new paper.Raster('obrazki/gul_calyBezOrganow1.png');
            imgKorpusGUL = new paper.Raster('obrazki/gul_caly_odZawalu01.png');
            imgKorpusGUL.position = paper.view.center;
            imgKorpusGUL.position.y += paper.view.size.height / 6; // 5
            imgKorpusGUL.scale(skala);
            imgKorpusGUL.onLoad = function () {
                // Numer sekcji na której znajduje się GUL
                punkt = new paper.Point(imgKorpusGUL.bounds.bottomCenter);
                nrSekcjiGULtekst = new paper.PointText(punkt);
                nrSekcjiGULtekst.content = '66/124';
                nrSekcjiGULtekst.style = styl;
                nrSekcjiGULtekst.position.x -= nrSekcjiGULtekst.bounds.width / 2;
                nrSekcjiGULtekst.position.y += nrSekcjiGULtekst.bounds.height * 0.5;
                nrSekcjiGULtekst.fontWeight = 'bold';

                imgOrganLewy = new paper.Raster('obrazki/gul_organLewy2.png');
                imgOrganLewy.position = imgKorpusGUL.bounds.leftCenter;
                imgOrganLewy.position.x += imgKorpusGUL.bounds.width / 8; //  6.75
                imgOrganLewy.scale(skala * 2);
                imgOrganLewy.sendToBack();

                imgOrganPrawy = new paper.Raster('obrazki/gul_organPrawy2.png');
                imgOrganPrawy.position = imgKorpusGUL.bounds.rightCenter;
                imgOrganPrawy.position.x -= imgKorpusGUL.bounds.width / 9;
                imgOrganPrawy.scale(skala * 2);
                imgOrganPrawy.sendToBack();

                // stworzenie grupy do póżniejszego przesuwania kombajnu po trasie
                grupaKombajn = new paper.Group([imgOrganLewy, imgOrganPrawy, imgKorpusGUL, nrSekcjiGULtekst]);

                imgNapedLewy = new paper.Raster('obrazki/zwrotnyLewo01.png');
                punkt = new paper.Point(paper.view.bounds.left + 150, imgKorpusGUL.bounds.y + imgKorpusGUL.bounds.height);
                imgNapedLewy.position = punkt;
                imgNapedLewy.scale(skala * 1.6);
                imgNapedLewy.position.x += imgKorpusGUL.bounds.width / 4;
                imgNapedLewy.sendToBack();
                imgNapedLewy.onLoad = function () {
                    imgPrzekladniaLewa = new paper.Raster('obrazki/gear02.png');
                    imgPrzekladniaLewa.scale(skala * 1.6);
                    imgPrzekladniaLewa.position = imgNapedLewy.bounds.topLeft;
                    imgPrzekladniaLewa.position.x += 20;
                    imgPrzekladniaLewa.position.y -= 20;
                    imgPrzekladniaLewa.onLoad = function () {
                        punkt = new paper.Point(imgPrzekladniaLewa.bounds.rightCenter); // rightCenter   bottomRight
                        tekstNapedLewyOpis = new paper.PointText(punkt);
                        tekstNapedLewyOpis.content = 'NL: 0,0 A';
                        tekstNapedLewyOpis.style = styl;
                        tekstNapedLewyOpis.position.y -= imgPrzekladniaLewa.bounds.height / 15;
                        tekstNapedLewyOpis.fontWeight = 'bold';

                        tekstNapedLewyMoment = new paper.PointText(punkt);
                        tekstNapedLewyMoment.content = 'M: 78 / 100 %';
                        tekstNapedLewyMoment.style = styl;
                        tekstNapedLewyMoment.fontSize = '1.0em';
                        tekstNapedLewyMoment.position.y += imgPrzekladniaLewa.bounds.height / 4;

                        // KRAŃCÓWKI
                        //punkt = new paper.Point(paper.view.bounds.bottomCenter);
                        punkt = new paper.Point(imgNapedLewy.bounds.bottomCenter);
                        krancowkaNLzwolnij = new paper.Path.Circle(punkt, 10);
                        krancowkaNLzwolnij.style = stylKrancowka;
                        krancowkaNLzwolnij.position.y -= 15;
                        tekst = new paper.PointText(new paper.Point(krancowkaNLzwolnij.bounds.topLeft));
                        tekst.style = styl2;
                        tekst.fontSize = '1.0em';
                        tekst.content = 'SK1';
                        tekst.position.y -= krancowkaNLzwolnij.bounds.height / 6;

                        punkt = new paper.Point(imgNapedLewy.bounds.bottomCenter);
                        krancowkaNLstop = new paper.Path.Circle(punkt, 10);
                        krancowkaNLstop.style = stylKrancowka;
                        krancowkaNLstop.position.y -= 15;
                        krancowkaNLstop.position.x -= krancowkaNLstop.bounds.width * 2;
                        tekst = new paper.PointText(new paper.Point(krancowkaNLstop.bounds.topLeft));
                        tekst.style = styl2;
                        tekst.fontSize = '1.0em';
                        tekst.content = 'SK2';
                        tekst.position.y -= krancowkaNLstop.bounds.height / 6;

                        // ROLKA  W LEWO
                        if (_kierunekRolki === 1) {
                            imgPrzekladniaRolka = new paper.Raster('obrazki/gear02.png');
                            imgPrzekladniaRolka.scale(skala * 1.6);
                            imgPrzekladniaRolka.position = imgPrzekladniaLewa.bounds.topCenter;
                            imgPrzekladniaRolka.position.y -= imgPrzekladniaLewa.bounds.height / 2;
                            imgPrzekladniaRolka.onLoad = function () {
                                punkt = new paper.Point(imgPrzekladniaRolka.bounds.rightCenter);
                                tekstRolkaOpis = new paper.PointText(punkt);
                                tekstRolkaOpis.content = 'NR: 0,0 A';
                                tekstRolkaOpis.style = styl;
                                tekstRolkaOpis.position.y -= imgPrzekladniaRolka.bounds.height / 15;
                                tekstRolkaOpis.fontWeight = 'bold';

                                tekstRolkaMoment = new paper.PointText(punkt);
                                tekstRolkaMoment.content = 'M: 78 / 100 %';
                                tekstRolkaMoment.style = styl;
                                tekstRolkaMoment.fontSize = '1.0em';
                                tekstRolkaMoment.position.y += imgPrzekladniaRolka.bounds.height / 4;

                                imgRolka = new paper.Raster('obrazki/rolkaLewo01.png');
                                imgRolka.position = tekstRolkaOpis.bounds.rightCenter;
                                imgRolka.scale(skala / 2);
                                imgRolka.onLoad = function () {
                                    imgRolka.position.x += imgRolka.bounds.width * 1.1;
                                    //zezwoleniePracyOrganow = true; // start animacji dopiero po zaladowaniu obrazkow
                                    defer.resolve(true);

                                    tekstPozycjaRolki = new paper.PointText(new paper.Point(imgRolka.bounds.bottomRight));
                                    tekstPozycjaRolki.style = styl2;
                                    tekstPozycjaRolki.content = '66/666';
                                    tekstPozycjaRolki.fontWeight = 'bold';
                                    tekstPozycjaRolki.position.y += imgRolka.bounds.height / 5;
                                    tekstPozycjaRolki.position.x -= imgRolka.bounds.width / 3;
                                };
                            };
                        }
                    };
                };

                imgNapedPrawy = new paper.Raster('obrazki/wysypPrawo01.png');
                punkt = new paper.Point(paper.view.bounds.right - 150, imgKorpusGUL.bounds.y + imgKorpusGUL.bounds.height);
                imgNapedPrawy.position = punkt;
                imgNapedPrawy.scale(skala * 1.6);
                imgNapedPrawy.position.x -= imgKorpusGUL.bounds.width / 4;
                imgNapedPrawy.sendToBack();
                imgNapedPrawy.onLoad = function () {
                    imgPrzekladniaPrawa = new paper.Raster('obrazki/gear02.png');
                    imgPrzekladniaPrawa.scale(skala * 1.6);
                    imgPrzekladniaPrawa.position = imgNapedPrawy.bounds.topRight;
                    imgPrzekladniaPrawa.position.x -= 20;
                    imgPrzekladniaPrawa.position.y -= 20;
                    imgPrzekladniaPrawa.onLoad = function () {
                        punkt = new paper.Point(imgPrzekladniaPrawa.bounds.leftCenter);
                        tekstNapedPrawyOpis = new paper.PointText(punkt);
                        tekstNapedPrawyOpis.content = 'NP: 0,0 A';
                        tekstNapedPrawyOpis.style = styl;
                        tekstNapedPrawyOpis.position.y -= imgPrzekladniaLewa.bounds.height / 15;
                        tekstNapedPrawyOpis.position.x -= tekstNapedPrawyOpis.bounds.width * 1.2;
                        tekstNapedPrawyOpis.fontWeight = 'bold';

                        tekstNapedPrawyMoment = new paper.PointText(punkt);
                        tekstNapedPrawyMoment.content = 'M: 66 / 100 %';
                        tekstNapedPrawyMoment.style = styl;
                        tekstNapedPrawyMoment.fontSize = '1.0em';
                        tekstNapedPrawyMoment.position.y += imgPrzekladniaPrawa.bounds.height / 4;
                        tekstNapedPrawyMoment.position.x -= tekstNapedPrawyMoment.bounds.width * 1.1;


                        // KRAŃCÓWKI
                        punkt = new paper.Point(imgNapedPrawy.bounds.bottomCenter);
                        krancowkaNPzwolnij = new paper.Path.Circle(punkt, 10);
                        krancowkaNPzwolnij.style = stylKrancowka;
                        krancowkaNPzwolnij.position.y -= 15;
                        tekst = new paper.PointText(new paper.Point(krancowkaNPzwolnij.bounds.topLeft));
                        tekst.style = styl2;
                        tekst.fontSize = '1.0em';
                        tekst.content = 'SK3';
                        tekst.position.y -= krancowkaNPzwolnij.bounds.height / 6;

                        punkt = new paper.Point(imgNapedPrawy.bounds.bottomCenter);
                        krancowkaNPstop = new paper.Path.Circle(punkt, 10);
                        krancowkaNPstop.style = stylKrancowka;
                        krancowkaNPstop.position.y -= 15;
                        krancowkaNPstop.position.x += krancowkaNPstop.bounds.width * 2;
                        tekst = new paper.PointText(new paper.Point(krancowkaNPstop.bounds.topLeft));
                        tekst.style = styl2;
                        tekst.fontSize = '1.0em';
                        tekst.content = 'SK4';
                        tekst.position.y -= krancowkaNPstop.bounds.height / 6;

                        // ROLKA  W PRAWO
                        if (_kierunekRolki === 0) {
                            imgPrzekladniaRolka = new paper.Raster('obrazki/gear02.png');
                            imgPrzekladniaRolka.scale(skala * 1.6);
                            imgPrzekladniaRolka.position = imgPrzekladniaPrawa.bounds.topCenter;
                            imgPrzekladniaRolka.position.y -= imgPrzekladniaPrawa.bounds.height / 2;
                            imgPrzekladniaRolka.onLoad = function () {
                                punkt = new paper.Point(imgPrzekladniaRolka.bounds.leftCenter);
                                tekstRolkaOpis = new paper.PointText(punkt);
                                tekstRolkaOpis.content = 'NR: 66,6 A';
                                tekstRolkaOpis.style = styl;
                                tekstRolkaOpis.position.y += tekstRolkaOpis.bounds.height / 3; // 3
                                tekstRolkaOpis.position.x -= tekstRolkaOpis.bounds.width;
                                tekstRolkaOpis.fontWeight = 'bold';

                                tekstRolkaMoment = new paper.PointText(punkt);
                                tekstRolkaMoment.content = 'M: 78 / 100 %';
                                tekstRolkaMoment.style = styl;
                                tekstRolkaMoment.fontSize = '1.0em';
                                tekstRolkaMoment.position.y += imgPrzekladniaRolka.bounds.height / 3;
                                tekstRolkaMoment.position.x -= tekstRolkaMoment.bounds.width * 1.2;


                                imgRolka = new paper.Raster('obrazki/rolkaPrawo01.png');
                                imgRolka.position = tekstRolkaOpis.bounds.leftCenter;
                                imgRolka.scale(skala / 2);
                                //zezwoleniePracyOrganow = true; // start animacji dopiero po zaladowaniu obrazkow

                                imgRolka.onLoad = function () {
                                    defer.resolve(true);

                                    tekstPozycjaRolki = new paper.PointText(new paper.Point(imgRolka.bounds.bottomLeft));
                                    tekstPozycjaRolki.style = styl2;
                                    tekstPozycjaRolki.content = '66/666';
                                    tekstPozycjaRolki.fontWeight = 'bold';
                                    tekstPozycjaRolki.position.y += imgRolka.bounds.height / 5;
                                };
                            };
                        }
                    };
                };

                imgSekcje = new paper.Raster('obrazki/sekcje01.png');
                punkt = new paper.Point(paper.view.center.x, imgKorpusGUL.bounds.y + imgKorpusGUL.bounds.height);
                imgSekcje.position = punkt;
                imgSekcje.scale(skala * 1.6);
                imgSekcje.sendToBack();

                // PRĘDKOŚĆ I KIERUNEK JAZDY
                var rectJazda,
                    skala2,
                    tekstJednostka,
                    pathLewo1 = 'M21.871,9.814 15.684,16.001 21.871,22.188 18.335,25.725 8.612,16.001 18.335,6.276z',
                    pathPrawo1 = 'M10.129,22.186 16.316,15.999 10.129,9.812 13.665,6.276 23.389,15.999 13.665,25.725z',
                    pathOrganSVG = "M240 521 c0 -24 -33 -36 -60 -21 -26 14 -46 4 -30 -15 14 -17 -15 -35 -54 -35 -27 0 -43 -15 -26 -25 19 -12 10 -35 -17 -48 -38 -16 -46 -30 -23 -39 22 -8 17 -38 -11 -64 -24 -22 -24 -34 0 -34 24 0 36 -33 21 -60 -13 -25 -5 -41 16 -33 19 7 34 -13 34 -48 0 -34 8 -40 32 -26 20 13 21 12 55 -40 11 -17 13 -17 23 -1 12 19 42 14 66 -13 22 -24 34 -24 34 0 0 24 23 33 56 21 31 -12 45 -5 37 16 -7 20 13 34 49 34 29 0 45 15 28 25 -19 12 -10 35 18 48 37 16 45 30 22 39 -22 8 -17 38 11 64 24 22 24 34 0 34 -24 0 -33 23 -21 56 12 31 5 45 -16 37 -19 -7 -34 13 -34 48 0 34 -8 40 -32 26 -17 -10 -22 -9 -32 6 -30 50 -35 53 -46 35 -12 -19 -42 -14 -66 13 -22 24 -34 24 -34 0z",
                    tempPath;

                // stworzenie prostokąta głównego w którym będą umieszczone strzałki w wskaźnik prędkości
                rectJazda = new paper.Rectangle(new paper.Point(paper.view.bounds.topCenter), new paper.Size(paper.view.bounds.width / 3, paper.view.bounds.height / 3.5));
                pathJazda = new paper.Path.RoundRectangle(rectJazda, new paper.Size(10, 10));
                pathJazda.strokeColor = 'grey';
                pathJazda.position.x -= pathJazda.bounds.width / 2;

                // napis z aktualną prędkością
                punkt = new paper.Point(pathJazda.bounds.center); // paper.view.center
                tekstPredkosc = new paper.PointText(punkt);
                tekstPredkosc.position = new paper.Point(pathJazda.bounds.center);
                tekstPredkosc.style = styl;
                tekstPredkosc.fontWeight = 'bold';
                tekstPredkosc.fillColor = 'grey';
                tekstPredkosc.fontSize = '4.1em';
                tekstPredkosc.content = '0.0';
                tekstPredkosc.justification = 'center';
                tekstPredkosc.position.y += pathJazda.bounds.height / 7;

                // napis z jednąstką prędkości (m/min)
                punkt = new paper.Point(pathJazda.bounds.bottomCenter); // paper.view.center
                tekstJednostka = new paper.PointText(punkt);
                tekstJednostka.content = 'm/min';
                tekstJednostka.style = styl;
                tekstJednostka.fillColor = 'grey';
                tekstJednostka.position.y -= pathJazda.bounds.height / 20;
                tekstJednostka.position.x -= tekstJednostka.handleBounds.width / 2;

                // strzałka w lewo
                pathLewo = new paper.CompoundPath(pathLewo1);
                pathLewo.fillColor = 'grey';
                pathLewo.scale(pathJazda.bounds.height / pathLewo.bounds.height - 0.5);
                pathLewo.position = pathJazda.bounds.leftCenter;
                pathLewo.position.x += pathLewo.bounds.width / 1.5;

                // strzałka w prawo
                pathPrawo = new paper.CompoundPath(pathPrawo1);
                pathPrawo.fillColor = 'grey';
                pathPrawo.scale(pathJazda.bounds.height / pathPrawo.bounds.height - 0.5);
                pathPrawo.position = pathJazda.bounds.rightCenter;
                pathPrawo.position.x -= pathPrawo.bounds.width / 1.5;

                // rysunek organu z informacją o prądzie organu
                pathOrganPrad = new paper.CompoundPath(pathOrganSVG);
                pathOrganPrad.style = stylKrancowka;
                skala2 = pathJazda.bounds.height / pathOrganPrad.bounds.height;
                pathOrganPrad.scale(skala2 - (skala2 * 0.5));
                pathOrganPrad.position = pathJazda.bounds.leftCenter;
                pathOrganPrad.position.x = paper.view.bounds.x + pathOrganPrad.bounds.width;

                //tekstOrganPrad
                punkt = new paper.Point(pathOrganPrad.bounds.rightCenter);
                tekstOrganPrad = new paper.PointText(punkt);
                tekstOrganPrad.content = 'M1: 66,6 A';
                tekstOrganPrad.style = styl;
                tekstOrganPrad.position.x += 5;
                //tekstOrganPrad.position.y += pathOrganPrad.bounds.height;
                tekstOrganPrad.fontWeight = 'bold';

                punkt = new paper.Point(pathOrganPrad.bounds.rightCenter);
                tekstOrganTh = new paper.PointText(punkt);
                tekstOrganTh.content = 'Th: 77%';
                tekstOrganTh.style = styl;
                tekstOrganTh.fontSize = '1.0em';
                tekstOrganTh.position.x += 5;
                tekstOrganTh.position.y += pathOrganPrad.bounds.height / 3.5;






                // potwierdzenia załączenia styczników
                punkt = new paper.Point(pathJazda.bounds.rightCenter);
                stycznikM1 = new paper.Path.Circle(punkt, 10);
                stycznikM1.style = stylKrancowka;
                stycznikM1.position.x += stycznikM1.bounds.width * 1.5;
                stycznikM1.position.y -= stycznikM1.bounds.height / 3.5;
                tekst = new paper.PointText(new paper.Point(stycznikM1.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'M1';
                tekst.position.y += stycznikM1.bounds.width;

                tekst = new paper.PointText(new paper.Point(stycznikM1.bounds.topLeft));
                tekst.style = styl2;
                tekst.content = 'Styczniki:';
                //tekst.position.y -= stycznikM1.bounds.width / 2;
                tekst.position.y -= stycznikM1.bounds.width / 3;

                punkt = new paper.Point(stycznikM1.bounds.rightCenter);
                stycznikT1 = new paper.Path.Circle(punkt, 10);
                stycznikT1.style = stylKrancowka;
                stycznikT1.position.x += stycznikT1.bounds.width * 1.5;
                tekst = new paper.PointText(new paper.Point(stycznikT1.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'T1';
                tekst.position.y += stycznikT1.bounds.width;

                punkt = new paper.Point(stycznikT1.bounds.rightCenter);
                stycznikNL = new paper.Path.Circle(punkt, 10);
                stycznikNL.style = stylKrancowka;
                stycznikNL.position.x += stycznikNL.bounds.width * 1.5;
                tekst = new paper.PointText(new paper.Point(stycznikNL.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'NL';
                tekst.position.y += stycznikNL.bounds.width;

                punkt = new paper.Point(stycznikNL.bounds.rightCenter);
                stycznikNP = new paper.Path.Circle(punkt, 10);
                stycznikNP.style = stylKrancowka;
                stycznikNP.position.x += stycznikNP.bounds.width * 1.5;
                tekst = new paper.PointText(new paper.Point(stycznikNP.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'NP';
                tekst.position.y += stycznikNP.bounds.width;

                punkt = new paper.Point(stycznikNP.bounds.rightCenter);
                stycznikNR = new paper.Path.Circle(punkt, 10);
                stycznikNR.style = stylKrancowka;
                stycznikNR.position.x += stycznikNR.bounds.width * 1.5;
                tekst = new paper.PointText(new paper.Point(stycznikNR.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'NR';
                tekst.position.y += stycznikNR.bounds.width;

                punkt = new paper.Point(stycznikNR.bounds.rightCenter);
                stycznikSNP = new paper.Path.Circle(punkt, 10);
                stycznikSNP.style = stylKrancowka;
                stycznikSNP.position.x += stycznikSNP.bounds.width * 1.5;
                tekst = new paper.PointText(new paper.Point(stycznikSNP.bounds.bottomLeft));
                tekst.style = styl2;
                tekst.content = 'SNP';
                tekst.position.y += stycznikSNP.bounds.width;

                // Legenda z krańcówkami
                punkt = new paper.Point(paper.view.bounds.bottomCenter);
                tempPath = new paper.Path.Circle(punkt, 5);
                tempPath.position.x -= tempPath.bounds.width * 5;
                tempPath.position.y -= tempPath.bounds.width / 2;
                tempPath.fillColor = 'green';
                tekst = new paper.PointText(new paper.Point(tempPath.bounds.rightCenter));
                tekst.style = styl2;
                tekst.fontSize = '0.9em';
                tekst.content = 'NAM';
                tekst.position.x += tempPath.bounds.width / 4;
                tekst.position.y += tempPath.bounds.height / 4;

                // tekst z informacją o aktywnych wirtualnych krańcówkach
                tekstWirtualneKrancowki = new paper.PointText(new paper.Point(tempPath.bounds.topRight));
                tekstWirtualneKrancowki.style = styl2;
                tekstWirtualneKrancowki.fontSize = '0.9em';
                tekstWirtualneKrancowki.content = 'Aktywne wirtualne krańcówki!';
                tekstWirtualneKrancowki.position.x -= tempPath.bounds.width * 1.5;
                tekstWirtualneKrancowki.position.y -= tempPath.bounds.height / 2;
                tekstWirtualneKrancowki.fillColor = 'orange';
                tekstWirtualneKrancowki.visible = false;

                punkt = new paper.Point(paper.view.bounds.bottomCenter);
                tempPath = new paper.Path.Circle(punkt, 5);
                tempPath.position.x = tekst.position.x + tekst.bounds.width * 2.2;
                tempPath.position.y -= tempPath.bounds.width / 2;
                tempPath.fillColor = 'orange';
                tekst = new paper.PointText(new paper.Point(tempPath.bounds.rightCenter));
                tekst.style = styl2;
                tekst.fontSize = '0.9em';
                tekst.content = 'EH';
                tekst.position.y += tempPath.bounds.height / 4;

                punkt = new paper.Point(paper.view.bounds.bottomCenter);
                tempPath = new paper.Path.Circle(punkt, 5);
                tempPath.position.x = tekst.position.x + tekst.bounds.width * 2.2;
                tempPath.position.y -= tempPath.bounds.width / 2;
                tempPath.fillColor = 'blue';
                tekst = new paper.PointText(new paper.Point(tempPath.bounds.rightCenter));
                tekst.style = styl2;
                tekst.fontSize = '0.9em';
                tekst.content = 'NAM+EH';
                tekst.position.y += tempPath.bounds.height / 4;




                // odpalenie odświeżania zmiennych
                defer.done(function (czyDOMready) { // Wywolanie asynchroniczne - po załądowaniu wszystkich obrazków
                    if (czyDOMready) {
                        trwaRysowanieGrafiki = false;
                        require(['grafikaGUL/odswiezaj'], function (odswiezaj) { // inicjacja odświeżania danych
                            odswiezaj.inicjacja();
                        });
                    }
                });

            };
        };


    return {
        inicjacja: inicjacja,
        odswiezKrancowke: odswiezKrancowke,
        odswiezAnalog: odswiezAnalog,
        odswiezJazde: odswiezJazde
    };
});

//console.log(event.count); // the number of times the frame event was fired:
//console.log(event.time);  // The total amount of time passed since the first frame event in seconds
//console.log(event.delta);  // The time passed in seconds since the last frame event: