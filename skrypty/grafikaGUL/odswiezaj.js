/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require */


define(['jquery',
        'obslugaJSON',
        'zmienneGlobalne',
        'wspolne/odswiezajObiekt',
        'grafikaGUL/main',
        'paper'
       ], function (
    $,
    json,
    varGlobal,
    odswiezajObiekt,
    grafikaGULmain,
    paper
) {
    "use strict";

    var init = false,
        daneDoOdswiezania = [],
        zezwoleniaStycznikow = [],
        xNAM_NLzwolnij = false,
        xNAM_NLstop = false,
        xNAM_NPzwolnij = false,
        xNAM_NPstop = false,
        xEH_NLzwolnij = false,
        xEH_NLstop = false,
        xEH_NPzwolnij = false,
        xEH_NPstop = false,
        xStycznikM1 = false,
        xStycznikM1zezwolenie = false,
        xStycznikT1 = false,
        xStycznikT1zezwolenie = false,
        xStycznikNL = false,
        xStycznikNLzezwolenie = false,
        xStycznikNP = false,
        xStycznikNPzezwolenie = false,
        xStycznikNR = false,
        xStycznikNRzezwolenie = false,
        xStycznikSNP = false,
        xStycznikSNPzezwolenie = false,
        xWirtualneKrancowkiAktywne = false,


        NLzwolnij_old,
        NLstop_old,
        NPzwolnij_old,
        NPstop_old,
        M1_old,
        T1_old,
        stycznikNL_old,
        stycznikNP_old,
        stycznikNR_old,
        stycznikSNP_old,


        dodajDaneDoOdswiezania = function () {
            if (init === false) {
                daneDoOdswiezania = daneDoOdswiezania.concat(json.szukajWartosci("grafikaTab1Analog", varGlobal.sygnaly));
                daneDoOdswiezania = daneDoOdswiezania.concat(json.szukajWartosci("grafikaTab1Krancowki", varGlobal.sygnaly));
                daneDoOdswiezania = daneDoOdswiezania.concat(json.szukajWartosci("grafikaTab1Jazda", varGlobal.sygnaly));
                daneDoOdswiezania = daneDoOdswiezania.concat(json.szukajWartosci("potwZalStycznikow", varGlobal.sygnaly));
                init = true;
            }
        },


        odswiezaj = function () {
            var i,
                sprawdzStanStycznika = function (zezwolenie, stanStycznika, nazwaStycznika, oldVal) {
                    var kolor;

                    if (zezwolenie) {
                        if (stanStycznika) {
                            kolor = 'zielony';
                        } else {
                            kolor = 'szary';
                        }
                    } else {
                        kolor = 'czerwony';
                    }

                    if (kolor !== oldVal) {
                        grafikaGULmain.odswiezKrancowke(nazwaStycznika, kolor);
                    }
                    
                    return kolor;

                    //                    if (zezwolenie) {
                    //                        if (stanStycznika) {
                    //                            grafikaGULmain.odswiezKrancowke(nazwaStycznika, 'zielony');
                    //                        } else {
                    //                            grafikaGULmain.odswiezKrancowke(nazwaStycznika, 'szary');
                    //                        }
                    //                    } else {
                    //                        grafikaGULmain.odswiezKrancowke(nazwaStycznika, 'czerwony');
                    //                    }
                },
                //                sprawdzStanKrancowki = function (stanNamur, stanEH, nazwaKrancowki) {
                //                    if (!stanNamur && !stanEH) {
                //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'szary');
                //                    } else if (stanNamur && !stanEH) {
                //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'zielony');
                //                    } else if (!stanNamur && stanEH) {
                //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'pomaranczowy');
                //                    } else if (stanNamur && stanEH) {
                //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'niebieski');
                //                    }
                //                },
                sprawdzStanKrancowki = function (stanNamur, stanEH, nazwaKrancowki, oldVal) {
                    var kolor;

                    if (!stanNamur && stanEH) {
                        kolor = 'szary';
                    } else if (stanNamur && stanEH) {
                        kolor = 'zielony';
                    } else if (!stanNamur && !stanEH) {
                        kolor = 'pomaranczowy';
                    } else if (stanNamur && !stanEH) {
                        kolor = 'niebieski';
                    }

                    if (kolor !== oldVal) {
                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, kolor);
                    }

                    return kolor;

                    //                    if (!stanNamur && stanEH) {
                    //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'szary');
                    //                    } else if (stanNamur && stanEH) {
                    //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'zielony');
                    //                    } else if (!stanNamur && !stanEH) {
                    //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'pomaranczowy');
                    //                    } else if (stanNamur && !stanEH) {
                    //                        grafikaGULmain.odswiezKrancowke(nazwaKrancowki, 'niebieski');
                    //                    }
                },
                length;


            paper.view.onFrame = function (event) { //  it is called up to 60 times a second
                if ($('#tabs').tabs("option", "active") !== 0) { // animacje tylko na tab 1
                    return;
                }
                if (event.count % 40 === 0) { // zmniejszenie ilosci klatek 22 standard - 40 zwolnione
                    length = daneDoOdswiezania.length;
                    for (i = 0; i < length; i += 1) {
                        if (daneDoOdswiezania[i].typ_danych === "Bit") {
                            switch (daneDoOdswiezania[i].id) {
                            case 'xNL_KrancowkaZwolnij':
                                xNAM_NLzwolnij = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xNL_KrancowkaStop':
                                xNAM_NLstop = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xNP_KrancowkaZwolnij':
                                xNAM_NPzwolnij = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xNP_KrancowkaStop':
                                xNAM_NPstop = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'idNLzwolnijStatEH':
                                xEH_NLzwolnij = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'idNLstopStatEH':
                                xEH_NLstop = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'idNPzwolnijStatEH':
                                xEH_NPzwolnij = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'idNPstopstatEH':
                                xEH_NPstop = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;


                            case 'xSA_PotZalOrgan':
                                xStycznikM1zezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]); // sygnał diagnostyki zawsze jest od razu za sygnałem technicznym  
                                xStycznikM1 = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xSA_PotZalFalNL':
                                xStycznikNLzezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]);
                                xStycznikNL = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xSA_PotZalFalNP':
                                xStycznikNPzezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]);
                                xStycznikNP = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xSA_PotZalFalNR':
                                xStycznikNRzezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]);
                                xStycznikNR = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xSA_PotZalZasilSNP':
                                xStycznikSNPzezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]);
                                xStycznikSNP = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;
                            case 'xSA_PotZalTranT1':
                                xStycznikT1zezwolenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i + 1]);
                                xStycznikT1 = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                break;

                            case 'idWirtKrancowkiAktywne':
                                if (xWirtualneKrancowkiAktywne !== odswiezajObiekt.typBitStan(daneDoOdswiezania[i])) { // sprawdzenie zmiany wartości
                                    xWirtualneKrancowkiAktywne = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                                    if (xWirtualneKrancowkiAktywne) {
                                        grafikaGULmain.odswiezKrancowke('wirtualneKrancowki', 'pokaz');
                                    } else {
                                        grafikaGULmain.odswiezKrancowke('wirtualneKrancowki', 'ukryj');
                                    }
                                }
                                break;
                            }
                        }

                        if (daneDoOdswiezania[i].typ_danych === "Lista") {
                            if (daneDoOdswiezania[i].id === "idKierunekJazdy") {
                                if (daneDoOdswiezania[i].oldVal !== varGlobal.daneTCP.Analog[daneDoOdswiezania[i].poz_ramka]) { // nastąpiła zmiana wartości
                                    grafikaGULmain.odswiezJazde(odswiezajObiekt.typAnalog(daneDoOdswiezania[i]));
                                    daneDoOdswiezania[i].oldVal = varGlobal.daneTCP.Analog[daneDoOdswiezania[i].poz_ramka];
                                }
                            }
                        }

                        if ((daneDoOdswiezania[i].typ_danych === "Analog") && varGlobal.daneTCP.Analog[daneDoOdswiezania[i].poz_ramka] !== undefined) {
                            if ((daneDoOdswiezania[i].grupa === "grafikaTab1Analog") || (daneDoOdswiezania[i].grupa_2 === "grafikaTab1Analog")) {
                                switch (daneDoOdswiezania[i].id) {
                                case 'idPradNP': // te wartości odświeżamy cały czas
                                case 'idPradNL':
                                case 'idPradRolka':
                                case 'uiISilnikOrganu':
                                    grafikaGULmain.odswiezAnalog(daneDoOdswiezania[i], odswiezajObiekt.typAnalog(daneDoOdswiezania[i]));
                                    break;
                                default: // te wartości odświeżamy tylko po zmianie wartości
                                    if (daneDoOdswiezania[i].oldVal !== varGlobal.daneTCP.Analog[daneDoOdswiezania[i].poz_ramka]) { // nastąpiła zmiana wartości
                                        grafikaGULmain.odswiezAnalog(daneDoOdswiezania[i], odswiezajObiekt.typAnalog(daneDoOdswiezania[i]));
                                        daneDoOdswiezania[i].oldVal = varGlobal.daneTCP.Analog[daneDoOdswiezania[i].poz_ramka];
                                    }
                                    break;
                                }
                            }
                        }
                    } // end for



                    NLzwolnij_old = sprawdzStanKrancowki(xNAM_NLzwolnij, xEH_NLzwolnij, 'krancowkaNLzwolnij', NLzwolnij_old);
                    NLstop_old = sprawdzStanKrancowki(xNAM_NLstop, xEH_NLstop, 'krancowkaNLstop', NLstop_old);
                    NPzwolnij_old = sprawdzStanKrancowki(xNAM_NPzwolnij, xEH_NPzwolnij, 'krancowkaNPzwolnij', NPzwolnij_old);
                    NPstop_old = sprawdzStanKrancowki(xNAM_NPstop, xEH_NPstop, 'krancowkaNPstop', NPstop_old);

                    M1_old = sprawdzStanStycznika(xStycznikM1zezwolenie, xStycznikM1, 'PotZalOrgan', M1_old);
                    T1_old = sprawdzStanStycznika(xStycznikT1zezwolenie, xStycznikT1, 'PotZalT1', T1_old);
                    stycznikNL_old = sprawdzStanStycznika(xStycznikNLzezwolenie, xStycznikNL, 'PotZalNL', stycznikNL_old);
                    stycznikNP_old = sprawdzStanStycznika(xStycznikNPzezwolenie, xStycznikNP, 'PotZalNP', stycznikNP_old);
                    stycznikNR_old = sprawdzStanStycznika(xStycznikNRzezwolenie, xStycznikNR, 'PotZalNR', stycznikNR_old);
                    stycznikSNP_old = sprawdzStanStycznika(xStycznikSNPzezwolenie, xStycznikSNP, 'PotZalSNP', stycznikSNP_old);
                }
            };
        },


        inicjacja = function () {
            dodajDaneDoOdswiezania();
            odswiezaj();
        };


    return {
        inicjacja: inicjacja
    };
});