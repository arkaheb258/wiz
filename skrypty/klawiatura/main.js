/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne', 'obslugaJSON'], function ($, varGlobal, json) {
    'use strict';

    var aktywneKlawiszeOld, // Zapamietany poprzedni stan klawiszy
        aktywneKlawisze,
        odswiezInit = false,
        pasujaceObiekty = [],
        timeoutId,
        initSzybkiePrzew = false,
        init = false,

        klawiszeRamkaPLC = function () { // Przechwycenie zadania sterowania z ramki tcp od sterownika plc
            var i,
                maska = 1,
                length,
                //e = jQuery.Event("keydown"),
                e = jQuery.Event("keyup"),
                tymczasKlawisz,
                tablicaWcisnietychKlawiszy = [],
                wykonajRozkazKlawisza = function (wcisnietyKlawisz) {
                    //console.log(wcisnietyKlawisz);
                    switch (wcisnietyKlawisz) {
                    case 'LEWO':
                        e.which = varGlobal.kodyKlawiszy.lewo;
                        break;
                    case 'PRAWO':
                        e.which = varGlobal.kodyKlawiszy.prawo;
                        break;
                    case 'GORA':
                        e.which = varGlobal.kodyKlawiszy.gora;
                        break;
                    case 'DOL':
                        e.which = varGlobal.kodyKlawiszy.dol;
                        break;
                    case 'ENTER':
                        e.which = varGlobal.kodyKlawiszy.enter;
                        break;
                    case 'ESCAPE':
                        e.which = varGlobal.kodyKlawiszy.escape;
                        break;

                    case 'KLAWISZNUM0': // klawisze numeryczne na wizualizacji właściwie nie są używane
                        e.which = 48;
                        break;
                    case 'KLAWISZF1':
                    case 'KLAWISZNUM1':
                        e.which = 49;
                        break;
                    case 'KLAWISZF2':
                    case 'KLAWISZNUM2':
                        e.which = 50;
                        break;
                    case 'KLAWISZF3':
                    case 'KLAWISZNUM3':
                        e.which = 51;
                        break;
                    case 'KLAWISZF4':
                    case 'KLAWISZNUM4':
                        e.which = 52;
                        break;
                    case 'KLAWISZF5':
                    case 'KLAWISZNUM5':
                        e.which = 53;
                        break;
                    case 'KLAWISZF6':
                    case 'KLAWISZNUM6':
                        e.which = 54;
                        break;
                    case 'KLAWISZF7':
                    case 'KLAWISZNUM7':
                        e.which = 55;
                        break;
                    case 'KLAWISZF8':
                    case 'KLAWISZNUM8':
                        e.which = 56;
                        break;
                    case 'KLAWISZF9':
                    case 'KLAWISZNUM9':
                        e.which = 57;
                        break;

                    default:
                    }

                    //  _            _                               
                    // | |_   _ __  (_)   __ _    __ _    ___   _ __ 
                    // | __| | '__| | |  / _` |  / _` |  / _ \ | '__|
                    // | |_  | |    | | | (_| | | (_| | |  __/ | |   
                    //  \__| |_|    |_|  \__, |  \__, |  \___| |_|   
                    //                   |___/   |___/             
                    $(document).trigger(e); // To zdarzenie bedzie zlapane w module zdarzenia w funkcji przechwycZdarzenieKlawiatury(
                };

            if (init === false) {
                pasujaceObiekty = pasujaceObiekty.concat(json.szukajWartosci("klawiszeLCD", varGlobal.sygnaly));
                init = true;
            }

            tablicaWcisnietychKlawiszy.length = 0;
            aktywneKlawisze = 0;
            length = pasujaceObiekty.length;
            //console.log(varGlobal.daneTCP.Bit);
            for (i = 0; i < length; i += 1) { // Sprawdzenie ktory klawisz zostal wcisniety
                maska = 1;
                maska = maska << pasujaceObiekty[i].poz_bit; // Ustawienie maski na odpowiedniej pozycji
                if (varGlobal.daneTCP.Bit[pasujaceObiekty[i].poz_ramka] & maska) { // Jest jedynka na odpowiedniej pozycji
                    aktywneKlawisze += maska; // Zapamietanie wszystkich jedynek (ktore klawisze zostaly wcisniete)

                    tymczasKlawisz = pasujaceObiekty[i].id.toUpperCase(); // konwersja na duze litery zeby pozbyc sie ewentualnych bledow z json'a
                    tablicaWcisnietychKlawiszy.push(tymczasKlawisz);
                }
            }

            // normalna nawigacja z ramki tcp + szybkie przewijanie
            if (aktywneKlawisze !== aktywneKlawiszeOld) {

                if (aktywneKlawisze !== 0) { // To tylko do najbardziej uperdliwych kontrolek(np selectmenU na planszy z trybem serwisowym dla GULa)
                    varGlobal.typNawigacjiPoEkranach = 1; // 0 - komendy z klawiatury usb,  1 - komendy z ramki tcp
                }
                aktywneKlawiszeOld = aktywneKlawisze;
                wykonajRozkazKlawisza(tymczasKlawisz);
                //                $.each(tablicaWcisnietychKlawiszy, function (key, val) {
                //                    wykonajRozkazKlawisza(val);
                //                });

                clearTimeout(timeoutId);
                initSzybkiePrzew = false;
            } else if ((aktywneKlawisze === aktywneKlawiszeOld) && (aktywneKlawisze !== 0)) { // jest wcisniety dluzej jeden klawisz (nie zero)
                if (!initSzybkiePrzew) { // aktywuj szybkie przewijanie dopiero po dluzszym przytrzymaniu klawisza
                    setTimeout(function () {
                        initSzybkiePrzew = true;
                    }, 500);
                }

                if (initSzybkiePrzew) {
                    timeoutId = setTimeout(function () {
                        clearTimeout(timeoutId);
                        if (tymczasKlawisz === 'LEWO' || tymczasKlawisz === 'PRAWO' || tymczasKlawisz === 'GORA' || tymczasKlawisz === 'DOL') { // szybkie przewijanie na wszystkich klawiszach oprocz enter
                            varGlobal.typNawigacjiPoEkranach = 1; // 0 - komendy z klawiatury usb,  1 - komendy z ramki tcp
                            wykonajRozkazKlawisza(tymczasKlawisz);
                            console.log('szybkie przewijanie ' + tymczasKlawisz);
                        }
                    }, 200);
                }

            } else if ((aktywneKlawisze === aktywneKlawiszeOld) && (aktywneKlawisze !== 0)) { // przychodzą zera (brak wysterowanych klawiszy)
                clearTimeout(timeoutId);
                initSzybkiePrzew = false;
            }
        },


        inicjacja = function () {
            // Ustawienie cyklicznego odswiezania stanu klawiszy przychodzacych z ramki
            //console.log(varGlobal.hardware.czyMinimumViz);
            //if (!varGlobal.hardware.czyMinimumViz) { // sterowanie z ramki tylko na dużym wyświetlaczu
            if (varGlobal.hardware.ip !== '192.168.3.51') { // sterowanie z ramki tylko na dużym wyświetlaczu
                console.log('init komunikacja ramka tcp');
                setInterval(function () {
                    klawiszeRamkaPLC();
                }, varGlobal.czasOdswiezania);
            }
            require(['klawiatura/zdarzenia'], function (zdarzenia) {
                zdarzenia.przechwycZdarzenieKlawiatury();
            });

        };


    return {
        inicjacja: inicjacja
    };
});






// Kombinacja klawiszy - odswiezenie okna przegladarki
//            if (aktywneKlawisze === 5) { // LEWO + GORA
//                if (odswiezInit === false) { // wlaczenie odswiezenia po kilku sekundach (potrzebne po wprowadzeniu sterowania na joystick)
//                    odswiezInit = true;
//                    setTimeout(function () {
//                        if (aktywneKlawisze === 5) { // jesli klawisze sa dalej wcisniete - odswiez
//                            if ($('#DialogGULtrybSerwisowy').length === 0) { // nie działa na trybie serwisowym
//                                location.reload();
//                            }
//                        }
//                        console.log('timer');
//                        odswiezInit = false;
//                    }, 4000);
//                }
//            }