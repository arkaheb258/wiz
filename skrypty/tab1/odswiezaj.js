/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  define, require */

define(['jquery',
        'obslugaJSON',
        'zmienneGlobalne',
        'kommTCP',
        'wspolne/odswiezajObiekt',
        'antykolizja/popUpAntykolizja',
        'komunikaty/uaktualnijStatus',
        //'alert',
        'alert2'
       ], function (
    $,
    json,
    varGlobal,
    dane,
    odswiezajObiekt,
    popUpAntykolizja,
    uaktualnijStatus,
    //alert,
    AlertKM
) {
    "use strict";


    var daneDoOdswiezania = [],
        czyAlarm = false,
        czyOstrzezenie = false,
        plcZalaczonyTrybServ = false,
        czyBrakPolaczeniaPulpit = false,
        notAusWOW = false,
        init = false,
        init2 = false,
        init3 = false,
        wowNotAusZezwolenie = true,
        infoAktywnyNotAusWOW,
        infoBrakPolaczeniaPulpit,
        tempalert,
        tempalert2,


        dodajDaneDoOdswiezania = function (paczkaDanych) {
            daneDoOdswiezania = daneDoOdswiezania.concat(paczkaDanych);
        },



        wylaczTrybSerwisowy = function () {
            setTimeout(function () {
                init2 = false;
                if (!varGlobal.trybSerwisowyAktywny && plcZalaczonyTrybServ) {
                    console.log('ALARM - PLC zgłasza załączony tryb serwisowy!');
                    varGlobal.doWyslania.zalTrybSerwisowy.wID = 0;
                    varGlobal.doWyslania.zalTrybSerwisowy.wWartosc = 0;
                    json.wyslij(varGlobal.doWyslania.zalTrybSerwisowy);
                }
            }, 2000);
        },



        odswiezajTylkoNaTab1 = function (sygnal, kolorLo, kolorHi) {
            if ($('#tabs').tabs("option", "active") !== 0) { // odświeżanie tylko na tab 1
                return;
            } else {
                odswiezajObiekt.typBit(sygnal, kolorLo, kolorHi);
            }
        },


        odswiezajDane = function () {
            var i,
                length;

            // ten warunek nie może występować gdyż potem jest problem z wyświetlaniem okienek o braku połączenia z pulpitem itp
            //                        if ($('#tabs').tabs("option", "active") !== 0) { // odświeżanie tylko na tab 1
            //                            return;
            //                        }

            //console.log(daneDoOdswiezania);
            length = daneDoOdswiezania.length;
            for (i = 0; i < length; i += 1) {

                // ------------------------------
                // dane typu analog
                // ------------------------------
                if (daneDoOdswiezania[i].typ_danych === "Analog") {
                    odswiezajObiekt.typAnalog(daneDoOdswiezania[i]);

                    if (daneDoOdswiezania[i].id === 'idOdblokowanieFalownika') { // GUL
                        if (odswiezajObiekt.typAnalog(daneDoOdswiezania[i]) === 0) {
                            $("." + daneDoOdswiezania[i].id).addClass('ui-state-default');
                            $("." + daneDoOdswiezania[i].id).removeClass('ui-state-error');
                        } else {
                            $("." + daneDoOdswiezania[i].id).removeClass('ui-state-default');
                            $("." + daneDoOdswiezania[i].id).addClass('ui-state-error');
                        }
                    }
                }

                // ------------------------------
                // dane typu lista
                // ------------------------------
                if (daneDoOdswiezania[i].typ_danych === "Lista") {
                    odswiezajObiekt.typLista(daneDoOdswiezania[i]);

                    if (daneDoOdswiezania[i].id === 'idNapinanieWstepneLista') { // GUL
                        if (odswiezajObiekt.typLista(daneDoOdswiezania[i]) === 3) {
                            $("." + daneDoOdswiezania[i].id).removeClass('ui-state-default');
                            $("." + daneDoOdswiezania[i].id).addClass('ui-state-error');
                        } else {
                            $("." + daneDoOdswiezania[i].id).addClass('ui-state-default');
                            $("." + daneDoOdswiezania[i].id).removeClass('ui-state-error');
                        }
                    }
                }

                // ------------------------------
                // dane typu bit
                // ------------------------------
                if (daneDoOdswiezania[i].typ_danych === "Bit") {

                    switch (daneDoOdswiezania[i].id) {
                    case 'sprawnoscAntykolizji': // uszkodzenie antykolizji
                        varGlobal.uszkodzenieAntykolizji = odswiezajObiekt.typBit(daneDoOdswiezania[i], '', 'red');
                        if (varGlobal.uszkodzenieAntykolizji) {
                            popUpAntykolizja.inicjacja(); // wyświetlenia okienka pop up, które trzeba zatwierdzić enterem
                        } else {
                            if ($("#DialogPopUpAntykolizja").length > 0) { // zniszczenie okienka popoup jeśli otwarte
                                $("#DialogPopUpAntykolizja").remove();
                            }
                        }
                        break;

                    case 'idBrakPolaczeniaPulpit':
                        czyBrakPolaczeniaPulpit = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                        if (varGlobal.hardware.ip === '192.168.3.31') { // tylko na monitorze głónym
                            if (czyBrakPolaczeniaPulpit) {
                                if (!init) {
                                    console.log('brak polaczenia z pulpitem');
                                    init = true;
                                    infoBrakPolaczeniaPulpit = new AlertKM({
                                        id: 'idCzekamNaserwer',
                                        texts: [varGlobal.danePlikuKonfiguracyjnego.TEKSTY.brakPolPulpit]
                                    });
                                    infoBrakPolaczeniaPulpit.render();

                                }
                            } else {
                                if (init) {
                                    infoBrakPolaczeniaPulpit.remove();
                                    infoBrakPolaczeniaPulpit = null;
                                    init = false;
                                }
                            }
                        }
                        break;

                    case 'zadzialanieAntykolizji':
                        odswiezajObiekt.typBit(daneDoOdswiezania[i], '', 'red');
                        break;

                    case 'trwaPlukanieFiltra':
                        odswiezajObiekt.typBit(daneDoOdswiezania[i], '', 'red');
                        break;

                    case 'diagnostykaBlokad': // to jest robione po staremu!!!!
                        //odswiezajObiekt.typBit(daneDoOdswiezania[i], '', 'red');
                        break;

                    case 'statusKombajnu': // status alarm
                        czyAlarm = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                        break;

                    case 'statusOstrzezenie': // status ostrzeżenie
                        czyOstrzezenie = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                        uaktualnijStatus.zmienStatusAlarm(czyAlarm, czyOstrzezenie);
                        break;

                    case 'idPolaczenieKES': // GUL
                        odswiezajObiekt.typBit(daneDoOdswiezania[i], 'red', '');
                        break;

                    case 'idTrybSerwisowyZalaczony': // GUL
                        plcZalaczonyTrybServ = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]);
                        //console.log(varGlobal.trybSerwisowyAktywny + ' ' + plcZalaczonyTrybServ);
                        if (!varGlobal.trybSerwisowyAktywny && plcZalaczonyTrybServ) {
                            if (!init2) {
                                init2 = true;
                                wylaczTrybSerwisowy(); // odczekanie jeszcze zwłoki czasowej i ponowne sprawdzenie
                            }
                        } else {
                            init2 = false;
                        }
                        break;

                    case 'idWOWPozSmaru': // WOW
                        odswiezajObiekt.typBit(daneDoOdswiezania[i], 'darkred', 'green');
                        break;

                    case 'idNotAus': // WOW
                        odswiezajTylkoNaTab1(daneDoOdswiezania[i], 'darkred', 'green');
                        //odswiezajObiekt.typBit(daneDoOdswiezania[i], 'darkred', 'green');

                        notAusWOW = odswiezajObiekt.typBitStan(daneDoOdswiezania[i]); // 1-OK ; 0-Zadziałany
                        if (!notAusWOW) {
                            if (!init3) {
                                init3 = true;
                                infoAktywnyNotAusWOW = new AlertKM({
                                    id: 'idNotAusWOW',
                                    position: 'bottom',
                                    type: 'alarm',
                                    padding: '0.5em',
                                    width: '99%',
                                    timeVisible: 5000,
                                    timeCyclic: 5 * 60 * 1000,
                                    texts: ['Wciśnięto Not-Aus!']
                                });
                                infoAktywnyNotAusWOW.render();
                            }
                        } else {
                            if (init3) {
                                init3 = false;
                                console.log('removing notaus popup window');
                                infoAktywnyNotAusWOW.remove();
                                infoAktywnyNotAusWOW = null;
                            }
                        }
                        break;

                    default:
                        odswiezajTylkoNaTab1(daneDoOdswiezania[i], '', 'green');
                        //odswiezajObiekt.typBit(daneDoOdswiezania[i], '', 'green');
                        break;
                    }
                }
            }


        },


        inicjacja = function (paczkaDanych) { // wywołanie z tab1/main.dodajStatusy()
            daneDoOdswiezania = daneDoOdswiezania.concat(paczkaDanych);
            setInterval(function () {
                uaktualnijStatus.zmienStatus_Blokady_OLD(); // do wywalenia jak Robert doda obsługę statusów w status Wordzie !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                odswiezajDane();
            }, varGlobal.czasOdswiezania);
        };

    return {
        inicjacja: inicjacja,
        dodajDaneDoOdswiezania: dodajDaneDoOdswiezania
    };
});