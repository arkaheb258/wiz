/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

// test zabezpieczen na duzym wyswietlaczu 10"
define(['jquery',
        'zmienneGlobalne',
        'obslugaJSON',
        'wspolne/odswiezajObiekt',
        'dodajPojedynczaTabele',
        'testZabezpieczen/testNowy'
       ], function (
    $,
    varGlobal,
    json,
    odswiezajObiekt,
    dodajPojedynczaTabele,
    testNowy
) {
    "use strict";


    var init = false,
        obiektTrwaTestZabezpieczen,
        obiektTrwaTestZabezpieczen2,
        idDialog = "#dialogTestZabezpieczen", // tak będzie nazwane okienko popup
        test1 = {
            daneDoWyswietlenia: [],
            daneStatusowe: [],
            daneWyjatki: []
        },
        test2 = {
            daneDoWyswietlenia: [],
            daneStatusowe: [],
            daneWyjatki: []
        },
        pelneDaneTestuZabezpieczen = [],
        pelneDaneTestuZabezpieczen2 = [],


        otworzDialog = function (_tytul, daneTestu) {
            var div,
                szer;

            if ($(idDialog).length === 0) { // sprawdzenie czy div już nie istnieje
                div = document.createElement("div");
                $(div)
                    .addClass('OknaDialog')
                    .addClass('ui-corner-all')
                    .attr('id', idDialog.replace("#", ""));
                $('body').append(div);

                if (varGlobal.hardware.czyMinimumViz) { // wielkość ramki w zależności od typu wyświetlacza
                    szer = '90%';
                } else {
                    szer = '60%';
                }

                $(idDialog).dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    height: 'auto',
                    minHeight: 20,
                    width: szer,
                    title: _tytul,
                    //                    show: {
                    //                        delay: 200,
                    //                        effect: varGlobal.efektShowHide,
                    //                        duration: 350
                    //                    },
                    //                    hide: {
                    //                        delay: 200,
                    //                        effect: varGlobal.efektShowHide,
                    //                        duration: 350
                    //                    },
                    close: function () {
                        $(this).parent().promise().done(function () {
                            $(idDialog).remove();
                            testNowy.koniec(); // wyczyszczenie pamięci w procedurze testu
                        });
                    }
                });

                dodajPojedynczaTabele.dodaj({
                    objects: daneTestu.daneDoWyswietlenia,
                    id: idDialog,
                    cssDescription: 'tdOpis',
                    cssValue: 'tdWartosc'
                });

                $(idDialog).dialog("open");

                testNowy.inicjacja({
                    statusData: daneTestu.daneStatusowe,
                    displayData: daneTestu.daneDoWyswietlenia,
                    exceptions: daneTestu.daneWyjatki
                });

            }
        },


        inicjacja = function () {
            var czyWyswietlicTest,
                czyWyswietlicTest2,
                zezwolenieTest2 = true;

            //console.log('test zabezpieczen - inicjacja');

            pelneDaneTestuZabezpieczen = json.szukajWartosci('testZabezpieczen', varGlobal.sygnaly); // pobranie pełnej paczki danych potrzebnych do testu
            obiektTrwaTestZabezpieczen = json.szukajWartosci('trwaTestZabezpieczen', pelneDaneTestuZabezpieczen); //zmienna informujaca o rozpoczeciu i zakonczeniu testu 

            pelneDaneTestuZabezpieczen2 = json.szukajWartosci('testZabezpieczen2', varGlobal.sygnaly);
            obiektTrwaTestZabezpieczen2 = json.szukajWartosci('trwaTestZabezpieczen', pelneDaneTestuZabezpieczen2);

            // jeśli nie znaleziono obiektu odpowiadającego za start testu -> wyjście z procedury (np. w WOW nie ma testu w ogóle)
            if (obiektTrwaTestZabezpieczen[0] === undefined) {
                test1 = null;
                return;
            } else {
                test1.daneDoWyswietlenia = json.szukajWartosci('zabezpieczenie', pelneDaneTestuZabezpieczen); // sygnały, które mają być wyświetlane
                test1.daneStatusowe = json.szukajWartosci('zabezpieczenieStatus', pelneDaneTestuZabezpieczen); // sygnały, które mają dane statusowe
                test1.daneWyjatki = json.szukajWartosci('zabezpieczenieWylacz', pelneDaneTestuZabezpieczen); // sygnały, które mają nie być uwzględniane w odświeżaniu
            }

            // KTW ma tylko jeden test, GUL ma osobny test do BZU i osobny do CZU
            if (obiektTrwaTestZabezpieczen2[0] === undefined) {
                zezwolenieTest2 = false;
                test2 = null;
            } else {
                test2.daneDoWyswietlenia = json.szukajWartosci('zabezpieczenie', pelneDaneTestuZabezpieczen2);
                test2.daneStatusowe = json.szukajWartosci('zabezpieczenieStatus', pelneDaneTestuZabezpieczen2);
                test2.daneWyjatki = json.szukajWartosci('zabezpieczenieWylacz', pelneDaneTestuZabezpieczen2);
            }

            // czyszczenie pamięci, te tablice już nie są potrzebne
            pelneDaneTestuZabezpieczen = null;
            pelneDaneTestuZabezpieczen2 = null;

            setInterval(function () {
                czyWyswietlicTest = odswiezajObiekt.typBitStan(obiektTrwaTestZabezpieczen[0]);
                if (zezwolenieTest2) {
                    czyWyswietlicTest2 = odswiezajObiekt.typBitStan(obiektTrwaTestZabezpieczen2[0]);
                }

                if (czyWyswietlicTest) { // rozpoczecie testu zabezpieczen nr 1 (BZU)
                    otworzDialog(obiektTrwaTestZabezpieczen[0].opis_pelny, test1);
                } else if (czyWyswietlicTest2) { // rozpoczecie testu zabezpieczen nr 2 (CZU)
                    otworzDialog(obiektTrwaTestZabezpieczen2[0].opis_pelny, test2);
                } else { // koniec testu zabezpieczen
                    if ($(idDialog).length !== 0) { // przyszło polecenie z PLC zakończenia testu - zamknij okienko
                        $(idDialog).dialog("close");
                    }
                }
            }, varGlobal.czasOdswiezania);

            //            pelneDaneTestuZabezpieczen = json.szukajWartosci('testZabezpieczen', varGlobal.sygnaly); // pobranie pełnej paczki danych potrzebnych do testu
            //            daneDoWyswietlenia = json.szukajWartosci('zabezpieczenie', pelneDaneTestuZabezpieczen); // sygnały, które mają być wyświetlane
            //            daneStatusowe = json.szukajWartosci('zabezpieczenieStatus', pelneDaneTestuZabezpieczen); // sygnały, które mają dane statusowe
            //            daneWyjatki = json.szukajWartosci('zabezpieczenieWylacz', pelneDaneTestuZabezpieczen); // sygnały, które mają nie być uwzględniane w odświeżaniu
            //            obiektTrwaTestZabezpieczen = json.szukajWartosci('trwaTestZabezpieczen', pelneDaneTestuZabezpieczen); //zmienna informujaca o rozpoczeciu i zakonczeniu testu 
            //
            //            // jeśli nie znaleziono obiektu odpowiadającego za start testu -> wyjście z procedury (np. w WOW nie ma testu)
            //            if (obiektTrwaTestZabezpieczen[0] === undefined) {
            //                return;
            //            }
            //
            //            pelneDaneTestuZabezpieczen = [];
            //            setInterval(function () {
            //                czyWyswietlicTest = odswiezajObiekt.typBitStan(obiektTrwaTestZabezpieczen[0]);
            //                if (czyWyswietlicTest) { // rozpoczecie testu zabezpieczen
            //                    otworzDialog(obiektTrwaTestZabezpieczen[0].opis_pelny);
            //                } else { // koniec testu zabezpieczen
            //                    if ($(idDialog).length !== 0) { // przyszło polecenie z PLC zakończenia testu - zamknij okienko
            //                        $(idDialog).dialog("close");
            //                    }
            //                }
            //            }, varGlobal.czasOdswiezania);

        };


    return {
        inicjacja: inicjacja
    };

});