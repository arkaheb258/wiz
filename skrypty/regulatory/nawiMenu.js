/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define */

define(['jquery', 'zmienneGlobalne', 'obslugaJSON'], function ($, varGlobal, json) {
    'use strict';


    var cc,
        intervalID,

        wykonaj = function (kod, selected) {
            var i,
                MENU_GULTRYBSERW,
                rozkaz,
                tablicaVal = [],
                aZmienneWykresy = [],
                aGrupy = [],
                idButtona;

            selected.blur();

            switch (kod) {
            case varGlobal.kodyKlawiszy.gora:
                if (selected.prev().length === 0) {
                    selected.parent().find(".przyciskMenuRegulatory").last().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.prev().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            case varGlobal.kodyKlawiszy.dol:
                if (selected.next().length === 0) {
                    selected.parent().find(".przyciskMenuRegulatory").first().addClass("kopex-selected").addClass(varGlobal.ui_state);
                } else {
                    selected.next().addClass("kopex-selected").addClass(varGlobal.ui_state);
                }
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;
            case varGlobal.kodyKlawiszy.enter:
                idButtona = $(selected).attr("id");


                require(['diagnostykaKolumny/main'], function (main) {
                    selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                    //console.log(idButtona);


                    aGrupy = []; // Wyczyszczenie tablicy z poprzednich wynikow
                    aGrupy = idButtona.split("_"); // stworzenie z nazwy buttona tablicy stringow oddzielonych znakiem "_"
                    //console.log(aGrupy);
                    for (i = 0; i < aGrupy.length; i += 1) {
                        tablicaVal = tablicaVal.concat(json.szukajWartosci(aGrupy[i])); // przeszukanie tablicy pod katem pasujacych id i scalenie wynikow
                    }

                    for (i = 0; i < tablicaVal.length; i += 1) { // znalezienie sygnałów z wykresami
                        if (tablicaVal[i].wykresy === 'reg') {
                            aZmienneWykresy.push(tablicaVal[i]);
                        }
                    }
                    //console.log(tablicaVal);
                    //console.log(aZmienneWykresy);


                    //main.start(idButtona, "NL-TRYB-SRVC_NP-TRYB-SRVC_NR-TRYB-SRVC_M1-TRYB-SRVC_SK-TRYB-SRVC_IPS-TRYB-SRVC");
                    main.start(idButtona, idButtona);

                    require(['regulatory/rysujWykresy'], function (rysujWykresy) {
                        rysujWykresy.inicjacja("#dialogDiagnostykaKolumny", aZmienneWykresy);
                    });

                });

                break;
            case varGlobal.kodyKlawiszy.escape:
                require(['regulatory/main'], function (main) {
                    main.zamknij();
                });
                selected.removeClass("kopex-selected").removeClass(varGlobal.ui_state);
                break;

            default:

            }


        };

    return {
        wykonaj: wykonaj
    };
});