/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt */
/*jslint nomen: true*/
/*global  require, define, Raphael */

define(['jquery', 'd3', 'c3', 'wspolne/odswiezajObiekt'], function ($, d3, c3, odswiezajObiekt) {
    'use strict';

    var init = false,
        idDiv = '#idMainDivWykresy',
        czyBrakSeriiDoRysowania = false,
        czyZezwolenieDoRysowania = false,
        chartReg, // zmienna na której będzie rysowany wykres

        serieDanych = [],
        obiektyDoWykresow = [],
        intervalIdLadujDoBufora,


        getRandomInt = function (_min, _max) {
            return Math.floor(Math.random() * (_max - _min)) + _min;
        },


        getAnalogVal = function (_idZmiennej) {
            var i,
                length,
                wartoscAnalogu;

            length = obiektyDoWykresow.length;
            for (i = 0; i < length; i += 1) {
                if (obiektyDoWykresow[i].id === _idZmiennej) {
                    wartoscAnalogu = odswiezajObiekt.typAnalog(obiektyDoWykresow[i]);
                }
            }
            if (wartoscAnalogu === undefined) {  // nie znaleziono podanego id w strukturze obiektów do rysowania na wykresie
                wartoscAnalogu = 0;
            }
            return wartoscAnalogu;
        },


        ladujDaneDoBufora = function () {
            var i;

            intervalIdLadujDoBufora = setInterval(function () {
                if (czyBrakSeriiDoRysowania) {
                    return;
                }
                if (!czyZezwolenieDoRysowania) {
                    return;
                }

                for (i = 0; i < serieDanych.length; i += 1) {
                    if (i === 0) {
                        serieDanych[i].push(new Date().getTime());
                    } else {
                        //serieDanych[i].push(getRandomInt(20, 30));
                        serieDanych[i].push(getAnalogVal(serieDanych[i][0]));
                    }
                }

                if (serieDanych[0].length === 8) { // rysowanie wykresu po uzyskaniu odpowiedniej liczby próbek
                    chartReg.flow({
                        columns: serieDanych,
                        length: 0,
                        duration: 0,
                        to: new Date().setSeconds(new Date().getSeconds() - 60), // wielkość bufora w sekundach
                        done: function () {}
                    });
                    for (i = 0; i < serieDanych.length; i += 1) {
                        serieDanych[i].length = 1; // zerowanie bufora
                    }
                }
            }, 200);
        },


        zamknij = function () {
            czyZezwolenieDoRysowania = false;
            clearInterval(intervalIdLadujDoBufora);
            init = false;
            if (chartReg !== undefined && chartReg !== null) {
                chartReg = chartReg.destroy(); //chart.destroy();
            }
            serieDanych = [];
            obiektyDoWykresow = [];
        },


        inicjacja = function (_idDialog, _valNoweObiektySerii) {
            var div,
                nazwy = {},
                i;

            zamknij();
            obiektyDoWykresow = _valNoweObiektySerii;
            serieDanych.push(['x1', new Date().getTime()]);
            for (i = 0; i < _valNoweObiektySerii.length; i += 1) {
                serieDanych.push([_valNoweObiektySerii[i].id, 0]);
                nazwy[_valNoweObiektySerii[i].id] = _valNoweObiektySerii[i].opis_pelny; // zmiana opisów nazw serii na legendzie wykresu
            }
            
            if ($(idDiv).length > 0) {
                $(idDiv).remove();
            }
            div = document.createElement("div");
            $(div)
                .attr('id', idDiv.replace("#", ""))
                .css({
                    'border': '0.1em solid',
                    'border-color': 'grey',
                    'border-radius': '0.5em',
                    'padding': '1',
                    'width': '99%',
                    'height': '90%',
                    'fill': 'grey',
                    'margin': '2px 2px 2px 2px'
                });
            $(_idDialog).append(div);

            chartReg = c3.generate({ // pierwsze wyrysowanie wykresu
                bindto: idDiv,
                data: {
                    interaction: {
                        enabled: false
                    },
                    x: 'x1',
                    empty: {
                        label: {
                            text: "No Data"
                        }
                    },
                    columns: serieDanych,
                    names: nazwy
                },
                legend: {
                    show: true
                },
                onrendered: function () {
                    if (!init) {
                        //console.log('on render');
                        init = true;
                        czyZezwolenieDoRysowania = true;
                        ladujDaneDoBufora();
                    }
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            fit: false,
                            count: 20,
                            format: '%H:%M:%S'
                        }
                    }
                },
                point: {
                    show: false
                }
            });


            //$("#DialogRegulatoryMenu").one("dialogclose", function (event, ui) { // oczekiwanie na zdarzenie zamknięcia okienka
            $(_idDialog).one("dialogclose", function (event, ui) { // oczekiwanie na zdarzenie zamknięcia okienka
                zamknij();
            });

            $(_idDialog).dialog("option", "position", {
                my: "center",
                at: "center",
                of: window
            });
        };


    return {
        inicjacja: inicjacja,
        zamknij: zamknij
    };
});