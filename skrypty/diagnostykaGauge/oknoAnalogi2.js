/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global JustGage, getRandomInt, gaugeKMSA */
/*jslint nomen: true*/
/*global  define, require*/

define(['jquery', 'zmienneGlobalne', 'diagnostykaGauge/gaugeKMv2', 'ustawKolejnosc'], function ($, varGlobal, GaugeKM, ustawKolejnosc) {
    'use strict';

    var init = false,


        inicjacja = function (daneWej) {
            var i,
                intervalId,
                div,
                length,
                plc_id,
                kontrolkiGage = [],
                daneWejKolejne = [];

            daneWejKolejne = ustawKolejnosc.inicjacja({
                inputData: daneWej,
                sortData: true
            });

            //  ____     ___         _____   _____   ____    _____    ___   __        __
            // |  _ \   / _ \       |_   _| | ____| / ___|  |_   _|  / _ \  \ \      / /
            // | | | | | | | |        | |   |  _|   \___ \    | |   | | | |  \ \ /\ / /
            // | |_| | | |_| |        | |   | |___   ___) |   | |   | |_| |   \ V  V /
            // |____/   \___/         |_|   |_____| |____/    |_|    \___/     \_/\_/
            //            require(['diagnostykaGauge/testyGauge'], function (testyGauge) {
            //                testyGauge.inicjacja();
            //            });


            //console.log(daneWejKolejne);
            length = daneWejKolejne.length;
            for (i = 0; i < length; i += 1) {
                daneWejKolejne[i].xHistHiEnabled = false; // !!!!! dodanie pola dla obsługi histerez
                daneWejKolejne[i].xHistLoEnabled = false;
                daneWejKolejne[i].ana_min = parseFloat(daneWejKolejne[i].ana_min);
                daneWejKolejne[i].ana_max = parseFloat(daneWejKolejne[i].ana_max);
                daneWejKolejne[i].ana_alarm_l = parseFloat(daneWejKolejne[i].ana_alarm_l);
                daneWejKolejne[i].ana_warn_l = parseFloat(daneWejKolejne[i].ana_warn_l);
                daneWejKolejne[i].ana_warn_h = parseFloat(daneWejKolejne[i].ana_warn_h);
                daneWejKolejne[i].ana_alarm_h = parseFloat(daneWejKolejne[i].ana_alarm_h);
                daneWejKolejne[i].hist_LoALarm = parseFloat(daneWejKolejne[i].hist_LoALarm);
                daneWejKolejne[i].hist_HiALarm = parseFloat(daneWejKolejne[i].hist_HiALarm);

                div = document.createElement("div"); // Kontrolki gauge musza byc umieszczone w elementach div
                $(div)
                    .addClass('Gauge')
                    .css({
                        'padding-top': '0.5em'
                    })
                    .attr('id', daneWejKolejne[i].id);
                $('.kontenerGauge').append(div);
            }

            $("#dialogDiagnostykaGauge").dialog("open"); // Otwarcie dialogu
            $("#dialogDiagnostykaGauge").one("dialogclose", function (event, ui) { // po zamknieciu okenka zakonczenie odswieznia danych
                clearInterval(intervalId);
                for (i = 0; i < length; i += 1) {
                    kontrolkiGage[i].destroy();
                    kontrolkiGage[i] = null;
                }
                kontrolkiGage = null;
                daneWejKolejne = null;
            });

            for (i = 0; i < length; i += 1) {
                plc_id = '';
                if (daneWejKolejne[i].plc_id !== undefined) {
                    plc_id = daneWejKolejne[i].plc_id + ' - ';
                }

                kontrolkiGage[i] = new GaugeKM({
                    parent: document.getElementById(daneWejKolejne[i].id),
                    label: plc_id + daneWejKolejne[i].opis_pelny,
                    units: daneWejKolejne[i].jednostka,
                    minValue: daneWejKolejne[i].ana_min,
                    maxValue: daneWejKolejne[i].ana_max,
                    decimals: daneWejKolejne[i].mnoznik,
                    //autoControlHysteresis: true,
                    //ignoreHysteresis: true,
                    hysteresis: [
                        { // index: 0
                            type: 'High',
                            enabled: false,
                            val: -daneWejKolejne[i].hist_HiALarm, // górny zakres jest odejmowany!
                            sectorIndex: 4
                        },
                        { // index: 1
                            type: 'Low',
                            enabled: false,
                            val: daneWejKolejne[i].hist_LoALarm,
                            sectorIndex: 0
                        }
                    ],
                    sectors: ([
                        { // index: 0
                            color: "red",
                            lo: daneWejKolejne[i].ana_min,
                            hi: daneWejKolejne[i].ana_alarm_l
                        },
                        { // index: 1
                            color: "yellow",
                            lo: daneWejKolejne[i].ana_alarm_l,
                            hi: daneWejKolejne[i].ana_warn_l
                        },
                        { // index: 2
                            color: "green",
                            lo: daneWejKolejne[i].ana_warn_l,
                            hi: daneWejKolejne[i].ana_warn_h
                        },
                        { // index: 3
                            color: "yellow",
                            lo: daneWejKolejne[i].ana_warn_h,
                            hi: daneWejKolejne[i].ana_alarm_h
                        },
                        { // index: 4
                            color: "red",
                            lo: daneWejKolejne[i].ana_alarm_h,
                            hi: daneWejKolejne[i].ana_max
                        }
                    ])
                });
                kontrolkiGage[i].render();
                //console.log(kontrolkiGage);
            }


            intervalId = setInterval(function () { //przechwycenie Id funkcji setInterval, po zamknieciu okna bedzie mozliwe zakonczenie odswiezania
                var actVal;

                length = daneWejKolejne.length;
                for (i = 0; i < length; i += 1) {
                    if (varGlobal.daneTCP.Analog[daneWejKolejne[i].poz_ramka] !== undefined) {
                        actVal = varGlobal.daneTCP.Analog[daneWejKolejne[i].poz_ramka] / daneWejKolejne[i].mnoznik;
                        kontrolkiGage[i].refresh(actVal);

                        //if (daneWejKolejne[i].id !== 'wGU_PrzeklLewaCisnOleju') {
                            //return;
                        //}

                        // Każda histereza jest traktowana osobno
                        if (daneWejKolejne[i].xHistLoEnabled) { // ON -> OFF
                            if (actVal > daneWejKolejne[i].ana_alarm_l + daneWejKolejne[i].hist_LoALarm) {
                                //console.log('HL off');
                                daneWejKolejne[i].xHistLoEnabled = false;
                                kontrolkiGage[i].disableHysteresis(1);
                            }
                        } else { // OFF -> ON
                            if (actVal < daneWejKolejne[i].ana_alarm_l) {
                                //console.log('HL on');
                                daneWejKolejne[i].xHistLoEnabled = true;
                                kontrolkiGage[i].enableHysteresis(1);
                            }
                        }
   
                        if (daneWejKolejne[i].xHistHiEnabled) { // ON -> OFF
                            if (actVal < (daneWejKolejne[i].ana_alarm_h - daneWejKolejne[i].hist_HiALarm)) {
                                //console.log('HH off');
                                daneWejKolejne[i].xHistHiEnabled = false;
                                kontrolkiGage[i].disableHysteresis(0);
                            }
                        } else { // OFF -> ON
                            if (actVal > daneWejKolejne[i].ana_alarm_h) {
                                //console.log('HH on');
                                daneWejKolejne[i].xHistHiEnabled = true;
                                kontrolkiGage[i].enableHysteresis(0);
                            }
                        }



                    }
                }
            }, 500); // varGlobal.czasOdswiezania
        };


    return {
        inicjacja: inicjacja
    };

});




//                        if (daneWejKolejne[i].hist_HiALarm > 0 || daneWejKolejne[i].hist_LoALarm > 0) { // czy jest w ogóle zdefiniowana histereza dla sygnału
//                            if (daneWejKolejne[i].xHistLoEnabled && actVal > daneWejKolejne[i].ana_alarm_l + daneWejKolejne[i].hist_LoALarm) {
//                                //console.log('HL off');
//                                daneWejKolejne[i].xHistLoEnabled = false;
//                                kontrolkiGage[i].disableHysteresis(1);
//                            } else if (daneWejKolejne[i].xHistHiEnabled && actVal < (daneWejKolejne[i].ana_alarm_h - daneWejKolejne[i].hist_HiALarm)) {
//                                //console.log('HH off'); 
//                                daneWejKolejne[i].xHistHiEnabled = false;
//                                kontrolkiGage[i].disableHysteresis(0);
//                            } else if (!daneWejKolejne[i].xHistLoEnabled && actVal < daneWejKolejne[i].ana_alarm_l) {
//                                //console.log('HL on');
//                                daneWejKolejne[i].xHistLoEnabled = true;
//                                kontrolkiGage[i].enableHysteresis(1);
//                            } else if (!daneWejKolejne[i].xHistHiEnabled && actVal > daneWejKolejne[i].ana_alarm_h) {
//                                //console.log('HH on'); 
//                                daneWejKolejne[i].xHistHiEnabled = true;
//                                kontrolkiGage[i].enableHysteresis(0);
//                            }
//                        }