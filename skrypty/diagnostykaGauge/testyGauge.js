/*jslint browser: true*/
/*jslint bitwise: true */
/*global $, jQuery*/
/*jslint devel: true */
/*global document: false */
/*global idZegartestowy, getRandomInt */
/*jslint nomen: true*/
/*global  define, require*/


define(['jquery'], function ($) {
    'use strict';

    var ccc,
        k1,
        nowyZegarGauge = {
            id: 'idZegartestowy',
            opis_pelny: 'Zegar testowy',
            jednostka: 'A',
            ana_min: 0,
            ana_alarm_l: 10,
            ana_warn_l: 20,
            ana_warn_h: 80,
            ana_alarm_h: 90,
            ana_max: 100,
            hist_LoALarm: 5,
            hist_HiALarm: 5
        },


        odswiez2 = function (actVal) {

            k1.refresh(actVal);

            if (parseFloat(nowyZegarGauge.hist_HiALarm)) { // czy jest w ogóle zdefiniowana histereza dla sygnału
                console.log(actVal + ' / ' + (nowyZegarGauge.ana_alarm_h - nowyZegarGauge.hist_HiALarm));


                if (!nowyZegarGauge.xHistEnable && actVal > parseFloat(nowyZegarGauge.ana_alarm_h)) {
                    // przejście progu alarmowego - zmiana zakresów z uwzględnieniem histerezy
                    console.log('alarm - dodanie histerezy');
                    nowyZegarGauge.xHistEnable = true;
                    k1.updateSectors([
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l + nowyZegarGauge.hist_LoALarm
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l + nowyZegarGauge.hist_LoALarm,
                            hi: nowyZegarGauge.ana_warn_l
                        },
                        {
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l,
                            hi: nowyZegarGauge.ana_warn_h
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_warn_h,
                            hi: nowyZegarGauge.ana_alarm_h - nowyZegarGauge.hist_HiALarm
                        },
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_alarm_h - nowyZegarGauge.hist_HiALarm,
                            hi: nowyZegarGauge.ana_max
                        }
                    ]);
                } else if (nowyZegarGauge.xHistEnable && actVal > parseFloat(nowyZegarGauge.ana_alarm_h - nowyZegarGauge.hist_HiALarm)) {
                    // histereza aktywna - sygnał powyżej progu alarmowego
                    console.log('alarm  ' + actVal);
                } else if (nowyZegarGauge.xHistEnable && actVal < parseFloat(nowyZegarGauge.ana_alarm_h - nowyZegarGauge.hist_HiALarm)) {
                    // histereza aktywna - sygnał poniżej progu alarmowego
                    console.log('ok - usunięcie histerezy ' + actVal);
                    nowyZegarGauge.xHistEnable = false;
                    k1.updateSectors([
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l,
                            hi: nowyZegarGauge.ana_warn_l
                        },
                        {
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l,
                            hi: nowyZegarGauge.ana_warn_h
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_warn_h,
                            hi: nowyZegarGauge.ana_alarm_h
                        },
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_alarm_h,
                            hi: nowyZegarGauge.ana_max
                        }
                    ]);
                }
            }

        },


        odswiezSeria = function (instancjaKontrolki) {
            setTimeout(function () {
                instancjaKontrolki.refresh(5);
            }, 2000);
            setTimeout(function () {
                instancjaKontrolki.refresh(12);
            }, 3000);
            setTimeout(function () {
                instancjaKontrolki.refresh(18);
            }, 4000);
            setTimeout(function () {
                //id.value(40).render();
                instancjaKontrolki.refresh(40);
            }, 5000);
            setTimeout(function () {
                //id.value(83).render();
                instancjaKontrolki.refresh(83);
            }, 6000);
            setTimeout(function () {
                //id.value(87).render();
                instancjaKontrolki.refresh(87);
            }, 7000);
            setTimeout(function () {
                instancjaKontrolki.refresh(98);
            }, 8000);
            setTimeout(function () {
                instancjaKontrolki.refresh(133);
            }, 9000);
            setTimeout(function () {
                instancjaKontrolki.refresh(-25);
            }, 10000);
            setTimeout(function () {
                instancjaKontrolki.refresh(0);
            }, 11000);
            setTimeout(function () {
                instancjaKontrolki.refresh(100);
            }, 12000);
            setTimeout(function () {
                instancjaKontrolki.refresh(66.6);
            }, 13000);
        },


        zamknij = function () {
            console.log(k1);
            k1.destroy();
            k1 = null;
            console.log(k1);
        },


        inicjacja = function () {
            var div;

            div = document.createElement("div"); // Kontrolki gauge musza byc umieszczone w elementach div
            $(div)
                .addClass('Gauge')
                .css({
                    'padding-top': '0.5em',
                    'border': '0.1em solid',
                    'border-color': 'grey'
                })
                .attr('id', 'idTestGaugeKMv2');
            $('.kontenerGauge').append(div);

            require(['diagnostykaGauge/gaugeKMv2'], function (GaugeKM) {
                k1 = new GaugeKM({
                    parent: document.getElementById('idTestGaugeKMv2'),
                    minValue: 0,
                    label: 'TEST testy () () yyy jjj qwerty QWERTY TEST testy () () yyy jjj qwerty',
                    units: 'A',
                    decimals: 1,
                    maxValue: 100,
                    hysteresis: [
                        { // index: 0
                            val: -nowyZegarGauge.hist_HiALarm, // górny zakres jest odejmowany!
                            sectorIndex: 4 - 1
                        },
                        { // index: 1
                            val: nowyZegarGauge.hist_LoALarm,
                            sectorIndex: 0
                        }
                    ],
                    sectors: ([
                        { // index: 0
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l
                        },
                        { // index: 1
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l,
                            hi: nowyZegarGauge.ana_warn_l
                        },
                        { // index: 2
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l,
                            hi: nowyZegarGauge.ana_warn_h
                        },
                        { // index: 3
                            color: "yellow",
                            lo: nowyZegarGauge.ana_warn_h,
                            hi: nowyZegarGauge.ana_alarm_h
                        },
                        { // index: 4
                            color: "red",
                            lo: nowyZegarGauge.ana_alarm_h,
                            hi: nowyZegarGauge.ana_max
                        }
                    ])
                });

                k1.render();
                k1.refresh(33.3);

                //                nowyZegarGauge.xHistEnable = false;
                setTimeout(function () {
                    k1.enableHysteresis(0);
                }, 3000);
                setTimeout(function () {
                    k1.disableHysteresis(0);
                }, 4000);
                setTimeout(function () {
                    k1.enableHysteresis(1);
                }, 5000);
                setTimeout(function () {
                    k1.disableHysteresis(1);
                }, 6000);



                setTimeout(function () {
                    k1.updateSectors([
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l + 5
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l + 5,
                            hi: nowyZegarGauge.ana_warn_l + 30
                        },
                        {
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l + 30,
                            hi: nowyZegarGauge.ana_warn_h
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_warn_h,
                            hi: nowyZegarGauge.ana_alarm_h - 5
                        },
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_alarm_h - 5,
                            hi: nowyZegarGauge.ana_max
                        }
                    ]);

                    k1.refresh(33.3); // !!!! Trzeba odświżyć łuk główny żeby został dobrze narysowany kolor!!!!
                }, 1000);

                setTimeout(function () {
                    k1.updateSectors([
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l,
                            hi: nowyZegarGauge.ana_warn_l
                        },
                        {
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l,
                            hi: nowyZegarGauge.ana_warn_h
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_warn_h,
                            hi: nowyZegarGauge.ana_alarm_h
                        },
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_alarm_h,
                            hi: nowyZegarGauge.ana_max
                        }
                    ]);

                    k1.refresh(33.3); // !!!! Trzeba odświżyć łuk główny żeby został dobrze narysowany kolor!!!!
                }, 2000);


                //odswiezSeria(k1);
                //k1.destroy();



            });
        };


    return {
        inicjacja: inicjacja,
        zamknij: zamknij
    };


});