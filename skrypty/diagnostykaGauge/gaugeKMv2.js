/*jslint devel: true */
/*jslint nomen: true*/
/*global define */

/**
 * -----------------------------
 * 2015-10-01
 * -----------------------------
 * Stworzenie dokumentu (induGage)
 *
 *
 * -----------------------------
 * 2015-11-17
 * -----------------------------
 * Poprawki w działaniu histerezy. Wyłączenie jej gdy sektor alarmowy po jej zadziałaniu jest równy lub większy od sektora sąsiadującego (ostrzeżenia)
 */


/**
 * Moduł tworzący nową instancję kontrolki gauge do wizualizowania wartości analogowych.
 * @module gaugeKMv2
 */
define(['d3'], function (d3) {
    'use strict';

    /**
     * @description Stworzenie nowej instancji kontrolki.
     * @constructor
     * @alias module:gaugeKMv2
     * @version 1.0.0
     * @author Paweł Karp
     * @copyright Kopex Machinery S.A 2015
     * @requires module:d3
     * @param {Object} config Obiekt przekazywany do konstruktora z parametrami użytkownika
     * @param {string} config.parent ID kontenera (div), do którego ma być załadowana kontrolka. Id podawać bez '#'
     * @param {number} config.minValue=0 Wartość minimum
     * @param {number} config.maxValue=100 Wartość maksimum
     * @param {string} config.minMaxFontSize='0.9em' Wielkość czcionki dla labelek min i max
     * @param {Array.<Object>} config.sectors Zakresy alarmów i ostrzeżeń (poprzednio _dane)
     * @param {string} config.sectors[].color - Kolor jaki ma mieć dany zakres.
     * @param {number} config.sectors[].lo - Wartość początkowa.
     * @param {number} config.sectors[].hi - Wartość końcowa.
     * @param {number} [config.duration=10] Czas (ms) trwania animacji (patrz d3.js -> transition duration)
     * @param {number} [config.arcThickness=0.7] Szerokość łuku (przyjmuje wartość od 0 do 1)
     * @param {string} [config.fill='#edebeb'] Kolor wypełnienienia pustej części łuku
     * @param {string} [config.label=" "] Opis kontrolki
     * @param {string} [config.labelFontSize="1.0em"] Wielkość czcionki dla labelki z opisem zmiennej
     * @param {string} [config.labelFontColor="#b3b3b3"] Kolor napisu z opisem
     * @param {string} [config.units=" "] Jednostka wartości wyświetlanej np. %
     * @param {string} [config.unitsFontSize="1.0em"] Wielkość czcionki dla labelki z jednostką zmiennej
     * @param {string} [config.valueFontSize="1.9em"] Wielkość czcionki dla labelki z wartością zmiennej
     * @param {number} [config.decimals=1] Precyzja - ilość pokazywanych cyfr po przecinku, podajemy w postaci mnożnika (1, 10, 100...)
     * @param {number} [config.sectorsThicknes=0.15] Szrokość łuków pokazujących zakresy
     * @param {boolean} [config.showSectors=true] Czy pokazać łuki z zakresami
     * @param {boolean} [config.showSectorsValues=true] Czy pokazać teksty z wartościami granicznymi zakresów
     * @param {string} [config.sectorsValuesFontSize='0.85em'] Wielkość czcionki dla tekstu z wartością zakresu
     * @param {boolean} [config.animateMainArc=false] Czy animować wskazania głównego łuku
     * @param {number} [config.sectorsUpdateDuration=500] Czas animacji podczas zmiany wartości sektorów alarmów i ostrzeżeń
     * @example
     * // przykładowe zainicjowanie kontrolki
            require(['diagnostykaGauge/gaugeKMv2'], function (GaugeKM) {
                k1 = new GaugeKM({
                    parent: document.getElementById('idTestGaugeKMv2'),
                    minValue: 0,
                    label: 'Prąd silnika organu',
                    units: 'A',
                    decimals: 1,
                    maxValue: 100,
                    sectors: ([
                        {
                            color: "red",
                            lo: nowyZegarGauge.ana_min,
                            hi: nowyZegarGauge.ana_alarm_l
                        },
                        {
                            color: "yellow",
                            lo: nowyZegarGauge.ana_alarm_l,
                            hi: nowyZegarGauge.ana_warn_l
                        },
                        {
                            color: "green",
                            lo: nowyZegarGauge.ana_warn_l,
                            hi: nowyZegarGauge.ana_warn_h
                        }
                    ])
                });
                k1.render();
                k1.refresh(33.3);
     * @todo Parametr i funkcja czy klasa ma sama kontrolować przejścia stanów histerezy
     * @todo Pokazanie wszystkich sektorów osobno (łącznie z histerezami) - w celach diagnostycznych
     * @todo Wprowadzić czułość - przerysowanie kontrolki tylko po przekroczeniu progu o np. 0.5A
     * @todo Przesuwanie opisów sektrorów po aktywowaniu histerezy
     * @return {GaugeKM} Nowy obiekt kontrolki.
     */
    function GaugeKM(config) { // To jest konstruktor klasy
        if (!(this instanceof GaugeKM)) {
            throw new TypeError("*** GaugeKM - brak wywołania instancji ze słowem 'new'");
        }
        if (config === null || config === undefined) {
            console.log('*** gaugeKM: Nie przekazano parametrów do konstruktora!');
            return false;
        }
        if (config.parent === null || config.parent === undefined) {
            console.log('*** gaugeKM: Nie znaleziono id kontenera html do którego ma być załadowana kontrolka!');
            return false;
        }

        // Class public properties
        /** @public */
        this.parent = config.parent;
        this.minValue = config.minValue || 0;
        this.maxValue = config.maxValue || 100;
        this.minMaxFontSize = config.minMaxFontSize || '0.9em';
        this.sectors = config.sectors;
        this.duration = config.duration || 500;
        this.arcThickness = config.arcThickness || 0.7;
        this.fill = config.fill || '#edebeb';
        this.label = config.label || '';
        this.labelFontSize = config.labelFontSize || '1.0em';
        this.labelFontColor = config.labelFontColor || "#b3b3b3";
        this.units = config.units || '';
        this.unitsFontSize = config.unitsFontSize || '1.0em';
        this.decimals = config.decimals || 1;
        this.sectorsThicknes = config.sectorsThicknes || 0.3;
        this.valueFontSize = config.valueFontSize || '1.9em';
        this.showSectors = config.showSectors || true;
        this.showSectorsValues = config.showSectorsValues || true;
        this.sectorsValuesFontSize = config.sectorsValuesFontSize || '0.85em';
        this.animateMainArc = config.showSectors || false;
        this.sectorsUpdateDuration = config.sectorsUpdateDuration || 500;
        this.hysteresis = config.hysteresis || null; // dodać do jsdoc
        this.ignoreHysteresis = config.ignoreHysteresis || false;
        this.autoControlHysteresis = config.autoControlHysteresis || false;
        this.autoControlHysteresis = config.autoControlHysteresis || false;
        this.sensitivity = config.sensitivity || 1;

        // Class private properties
        /** @private */
        this._currentArcStartAngle = (-90 + 0.1) * (Math.PI / 180);
        this._sectorsColors = [];
        this._sectorsRanges = []; // lo:30, hi:50 -> value:20
        this._sectorsTreshold = []; // wartości graniczne pomiędzy sektorami
        this._mainArc = undefined; // d3.js object
        this._valueLabel = undefined; // d3.js object
        this._foregroundPath = undefined; // d3.js object
        this._sectorsArc = undefined; // d3.js object
        this._sectorsPie = undefined; // d3.js object
        this._sectorsGroup = undefined; // d3.js object
        this._sectorsPath = undefined; // d3.js object
        this._sectorsTexts = undefined; // d3.js object
        this._sectorsDefaults = null;
        this._initHyst = false;
        this._precision = false;
        this._timeoutId = 0;
        this._oldVal = 0;

        this._setSectorsRange = function (data) {
            var i;
            this._sectorsRanges = [];
            this._sectorsTreshold = [];
            for (i = 0; i < data.length; i += 1) { // stwotrzenie zakresów bez uwzględnienia histerez
                if (data[i].hi !== 0) {
                    if (data[i].hi - data[i].lo > 0) { // na zakresach wysokich gdy np. chcemy pozbyć się ostrzeżenia mogą powstać zera - powodują kłopoty z ustawianiem kolorów na wysokie zakresy
                        this._sectorsColors.push(data[i].color);
                        this._sectorsRanges.push(data[i].hi - data[i].lo);
                        this._sectorsTreshold.push(data[i].hi); // zapamiętanie wartość skrajnych (alarmów i ostrzeżeń) - do wyświetlenia przy sektorach
                    }
                }
            }
            this._sectorsTreshold[this._sectorsTreshold.length - 1] = ''; // ostatnia pozycja jest z wartością maksymalna, a jej nie chcemy wyświetlić
        };

        this._setNewSectorsArcs = function () {
            // Store the displayed angles in _current. Then, interpolate from _current to the new angles. During the transition, _current is updated in-place by d3.interpolate.
            var mainSectorsArc = this._sectorsArc,
                n = this._sectorsTreshold.length, // licznik transformacji (wszystkich przerysowywanych sektorów)
                endAllCallback = function (_this) {
                    //console.log('koniec transformacji - ODAĆ FLAGĘ NA ZEZWOLENIE PONOWNEGO PRZERYSOWANIA SEKTORÓW');
                },
                arcTween = function (a) {
                    var i = d3.interpolate(this._current, a);
                    this._current = i(0);
                    return function (t) {
                        return mainSectorsArc(i(t));
                    };
                };

            this._sectorsPath // Przerysowanie łuków
                .data(this._sectorsPie(this._sectorsRanges));
            this._sectorsPath
                .transition()
                .duration(this.sectorsUpdateDuration)
                .attrTween("d", arcTween)
                .each("end", function () { // licznik końca każdej transformacji
                    n -= 1;
                    if (n === 0) { // wszystkie transformacje zostały zakończone
                        endAllCallback(this);
                    }
                });
        };

        this._setNewSectorsTexts = function () {
            var sectorsTreshold,
                mainSectorsArc = this._sectorsArc;

            sectorsTreshold = this._sectorsTreshold;
            this._sectorsTexts = this._sectorsGroup.append("text")
                .attr("text-anchor", "middle")
                .attr('font-family', 'Arial')
                .attr("fill", this.labelFontColor) //.attr("dy", "1.35em") //return "translate(" + arc[i].centroid() + ")";
                .attr("fill-opacity", 0.8)
                .attr('font-size', this.sectorsValuesFontSize)
                .attr("transform", function (d) {
                    var tempData = Object.create(d),
                        c;
                    tempData.startAngle = tempData.endAngle;
                    c = mainSectorsArc.centroid(tempData);
                    return "translate(" + c[0] * 0.75 + "," + c[1] * 0.75 + ")"; // przesunięcie
                })
                .text(function (d, i) {
                    return sectorsTreshold[i];
                });
        };


        this._updateSectors = function (newSec) {
            this._sectorsTexts.remove();
            this._setSectorsRange(newSec);
            this._setNewSectorsArcs();
            this._setNewSectorsTexts();
        };


        this._enableHysteresis = function (histInx) {
            if (!this.hysteresis[histInx].enabled) {
                this.hysteresis[histInx].enabled = true;
            } else {
                return;
            }

            var sectIndex = this.hysteresis[histInx].sectorIndex, // odczytanie na który sektor ma działać histereza
                newSectorHi = this.sectors[sectIndex].hi + this.hysteresis[histInx].val,
                newSectorLo = this.sectors[sectIndex].lo + this.hysteresis[histInx].val;

            if (this.hysteresis[histInx].type === 'Low') {
                if (newSectorHi >= this.sectors[sectIndex + 1].hi) {
                    console.log('*** gaugeKM: ' + this.parent.id + ': Zła wartość histerezy Low!'); // wejście z wartością sektora na sąsiedni sektor
                    this.hysteresis[histInx].enabled = false; // anulacja
                    this.ignoreHysteresis = true; // wyłączenie wyświetlania histerezy dla tej instancji
                    return;
                }
                this.sectors[sectIndex].hi = this.sectors[sectIndex].hi + this.hysteresis[histInx].val;
                this.sectors[sectIndex + 1].lo = this.sectors[sectIndex + 1].lo + this.hysteresis[histInx].val;
            }
            if (this.hysteresis[histInx].type === 'High') {
                if (newSectorLo <= this.sectors[sectIndex - 1].lo) {
                    console.log('*** gaugeKM: ' + this.parent.id + ': Zła wartość histerezy High!');
                    this.hysteresis[histInx].enabled = false;
                    this.ignoreHysteresis = true;
                    return;
                }
                this.sectors[sectIndex - 1].hi = this.sectors[sectIndex - 1].hi + this.hysteresis[histInx].val;
                this.sectors[sectIndex].lo = this.sectors[sectIndex].lo + this.hysteresis[histInx].val;
            }
            this._updateSectors(this.sectors);
        };


        this._disableHysteresis = function (histInx) {
            if (this.hysteresis[histInx].enabled) {
                this.hysteresis[histInx].enabled = false;
            } else {
                return;
            }
            var sectIndex = this.hysteresis[histInx].sectorIndex;
            if (this.hysteresis[histInx].type === 'Low') {
                this.sectors[sectIndex].hi = this.sectors[sectIndex].hi - this.hysteresis[histInx].val;
                this.sectors[sectIndex + 1].lo = this.sectors[sectIndex + 1].lo - this.hysteresis[histInx].val;
            }
            if (this.hysteresis[histInx].type === 'High') {
                this.sectors[sectIndex - 1].hi = this.sectors[sectIndex - 1].hi - this.hysteresis[histInx].val;
                this.sectors[sectIndex].lo = this.sectors[sectIndex].lo - this.hysteresis[histInx].val;
            }
            this._updateSectors(this.sectors);
        };


        this._checkHysteresis = function (histInx) {

        };


        this._setPrecision = function () {
            var multiplication;
            if ((typeof this.decimals) !== 'number') { // sprawdzenie prawidłowego typu
                multiplication = 1;
                //throw new TypeError(typeof (this.decimals) + " is not a number.");
            } else {
                multiplication = this.decimals;
            }
            multiplication = multiplication.toString(); // konwersja mnożnika na string a potem policzenie ilości zer
            if (multiplication.match(/0/g) !== null) {
                this._precision = multiplication.match(/0/g).length;
            }
        };




    }


    // Dodanie parametrów statycznych
    GaugeKM.CONST_1 = 11;
    GaugeKM.START_VALUE = (-90 + (0.01 / 100) * 180) * (Math.PI / 180); // wartość poczatkowa dla narysowanej kontrolki (0.01)

    // Dodanie funkcji statycznych
    //GaugeKM._setNewSectorsArcs = function (data) {
    //    function _setNewSectorsArcs(obj) {
    //        console.log('rysuję nowe łuki sektorów');
    //        console.log(obj);
    //
    //    }

    //var event = new CustomEvent('build', { 'detail': elem.dataset.time });

    GaugeKM.prototype = {


        constructor: GaugeKM,


        render: function () {
            var canvas,
                i,
                //mnoznik,
                //precyzja,
                sectorsColors = this._sectorsColors,
                backgroundPath,
                width = this.parent.offsetWidth / 2 * 0.85, // *0.75,
                height = width / 2;

            this._setPrecision();

            // Create the SVG container, and apply a transform such that the origin is the center of the canvas. This way, we don't need to position arcs individually.
            canvas = d3.select(this.parent).append("svg").attr("class", "gauge-svg")
                .attr("width", '100%')
                .attr("height", '100%')
                .append("g")
                .attr("transform", "translate(" + (width + ((this.parent.offsetWidth - width * 2) / 2)) + "," + width + ")"); // "translate(" + x + "," + y + ")";

            // Tutaj podajemy tylko kąt początkowy, końcowy zostanie wypracowany w ścieżkach
            this._mainArc = d3.svg.arc()
                .innerRadius(width * this.arcThickness)
                .outerRadius(width)
                .startAngle(-90 * (Math.PI / 180));

            // Add the background arc
            backgroundPath = canvas.append("path")
                .datum({
                    endAngle: 90 * (Math.PI / 180)
                })
                .style("fill", "#ddd")
                .attr("class", "classPathBackground")
                .attr("d", this._mainArc);

            // Add the foreground arc
            this._foregroundPath = canvas.append("path")
                .datum({
                    endAngle: GaugeKM.START_VALUE
                })
                .style("fill", "silver")
                .attr("class", "classPathValue")
                .attr("d", this._mainArc);

            // ustalenie wartości zakresów
            this._setSectorsRange(this.sectors);

            // Narysowanie łuków z zakresami
            if (this.showSectors) {
                this._sectorsArc = d3.svg.arc()
                    .outerRadius(width * this.arcThickness - 2)
                    .innerRadius(width * this.arcThickness - (width / 4 * this.sectorsThicknes));
                this._sectorsPie = d3.layout.pie()
                    .value(function (d) {
                        return d;
                    })
                    .sort(null)
                    .startAngle(-90 * (Math.PI / 180))
                    .endAngle(90 * (Math.PI / 180));

                // stworzenie glownej grupy na luki sektorow i ich opiski
                this._sectorsGroup = canvas.append("g")
                    .selectAll(".arc")
                    .data(this._sectorsPie(this._sectorsRanges))
                    .enter()
                    .append("g"); // w tej podgrupie bedzie znajdowal sie pojedynczy sektor z opisem

                this._sectorsPath = this._sectorsGroup.append("path") //.transition().duration(500)
                    .attr("d", this._sectorsArc)
                    .style("fill", function (d, i) {
                        return sectorsColors[i];
                    })
                    .each(function (d) { // potrzebne do zmiany wartości sektorów
                        this._current = d; // store the initial angles
                    });

                // dodanie tekstów z wartościami granicznymi zakresów
                if (this.showSectorsValues) {
                    this._setNewSectorsTexts();
                }

            }

            this._valueLabel = canvas.append("text") // dodanie labelki z wartością zmiennej analogowej
                .attr('font-weight', 'bold')
                .attr('font-family', 'Arial')
                .attr('fill', this.labelFontColor)
                .attr('font-size', this.valueFontSize)
                .attr("dy", "-0.6em")
                .style("text-anchor", "middle")
                .text('0.0');
            canvas.append("foreignObject") // labelka z opisem zmiennej wyświetlanej na kontrolce, foreinObject pozwala na zawijanie długich tekstów
                .attr("height", 30)
                .attr("width", this.parent.offsetWidth - 15) // zwężenie pola tekstowego aby poprawić czytelnosć. Uwaga: niżej przy opcji "transform" jest przesunięcie o 7.5
                .attr('style', 'word-wrap: break-word; text-align:center;')
                .attr("text-anchor", "middle")
                .attr('font-family', 'Arial')
                .style("color", this.labelFontColor)
                .attr('font-size', this.labelFontSize)
                .attr("transform", "translate(" + ((-(this.parent.offsetWidth / 2)) + 7.5) + ", 0)")
                .text(this.label);
            canvas.append("text") // dodanie labelki z jednostką analogu
                .attr("text-anchor", "middle")
                .attr('font-family', 'Arial')
                .attr("fill-opacity", 0.8)
                .attr("fill", this.labelFontColor)
                .attr('font-size', this.unitsFontSize)
                .attr("dy", "-0.1em")
                .text(this.units);
            canvas.append("text")
                .attr("text-anchor", "middle")
                .attr('font-family', 'Arial')
                .attr("fill-opacity", 0.8)
                .attr("fill", this.labelFontColor)
                .attr('font-size', this.minMaxFontSize)
                .attr("transform", "translate(" + (-width / 1.8) + ",0)")
                .attr("dx", "0.3em")
                .text(this.minValue);
            canvas.append("text")
                .attr("text-anchor", "middle")
                .attr('font-family', 'Arial')
                .attr("fill-opacity", 0.8)
                .attr("fill", this.labelFontColor)
                .attr('font-size', this.minMaxFontSize)
                .attr("transform", "translate(" + (width / 2) + ",0)")
                .attr("dx", "-0.3em")
                .text(this.maxValue);
        },


        /** 
         * @description Odświeżenie wartości na kontrolce. 
         * @access public
         * @param {number} actualValue Aktualna wartość zmiennej
         * @example
         * // Przykład odświeżania wartości
         * k1.refresh(33.3);
         */
        refresh: function (actualValue) {
            var actualSectorColor,
                i,
                mainArc = this._mainArc,
                wartoscKolor = 0,
                startAngle = this._currentArcStartAngle,
                endAngle;

            

//            if (this.parent.id === 'uiISilnikOrganu') {
//                if (Math.abs(actualValue - this._oldVal) < this.sensitivity) {
//                    return;
//                }
//                console.log(Math.abs(actualValue - this._oldVal));
//            }

            this._valueLabel.text(actualValue.toFixed(this._precision)); // wpisanie nowej wartości do labelki
            if (Math.abs(actualValue - this._oldVal) < this.sensitivity) { // nie rysować każdej minimalnej zmiany - wydajność, procki się grzeją:)
                return;
            }
            this._oldVal = actualValue;

            // Funkcja służąca do animacji rysowanego łuku
            function arcTween(a) { // actualValue
                var i = d3.interpolate(startAngle, a);
                return function (t) { //transitionValue
                    startAngle = i(t);
                    return mainArc.endAngle(i(t))();
                };
            }

            endAngle = (-90 + (actualValue / this.maxValue) * 180);
            if ((actualValue > this.minValue) && (actualValue < this.maxValue)) { // wartość mieści się w zakresie
                for (i = 0; i < this._sectorsRanges.length; i += 1) { // pobranie odpowiadającego danej wartości koloru
                    wartoscKolor += this._sectorsRanges[i];
                    if (actualValue < wartoscKolor) {
                        actualSectorColor = this._sectorsColors[i];
                        break;
                    }
                }
                endAngle = endAngle * Math.PI / 180; // przeliczenie na radiany
            } else if (actualValue >= this.maxValue) { // wartość poza zakresem max, będzie narysowany pełny łuk 180stopni                        
                endAngle = 90 * (Math.PI / 180);
                actualSectorColor = this._sectorsColors[this._sectorsColors.length - 1];
            } else if (actualValue <= this.minValue) { // wartość poza zakresem min
                endAngle = -90 * (Math.PI / 180);
                actualSectorColor = this._sectorsColors[0];
            }

            //this._valueLabel.text(actualValue.toFixed(this._precision)); // wpisanie nowej wartości do labelki
            if (this.animateMainArc) { // animation
                this._foregroundPath.style("fill", actualSectorColor)
                    .datum(endAngle)
                    .transition()
                    .duration(this.duration)
                    .attrTween("d", arcTween);
            } else { // no animation
                this._foregroundPath
                    .datum({
                        endAngle: endAngle
                    })
                    .style("fill", actualSectorColor)
                    .attr("d", mainArc);
            }


            if (this.autoControlHysteresis) {
                this._checkHysteresis();
            }

            actualSectorColor = null; // var clear
            mainArc = null;
            startAngle = null;
            endAngle = null;
        },


        /**
         * @description Zniszczenie kotrolki
         * @access public
         */
        destroy: function () {
            d3.select("#" + this.parent.id).remove();
        },


        /**
         * @description Zmiana sektorów na nowe wartości podane przez użytkownika
         * @access public
         */
        updateSectors: function (newSectors) {
            this._updateSectors(newSectors);
        },


        enableHysteresis: function (histIndex) {
            if (!this.ignoreHysteresis) {
                this._enableHysteresis(histIndex);
            }
        },


        disableHysteresis: function (histIndex) {
            if (!this.ignoreHysteresis) {
                this._disableHysteresis(histIndex);
            }
        },


        storeSectorsDefaults: function () {
            this._sectorsDefaults = JSON.parse(JSON.stringify(this.sectors)); // copy array without references
        },


        restoreSectorsDefaults: function () {
            this.sectors = JSON.parse(JSON.stringify(this._sectorsDefaults));
            this._updateSectors(this.sectors);
        },


        showHysteresisSectors: function () {
            // pokazanie zakresów histerezy, kolor pomarańczowy
        }



    };

    return GaugeKM;
});


//        this._enableHysteresis = function (histInx) {
//            if (!this._initHyst) {
//                this._initHyst = true;
//            } else {
//                return;
//            }
//            var sectIndex = this.hysteresis[histInx].sectorIndex;
//            this.sectors[sectIndex].hi = this.sectors[sectIndex].hi + this.hysteresis[histInx].val;
//            this.sectors[sectIndex + 1].lo = this.sectors[sectIndex + 1].lo + this.hysteresis[histInx].val;
//            this._updateSectors(this.sectors);
//        };
//
//
//        this._disableHysteresis = function (histInx) {
//            if (this._initHyst) {
//                this._initHyst = false;
//            } else {
//                return;
//            }
//            var sectIndex = this.hysteresis[histInx].sectorIndex;
//            this.sectors[sectIndex].hi = this.sectors[sectIndex].hi - this.hysteresis[histInx].val;
//            this.sectors[sectIndex + 1].lo = this.sectors[sectIndex + 1].lo - this.hysteresis[histInx].val;
//            this._updateSectors(this.sectors);
//        };





//            if (!this.ignoreHysteresis) {
//                if (this.initHysteresisDelay > 0 && !this._initHyst && this._timeoutId === 0) {
//                    this._timeoutId = setTimeout(function () {
//                        console.log('delay');
//                        this._initHyst = true;
//                        this._enableHysteresis(histIndex);
//                    }, this.initHysteresisDelay);
//                } else {
//                    this._enableHysteresis(histIndex);
//                }
//            }